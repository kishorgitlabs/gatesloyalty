package com.brainmagic.gatescatalog.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.google.android.gms.gcm.GcmListenerService;
import java.util.Calendar;
import java.util.Random;


public class PushNotificationService extends GcmListenerService {
    int m;
    String message;
    @Override

    public void onMessageReceived(String from, Bundle data) {
         message = data.getString("message");
      //  sendNotification(message);
        createNotification(Calendar.getInstance().getTimeInMillis(),message);
        Random random = new Random();
         m=random.nextInt(9999-1000)+1000;

        Log.v(" m number === ", Integer.toString(m));
    }


    //This method is generating a notification and displaying the notification
    private void sendNotification(String message) {
        Intent intent = new Intent(this, Notification_Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int requestCode = 0;
        Notification notifyDetails=new Notification(R.drawable.new_notification,message, System.currentTimeMillis());
        PendingIntent pendingIntent = PendingIntent.getActivity(this, m, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder noBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.new_notification)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(sound)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(m, notifyDetails); //0 = ID of notification
    }


    private void createNotification(long when, String data){
       // String notificationContent ="Notification Content Click Here to go more details";
      //  String notificationTitle ="This is Notification";
        //large icon for notification,normally use App icon
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(),R.drawable.launcher);
        int smalIcon =R.drawable.new_notification;
        String notificationData=data;

		/*create intent for show notification details when user clicks notification*/
        Intent intent =new Intent(getApplicationContext(), Notification_Activity.class);
       // intent.putExtra(NOTIFICATION_DATA, notificationData);

		/*create unique this intent from  other intent using setData */
       // intent.setData(Uri.parse("content://"+when));
		/*create new task for each notification with pending intent so we set Intent.FLAG_ACTIVITY_NEW_TASK */
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), m, intent, PendingIntent.FLAG_ONE_SHOT);

		/*get the system service that manage notification NotificationManager*/
        NotificationManager notificationManager =(NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

		/*build the notification*/
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
                getApplicationContext())
                .setWhen(when)
                .setContentText(message)
                .setContentTitle("Gates Notification")
                .setSmallIcon(smalIcon)
                .setAutoCancel(true)
               // .setTicker(notificationTitle)
                .setLargeIcon(largeIcon)
                .setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_VIBRATE| Notification.DEFAULT_SOUND)
                .setContentIntent(pendingIntent);

		/*Create notification with builder*/
        Notification notification=notificationBuilder.build();

		/*sending notification to system.Here we use unique id (when)for making different each notification
		 * if we use same id,then first notification replace by the last notification*/
        notificationManager.notify((int) when, notification);
    }

}
