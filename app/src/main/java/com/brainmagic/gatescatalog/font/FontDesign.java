
package com.brainmagic.gatescatalog.font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class FontDesign extends TextView {
    public static Typeface FONT_NAME;

    public FontDesign(Context context) {
        super(context);
        isInEditMode();
    }

    public FontDesign(Context context, AttributeSet attrs) {
        super(context, attrs);
        isInEditMode();
    }

    public FontDesign(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        isInEditMode();
    }

    public void setTypeface(Typeface tf, int style) {
        if (!isInEditMode()) {
            Typeface normalTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Geogtq-Md.otf");
            Typeface boldTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Geogtq-Sb.otf");
            if (style == 1) {
                super.setTypeface(boldTypeface);
            } else {
                super.setTypeface(normalTypeface);
            }
        }
    }
}