package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.gatescatalog.api.models.moredetails.ModelDetailsResult;
import com.brainmagic.gatescatalog.gatesloyalty.R;

import java.util.List;

public class ModelSuitAdapter extends BaseAdapter {

    private Context mContext;
    private List<ModelDetailsResult> data;

    public ModelSuitAdapter(Context mContext, List<ModelDetailsResult> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.make_list_layout, parent, false);

        TextView make = convertView.findViewById(R.id.make_list);
        ImageView arrowRight = convertView.findViewById(R.id.arrow_right);

        String Make = data.get(position).getMake();
        int ArrowRight = R.drawable.right_arrow;


        make.setText(Make);
        arrowRight.setImageResource(ArrowRight);


        return convertView;
    }
}
