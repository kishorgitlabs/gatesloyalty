package com.brainmagic.gatescatalog.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.flipkart.youtubeview.activity.YouTubeActivity;
import com.brainmagic.gatescatalog.Constants;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.models.moredetails.VideoAttachment;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class VideoResourceRecyclerAdapter extends RecyclerView.Adapter<VideoResourceRecyclerAdapter.ViewHolder> {

    private List<String> data;
    private Context context;
    private Alertbox alertBox;

    public VideoResourceRecyclerAdapter(Context context, List<String> data) {
        this.data = data;
        this.context = context;
        this.alertBox = new Alertbox(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.part_no_video_adapter_list,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position){
        try {
            CircularProgressDrawable progressDrawable = new CircularProgressDrawable(context);
            progressDrawable.setStrokeWidth(5f);
            progressDrawable.setCenterRadius(90f);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                progressDrawable.setColorSchemeColors(context.getColor(R.color.red));
            }
            Picasso.with(context)

                    .load("http://img.youtube.com/vi/"+extractYoutubeId(data.get(position))+"/0.jpg")

                    .memoryPolicy(MemoryPolicy.NO_CACHE )

                    .networkPolicy(NetworkPolicy.NO_CACHE)

                    .error(R.drawable.noimages)

                    .placeholder(progressDrawable)

                    .into(holder.image);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

//        holder.videoDesc.setText(data.get(position).getVideoDescription());

        holder.videoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if(data.get(position)!=null) {
                        String URLString=data.get(position);
                        String urlsplit[]=URLString.split("=");
                        URLString=urlsplit[1];
                        Intent youtube=new Intent(context, YouTubeActivity.class);
                        youtube.putExtra("apiKey", Constants.API);
                        youtube.putExtra("videoId", URLString);
                        context.startActivity(youtube);
                    }
                }catch (ActivityNotFoundException e)
                {
                    alertBox.showAlert("No Link found to Open");
//                    Snackbar snackbar1 = Snackbar.make(holder.coordinatorLayout, context.getString(R.string.no_link_is_found), Snackbar.LENGTH_SHORT);
//                    snackbar1.show();
                }
                catch (Exception e)
                {
                    alertBox.showAlert("Invalid Link");
//                    Snackbar snackbar1 = Snackbar.make(holder.coordinatorLayout, context.getString(R.string.invalid_link), Snackbar.LENGTH_SHORT);
//                    snackbar1.show();
//                    Log.d(TAG, "onClick: "+e);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected ImageView image;
        protected TextView videoDesc;
        private LinearLayout videoLayout;
        public ViewHolder(@NonNull View view) {
            super(view);
            image = view.findViewById(R.id.video_img);
            videoDesc = view.findViewById(R.id.video_desc);
            videoLayout = view.findViewById(R.id.videos_layout);
        }
    }

    private String extractYoutubeId(String url) throws MalformedURLException {
        String id = null;
        try {
            String query = new URL(url).getQuery();
            String[] param = query.split("&");

            for (String row : param) {
                String[] param1 = row.split("=");
                if (param1[0].equals("v")) {
                    id = param1[1];
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return id;
    }

}
