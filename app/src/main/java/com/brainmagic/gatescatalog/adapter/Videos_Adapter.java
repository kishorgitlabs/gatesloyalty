package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.VideosMoreDetails.TrainingMoreDetailResult;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.brainmagic.gatescatalog.Constants.ROOT_URL;

/**
 * Created by BRAINMAGIC on 18-08-2016.
 */
public class Videos_Adapter extends ArrayAdapter {

    Context context;
    List<TrainingMoreDetailResult> data;


    public Videos_Adapter(Context context, List<TrainingMoreDetailResult> data) {
        super(context, R.layout.adapter_videos,data);
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Whats_NewOrderHolder holder;
        convertView = null;
        if (convertView == null) {

            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.adapter_videos, parent, false);

            holder = new Whats_NewOrderHolder();

          //  TextView grid_text = (TextView) convertView.findViewById(R.id.grid_text);
            TextView video_name = (TextView) convertView.findViewById(R.id.video_name);
          //  TextView video_descb = (TextView) convertView.findViewById(R.id.video_descb);
          //  TextView grid_text3 = (TextView) convertView.findViewById(R.id.grid_text3);
               ImageView video_image = (ImageView) convertView.findViewById(R.id.video_image);
           // ImageView grid_image = (ImageView) convertView.findViewById(R.id.grid_image);
            //   zoom = (ImageView) ((Activity) context).findViewById(R.id.zoomimage);

            //    adapter_engine_specification.setText(enggspecification.get(position));
           // grid_text.setText(select_catageory_name.get(position));
//            Log.v("Vedio URL :",select_url.get(position).toString());
//            Log.v("Image URL :",map.get(position).toString());

            Picasso.with(context)
                    .load(ROOT_URL + "Uploads/Events/"+data.get(position).getVideoAttachment())
                   // .resize(400,400)                        // optional
                    .error(R.drawable.gates_logo)
                    .into(video_image);
            video_name.setText(data.get(position).getVideoname());
         //   video_descb.setText(select_video_description.get(position));
           // video_image.setImageBitmap(map.get(position));



            convertView.setTag(holder);
        }


        //convertView.setTag(holder);


        else

        {
            holder = (Whats_NewOrderHolder) convertView.getTag();
        }


        return convertView;
    }


    class Whats_NewOrderHolder {
        public TextView sno;
        public TextView orderno;
        public TextView datetxt;
        public TextView delarname;


    }
}


