package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.gatescatalog.activities.productsearch.MoreDetailsActivity;
import com.brainmagic.gatescatalog.activities.productsearch.Product_OE_Activity;
import com.brainmagic.gatescatalog.api.models.Sample;
import com.brainmagic.gatescatalog.api.models.oesearch.Datum;
import com.brainmagic.gatescatalog.gatesloyalty.R;

import java.util.List;

public class OeSearchAdapter extends BaseAdapter {

    private List<Datum> sampleList;
    private Context mContext;

    public OeSearchAdapter(Context mContext, List<Datum> sampleList) {
        this.sampleList = sampleList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return sampleList.size();
    }

    @Override
    public Object getItem(int position) {
        return sampleList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.oe_search_view_layout, parent, false);

        TextView oem = convertView.findViewById(R.id.oem);
        TextView model = convertView.findViewById(R.id.model);
        TextView productType = convertView.findViewById(R.id.product_type);
        TextView segment = convertView.findViewById(R.id.segment);
        TextView engineCode = convertView.findViewById(R.id.engine_code);
        TextView engineSpecification = convertView.findViewById(R.id.engine_specification);
        TextView partNo = convertView.findViewById(R.id.part_no);
        TextView mrp = convertView.findViewById(R.id.mrp_oe);
        ImageView moreInfo = convertView.findViewById(R.id.more_info_oe);


        String Oem = sampleList.get(position).getMake();
        String Model = sampleList.get(position).getModel();
        String ProductType = sampleList.get(position).getArticlegroup();
        String Segment = sampleList.get(position).getSegment();
        String EngineCode = sampleList.get(position).getEnginecode();
        String EngineSpecification = sampleList.get(position).getFuel();
        String PartNo = sampleList.get(position).getArticle();
        Double MRP = sampleList.get(position).getMRP();
        int MoreInfo = R.drawable.more_info;

        moreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, MoreDetailsActivity.class)
                                .putExtra("searchType","OeSearch")
//                        .putExtra("navtxt", "OE >" + Part_Number_List.get(position))
                                .putExtra("navtxt","OE >" + PartNo)
                                .putExtra("partNo",PartNo)
                );
            }
        });

//        int MrpValue = MRP;

        oem.setText(Oem);
        model.setText(Model);
        productType.setText(ProductType);
        segment.setText(Segment);
        engineCode.setText(EngineCode);
        engineSpecification.setText(EngineSpecification);
        partNo.setText(PartNo);
        mrp.setText("₹ "+MRP+"");
        moreInfo.setImageResource(MoreInfo);

        return convertView;
    }
}
