package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.api.models.notification.NotificationResult;

import java.util.ArrayList;
import java.util.List;


public class Notification_Adapter extends ArrayAdapter {

    Context context;
    ArrayList<String> n_name;
    ArrayList<String> n_descb;
    ArrayList<String> n_exp_date;
    private List<NotificationResult> data;



//    public Notification_Adapter(Context context, ArrayList<String> n_name, ArrayList<String> n_descb, ArrayList<String> n_exp_date) {
//        super(context, R.layout.notification_adapter, n_name);
//        this.context = context;
//        this.n_name = n_name;
//        this.n_descb = n_descb;
//        this.n_exp_date = n_exp_date;
//
//
//    }

    public Notification_Adapter(Context context, List<NotificationResult> data) {
        super(context, R.layout.notification_adapter);
        this.data = data;
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Notify_OrderHolder holder;
        convertView=null;
        if (convertView == null) {

            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.notification_adapter, parent, false);

            holder = new Notify_OrderHolder();

            TextView s_no = (TextView) convertView.findViewById(R.id.s_no);
            TextView no_name = (TextView) convertView.findViewById(R.id.no_name);
            TextView no_descb = (TextView) convertView.findViewById(R.id.no_descb);
           // TextView no_exp_date = (TextView) convertView.findViewById(R.id.no_exp_date);


            s_no.setText(Integer.toString(position+1)+".");
            no_name.setText(data.get(position).getNotificationName());
            no_descb.setText(data.get(position).getDescription());

//            no_exp_date.setText(n_exp_date.get(position));



            convertView.setTag(holder);
        }


        //convertView.setTag(holder);


        else

        {
            holder = (Notify_OrderHolder) convertView.getTag();
        }


        return convertView;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    class Notify_OrderHolder {
        public TextView sno;
        public TextView orderno;
        public TextView datetxt;
        public TextView delarname;


    }
}
