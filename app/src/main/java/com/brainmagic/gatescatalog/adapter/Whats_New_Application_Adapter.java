package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import androidx.annotation.Nullable;

import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeResult;

import java.util.List;

/**
 * Created by BRAINMAGIC on 11-08-2016.
 */
public class Whats_New_Application_Adapter extends ArrayAdapter<EngineCodeResult> {

    private Context context;
//    private WhatsNewData data;
    List<EngineCodeResult> data;
    public Whats_New_Application_Adapter(Context context, List<EngineCodeResult> data) {
        super(context, R.layout.adapter_whats_application_new__grid_);
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Nullable
    @Override
    public EngineCodeResult getItem(int position) {
        return data.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        WhatsHolder_Application holder;

        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.adapter_whats_application_new__grid_, parent, false);

            holder = new WhatsHolder_Application();

            holder.modelcodetxt = (TextView) convertView.findViewById(R.id.modelcodetxt);
            holder.maketxt = (TextView) convertView.findViewById(R.id.maketxt);
            holder.partnumbertxt = (TextView) convertView.findViewById(R.id.parttxt);
            holder.modeltxt = (TextView) convertView.findViewById(R.id.modeltxt);
            holder.enginecodetxt = (TextView) convertView.findViewById(R.id.enginecodetxt);
            holder.storketxt = (TextView) convertView.findViewById(R.id.storketxt);


            holder.modelcodetxt.setText(data.get(position).getModelcode());
            holder.maketxt.setText(data.get(position).getMake());
            holder.partnumbertxt.setText(data.get(position).getArticle());
            holder.enginecodetxt.setText(data.get(position).getEnginecode());
//            holder.storketxt.setText(strokeList.get(position));
            holder.modeltxt.setText(data.get(position).getModel());

            convertView.setTag(holder);
        } else {
            holder = (WhatsHolder_Application) convertView.getTag();
            holder.modelcodetxt = (TextView) convertView.findViewById(R.id.modelcodetxt);
            holder.maketxt = (TextView) convertView.findViewById(R.id.maketxt);
            holder.partnumbertxt = (TextView) convertView.findViewById(R.id.parttxt);
            holder.modeltxt = (TextView) convertView.findViewById(R.id.modeltxt);
            holder.enginecodetxt = (TextView) convertView.findViewById(R.id.enginecodetxt);
            holder.storketxt = (TextView) convertView.findViewById(R.id.storketxt);


            holder.modelcodetxt.setText(data.get(position).getModelcode());
            holder.maketxt.setText(data.get(position).getMake());
            holder.partnumbertxt.setText(data.get(position).getArticle());
            holder.enginecodetxt.setText(data.get(position).getEnginecode());
//            holder.storketxt.setText(strokeList.get(position));
            holder.modeltxt.setText(data.get(position).getModel());
        }

        return convertView;
    }

}


class WhatsHolder_Application {

    TextView maketxt, modeltxt,whatsNewStatus;
    TextView partnumbertxt;
    TextView enginecodetxt;
    TextView storketxt, modelcodetxt;

}