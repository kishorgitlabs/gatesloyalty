package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.api.models.pdfmodel.PDFResult;

import java.util.List;

public class PDFListViewAdapter extends ArrayAdapter {

    private Context context;
    private List<PDFResult> data;

    public PDFListViewAdapter(@NonNull Context context,List<PDFResult> data) {
        super(context, R.layout.list_text);
        this.context = context;
        this.data = data;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_text,parent,false);

        }

        TextView pdfName = convertView.findViewById(R.id.item_text);
        pdfName.setText(data.get(position).getPdfName().trim());



        return convertView;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return data.get(position);
    }
}
