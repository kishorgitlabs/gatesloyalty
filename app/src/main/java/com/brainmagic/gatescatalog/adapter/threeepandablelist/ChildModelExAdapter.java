package com.brainmagic.gatescatalog.adapter.threeepandablelist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.api.models.moredetails.ModelList;
import com.brainmagic.gatescatalog.font.FontDesign;

import java.util.List;

@SuppressWarnings("unchecked")
public class ChildModelExAdapter extends BaseExpandableListAdapter {
    private List<ModelList> childModelListModelList;
    private Context mContext;

    public ChildModelExAdapter(Context mContext, List<ModelList> childModelListModelList) {
        this.mContext = mContext;
        this.childModelListModelList = childModelListModelList;
    }


    @Override
    public View getGroupView(int i, boolean isExpanded, View view, ViewGroup viewGroup) {
//        try {
            String name = childModelListModelList.get(i).getModel();
            if(view == null)
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_parent_make_expandable_list_view,viewGroup,false);
            FontDesign model = view.findViewById(R.id.heading_expandable_list);
            ImageView exArrow = view.findViewById(R.id.expandable_arrow);
            exArrow.setVisibility(View.VISIBLE);


            if(isExpanded)
                exArrow.animate().rotation(180).setInterpolator(new LinearInterpolator()).setDuration(500);
            else
                exArrow.animate().rotation(0).setInterpolator(new LinearInterpolator()).setDuration(500);
            model.setText("\t\t\t\t"+name);
//        }catch (Exception e){
//            e.printStackTrace();
//            Toast.makeText(mContext, "third one: "+e.toString(), Toast.LENGTH_SHORT).show();
//        }
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
//        if (getChildrenCount(i) > 1) {
//            try {
                if (view == null) {
                    view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_child_expandable_list_view, viewGroup, false);


                    TextView gatesPartNo = view.findViewById(R.id.list_gatespartno);
                    TextView partDesc = view.findViewById(R.id.list_partdescription);
                    TextView model = view.findViewById(R.id.list_model);
                    TextView engineCode = view.findViewById(R.id.list_engine_code);
                    TextView modelCode = view.findViewById(R.id.list_model_code);
                    TextView monthFrom = view.findViewById(R.id.list_monthfrom);
                    TextView monthTill = view.findViewById(R.id.list_monthtill);
                    TextView appAtt = view.findViewById(R.id.app_att);
                    TextView stroke = view.findViewById(R.id.stroke);
                    TextView ccValue = view.findViewById(R.id.cc_value);
                    TextView kkValue = view.findViewById(R.id.kw_value);

                    gatesPartNo.setText(childModelListModelList.get(i).getProduSpec().get(i1).getArticle());
                    partDesc.setText(childModelListModelList.get(i).getProduSpec().get(i1).getArticlegroup());
                    model.setText(childModelListModelList.get(i).getProduSpec().get(i1).getModel());
                    engineCode.setText(childModelListModelList.get(i).getProduSpec().get(i1).getEnginecode());
                    modelCode.setText(childModelListModelList.get(i).getProduSpec().get(i1).getModelcode());
                    monthFrom.setText(childModelListModelList.get(i).getProduSpec().get(i1).getMonthFrom());
                    monthTill.setText(childModelListModelList.get(i).getProduSpec().get(i1).getMonthTill());
                    appAtt.setText(childModelListModelList.get(i).getProduSpec().get(i1).getApplicationAttributes());
                    stroke.setText(String.valueOf(childModelListModelList.get(i).getProduSpec().get(i1).getStroke()));
                    ccValue.setText(childModelListModelList.get(i).getProduSpec().get(i1).getCC());
                    kkValue.setText(childModelListModelList.get(i).getProduSpec().get(i1).getKW());
                } else {
                    TextView gatesPartNo = view.findViewById(R.id.list_gatespartno);
                    TextView partDesc = view.findViewById(R.id.list_partdescription);
                    TextView model = view.findViewById(R.id.list_model);
                    TextView engineCode = view.findViewById(R.id.list_engine_code);
                    TextView modelCode = view.findViewById(R.id.list_model_code);
                    TextView monthFrom = view.findViewById(R.id.list_monthfrom);
                    TextView monthTill = view.findViewById(R.id.list_monthtill);
                    TextView appAtt = view.findViewById(R.id.app_att);
                    TextView stroke = view.findViewById(R.id.stroke);
                    TextView ccValue = view.findViewById(R.id.cc_value);
                    TextView kkValue = view.findViewById(R.id.kw_value);

                    gatesPartNo.setText(childModelListModelList.get(i).getProduSpec().get(i1).getArticle());
                    partDesc.setText(childModelListModelList.get(i).getProduSpec().get(i1).getArticlegroup());
                    model.setText(childModelListModelList.get(i).getProduSpec().get(i1).getModel());
                    engineCode.setText(childModelListModelList.get(i).getProduSpec().get(i1).getEnginecode());
                    modelCode.setText(childModelListModelList.get(i).getProduSpec().get(i1).getModelcode());
                    monthFrom.setText(childModelListModelList.get(i).getProduSpec().get(i1).getMonthFrom() + "/"
                            + childModelListModelList.get(i).getProduSpec().get(i1).getYearFrom());
                    monthTill.setText(childModelListModelList.get(i).getProduSpec().get(i1).getMonthTill() + "/" +
                            childModelListModelList.get(i).getProduSpec().get(i1).getYearTill());
                    appAtt.setText(childModelListModelList.get(i).getProduSpec().get(i1).getApplicationAttributes());
                    stroke.setText(String.valueOf(childModelListModelList.get(i).getProduSpec().get(i1).getStroke()));
                    ccValue.setText(childModelListModelList.get(i).getProduSpec().get(i1).getCC());
                    kkValue.setText(childModelListModelList.get(i).getProduSpec().get(i1).getKW());
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                Toast.makeText(mContext, "details view: " + e.toString(), Toast.LENGTH_SHORT).show();
//            }
//        }else {
//            Toast.makeText(mContext, "details view1 : ", Toast.LENGTH_LONG).show();
//        }
                }

        return view;
    }

    @Override
    public int getGroupCount() {
        return childModelListModelList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        try {
            return childModelListModelList.get(groupPosition).getProduSpec().size();
        }catch (Exception e)
        {
            return 0;
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return childModelListModelList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childModelListModelList.get(groupPosition).getProduSpec().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }


    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

}
