package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.brainmagic.gatescatalog.gatesloyalty.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class ListViewAdapter extends ArrayAdapter implements Filterable {

    Context context;
    List<String> n_name  = new ArrayList<String>();
    ArrayList<Integer> oemListCount ;
    ArrayList<String> n_name1;
    ArrayList<Integer> oemListCount1;
//    List<VideoGategoryDupData> videoDuba;
    private CheckBox checkBox;
    private ImageView imageNext;
    private Map<String, Boolean> isCheck;
    private boolean isModelCheck;
    private int VelCheck=-2;
    private RelativeLayout listRel;
    private SelectModel selectModel;
    private List<String> selectModelList;
//    private PdfForamt pdfGenerator;
//    private List<Model> modelList;
//    private HashMap<String,Model> hashMap;

//    public ListViewAdapter(Context context, ArrayList<String> n_name, ArrayList<Integer> oemListCount, Integer VelCheck, boolean forCheckModels) {
//        super(context, R.layout.list_text, n_name);
//        this.context = context;
//        this.n_name = n_name;
//        this.oemListCount=oemListCount;
//        this.VelCheck = VelCheck;
//        n_name1 = new ArrayList<>(n_name);
////        modelList = new ArrayList<>();
//        selectModelList = new ArrayList<>();
////        pdfGenerator=new PdfForamt();
////        hashMap=new HashMap<>();
//        // alertbox = new Alertbox(context);
//
//        this.isModelCheck=forCheckModels;
//        oemListCount1 = new ArrayList<>(oemListCount);
//            isCheck = new HashMap<>();
//    }
    public ListViewAdapter(Context context, List<String> n_name) {
        super(context, R.layout.list_text, n_name);
        this.context = context;
        this.n_name = n_name;
    }

//    public ListViewAdapter(Context context, ArrayList<String> n_name, List<VideoGategoryDupData> list) {
//        super(context, R.layout.list_text, n_name);
//        this.context = context;
//        this.n_name = n_name;
//        this.videoDuba = list;
//    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ListHolder  holder = new ListHolder();
        convertView=null;
        if (convertView == null) {

            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.list_text, parent, false);


            holder.no_name = (TextView) convertView.findViewById(R.id.item_text);
            holder.oemCountListText=convertView.findViewById(R.id.training_video_badge);
            checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);
            listRel = (RelativeLayout)convertView.findViewById(R.id.list) ;
            imageNext = (ImageView) convertView.findViewById(R.id.item_imageView);
            holder.no_name.setText(n_name.get(position));
        }
        else
        {
            holder = (ListHolder) convertView.getTag();
        }
        return convertView;
    }

    public void clearIsChecked()
    {
        isCheck.clear();
//        modelList.clear();
//        hashMap.clear();
        selectModelList.clear();
    }


    public void setSelectModelListItems(int isSelectedAll)
    {
        this.VelCheck=isSelectedAll;
        selectModelList.clear();
        selectModelList.addAll(n_name);
    }

    public void clearSelectModelListItems(int isSelectedAll)
    {
        this.VelCheck=isSelectedAll;
        selectModelList.clear();
    }


/*public void removeItems(List<String> items){
        for(String item: items){
            n_name.remove(item);
        }
        notifyDataSetChanged();
}*/
/*
    public void filtermodel(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());

        n_name.clear();
        oemListCount.clear();

        if (charText.length() == 0) {

            n_name.addAll(n_name2);
            oemListCount2.addAll(oemListCount);


        } else {

            for (int i = 0; i < n_name2.size(); i++) {


                String wp = String.valueOf(n_name2.get(i).toLowerCase(Locale.getDefault()));


                // String id=String.valueOf(arraylistSize.get(i).getEmployeeId(******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************).toLowerCase());

                //   String id=arraylistSize.get(i).getEmployeeId().toLowerCase();

                //String mobile=arraylistSize.get(i).getMobileNo().toLowerCase();

                if (wp.startsWith(charText.toLowerCase())) {


                    n_name.add(n_name2.get(i));
                    oemListCount.add(oemListCount2.get(i));

                    notifyDataSetChanged();

                }

            }

        }

    }
*/


    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());


        n_name.clear();
        oemListCount.clear();

        if (charText.length() == 0) {

            n_name.addAll(n_name1);
            oemListCount.addAll(oemListCount1);


        } else {

            for (int i = 0; i < n_name1.size(); i++) {

                String wp = String.valueOf(n_name1.get(i).toLowerCase(Locale.getDefault()));

                // String id=String.valueOf(arraylistSize.get(i).getEmployeeId(******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************).toLowerCase());

                //   String id=arraylistSize.get(i).getEmployeeId().toLowerCase();

                //String mobile=arraylistSize.get(i).getMobileNo().toLowerCase();

                if (wp.contains(charText.toLowerCase())) {


                    n_name.add(n_name1.get(i));
                    oemListCount.add(oemListCount1.get(i));

                    notifyDataSetChanged();

                }

            }

        }

}

 class ListHolder
    {
        public TextView no_name,oemCountListText;
    }

    public interface SelectModel{
//        void selectedModel(List<Model> selectModeList);
    }
}

