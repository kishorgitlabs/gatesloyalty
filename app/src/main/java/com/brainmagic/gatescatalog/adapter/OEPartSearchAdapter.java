package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.brainmagic.gatescatalog.gatesloyalty.R;

import java.util.List;

public class OEPartSearchAdapter extends BaseAdapter {

    private Context mContext;
    private List<String> partNoList;

    public OEPartSearchAdapter(Context mContext, List<String> partNoList) {
        this.mContext = mContext;
        this.partNoList = partNoList;
    }

    @Override
    public int getCount() {
        return partNoList.size();
    }

    @Override
    public Object getItem(int position) {
        return partNoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.oe_search_partno_layout, parent, false);

        TextView partNoSearch = convertView.findViewById(R.id.part_no_oe_search);

        partNoSearch.setText(partNoList.get(position));


        return convertView;
    }
}
