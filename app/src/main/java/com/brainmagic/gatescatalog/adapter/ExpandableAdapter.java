package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brainmagic.gatescatalog.gatesloyalty.R;

public class ExpandableAdapter extends BaseExpandableListAdapter {

    Context context;
//    private List<Group> groups;
    public ExpandableAdapter(Context context) {
        this.context = context;
//        this.groups=groups;

    }

    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return 1;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return 1;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

//        Group group= (Group) getGroup(groupPosition);
//
//        if(convertView==null)
//        {
            convertView= LayoutInflater.from(context).inflate(R.layout.adapter_group_expandable_list_view,null);
//        }
//
//        TextView make=convertView.findViewById(R.id.heading_expandable_list);
//        make.setText(group.getMakeName()+" [ "+group.getChildList().size()+" Vehicles]");


        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {



//        Child child= (Child) getChild(groupPosition,childPosition);

        if(convertView==null)
        {
            convertView= LayoutInflater.from(context).inflate(R.layout.adapter_expandable_oe_child_view,null);
        }

        LinearLayout header=convertView.findViewById(R.id.heading_child_oe);
        if(childPosition==0)
        {
            header.setVisibility(View.VISIBLE);
        }
        else {
            header.setVisibility(View.GONE);
        }
        TextView model=convertView.findViewById(R.id.model_oe_text);
        TextView engineCode=convertView.findViewById(R.id.model_code_oe_text);
        TextView newcount=convertView.findViewById(R.id.oe_new_cart);
        TextView segment=convertView.findViewById(R.id.segment_oe);

//        try {
//            if (child.getNewStatus() != null) {
//                if (child.getNewStatus().equals("Yes")) {
//                    newcount.setVisibility(View.VISIBLE);
//                    newcount.setText("N");
//                }
//                else {
//                    newcount.setVisibility(View.GONE);
//                }
//            }
//            if(child.getSegment().equals("Passenger Car"))
//            {
//                segment.setText("PCV");
//            }
//            else if(child.getSegment().equals("Light Commercial"))
//            {
//                segment.setText("LCV");
//            }
//            else if(child.getSegment().equals("2 Wheeler"))
//            {
//                segment.setText("2W");
//            }
//            else if(child.getSegment().equals("Heavy Commercial"))
//            {
//                segment.setText("HCV");
//            }
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//        Animation animation= AnimationUtils.loadAnimation(context,R.anim.scale);
//        model.setText(child.getModel());
//        engineCode.setText(child.getEngineCode());
//        convertView.startAnimation(animation);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
