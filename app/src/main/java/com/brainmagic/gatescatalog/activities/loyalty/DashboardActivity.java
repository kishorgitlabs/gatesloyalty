package com.brainmagic.gatescatalog.activities.loyalty;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributor_Search_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.activities.scan.AnyOrientationCaptureActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.gateslogin.GatesLogin;
import com.brainmagic.gatescatalog.api.models.redeenpoints.RedeemPoints;
import com.brainmagic.gatescatalog.api.models.scanloyalty.ScanLoyalty;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends AppCompatActivity {

    public static final String TAG = DashboardActivity.class.getSimpleName();

    private RelativeLayout scanBar, scanHistory, enterScanBar, giftCatalogue, redeenPoints, feedBack;
    private LinearLayout dashBoardLayout, gatesEmployeeLayout;
    private EditText userNameGates, passwordGates;
    private Button loginGates;
    private Alertbox alertbox;
    private ImageView menu;
    private IntentIntegrator qrScan;
    private FloatingActionButton home, back;
    SharedPreferences myShare;
    SharedPreferences.Editor sharedEditor;
    private String name, mobileNo, bussinessName, userType, address, latitude, longitude, state, city;
    private TextView enterCode, scanCode, title;
    String UserNameGates, PasswordGates;
    boolean gatesLogin;

    private TextView totalPoints, claimedPoints, balancedPoints;
    private Button okReward;
    private View points;

    AlertDialog scannednumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        scanBar = findViewById(R.id.scan_loyalty);
        scanHistory = findViewById(R.id.scan_history_loyalty);
        enterScanBar = findViewById(R.id.enter_code_loyalty);
        giftCatalogue = findViewById(R.id.gift_item_loyalty);
        redeenPoints = findViewById(R.id.redeem_point_loyalty);
        feedBack = findViewById(R.id.feedback_loyalty);
        title = findViewById(R.id.flowtext);
        enterCode = findViewById(R.id.enter_code_loyalty_text);
        scanCode = findViewById(R.id.scan_loyalty_txt);
        dashBoardLayout = findViewById(R.id.dash_board_layout);
        gatesEmployeeLayout = findViewById(R.id.gates_employee_layout);
        userNameGates = findViewById(R.id.user_name_gates);
        passwordGates = findViewById(R.id.password_gates);
        loginGates = findViewById(R.id.login_gates);
        home = findViewById(R.id.home);
        menu = findViewById(R.id.menu);
        back = findViewById(R.id.back);
        qrScan = new IntentIntegrator(this);
        myShare = getSharedPreferences("Registration", MODE_PRIVATE);
        sharedEditor = myShare.edit();


        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.brainmagic.gatescatalog.gates",
                    PackageManager.GET_SIGNATURES);
            Log.d("created", "afterpackage");
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.wtf("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("KeyHash", e.getMessage());

        } catch (NoSuchAlgorithmException e) {
            Log.d(TAG, "KeyHash: ", e);
        }
        mobileNo = myShare.getString("MobNo", "");
        name = myShare.getString("Name", "");
        userType = myShare.getString("Usertype", "");
        bussinessName = myShare.getString("Bname", "");
        address = myShare.getString("address", "");
        longitude = myShare.getString("longitude", "");
        latitude = myShare.getString("latitude", "");
        state = myShare.getString("state", "");
        city = myShare.getString("city", "");


        if (myShare.getString("Usertype", "").equals("Mechanic")) {
            title.setText(getString(R.string.dash_board));
            enterCode.setText("Enter Coupon Code");
            scanCode.setText("Scan Coupon Code");
            dashBoardLayout.setVisibility(View.VISIBLE);
            gatesEmployeeLayout.setVisibility(View.GONE);
        } else if (myShare.getString("Usertype", "").equals("Dealer / Retailer")) {
            title.setText(getString(R.string.dash_board));
            enterCode.setText("Enter MRP Code");
            scanCode.setText("Scan MRP label");
            dashBoardLayout.setVisibility(View.VISIBLE);
            gatesEmployeeLayout.setVisibility(View.GONE);
        } else if (myShare.getString("Usertype", "").equals("Gates Employee")) {
            title.setText(getString(R.string.gates_login));
            dashBoardLayout.setVisibility(View.GONE);
            gatesEmployeeLayout.setVisibility(View.VISIBLE);
            menu.setVisibility(View.GONE);
        }

        loginGates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserNameGates = userNameGates.getText().toString();
                PasswordGates = passwordGates.getText().toString();

                if (UserNameGates.equals("") && PasswordGates.equals("")) {
                    Toast.makeText(DashboardActivity.this, "Enter the user name and password", Toast.LENGTH_LONG).show();
                } else if (UserNameGates.equals("")) {
//                    userNameGates.setFocusable(true);
                    userNameGates.setError("Enter the User Name");
                } else if (PasswordGates.equals("")) {
//                    passwordGates.setFocusable(true);
                    passwordGates.setError("Enter the Password");
                } else {
                    loginGatesEmployee();
                }
            }
        });


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardActivity.this, HomeScreenActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

        alertbox = new Alertbox(DashboardActivity.this);

        scanHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardActivity.this, ScanHistoryLoyaltyActivity.class));
            }
        });

        scanBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qrScan.setBeepEnabled(true);
                qrScan.setOrientationLocked(false);
                qrScan.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                qrScan.setCaptureActivity(AnyOrientationCaptureActivity.class);
                qrScan.initiateScan();
            }
        });

        feedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardActivity.this, FeedbackActivity.class));
            }
        });

        giftCatalogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(DashboardActivity.this, GiftiemActivity.class));
                alertbox.showAlert("Under Production !");
            }
        });

        enterScanBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog alertDialog = new AlertDialog.Builder(DashboardActivity.this).create();
                final View enteralert = LayoutInflater.from(DashboardActivity.this).inflate(R.layout.enterpartnumbervalidate, null);
                final EditText scannnumberenter = enteralert.findViewById(R.id.scannnumberenter);
                if (myShare.getString("Usertype", "").equals("Mechanic")) {
                    scannnumberenter.setHint("Enter Coupon Code");
                } else if (myShare.getString("Usertype", "").equals("Dealer / Retailer")) {
                    scannnumberenter.setHint("Enter MRP Label Code");
                }
                Button validateenter = enteralert.findViewById(R.id.validateenter);
                Button cancel = enteralert.findViewById(R.id.cancel);

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                validateenter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String productnumb = scannnumberenter.getText().toString();
                        alertDialog.dismiss();
                        if (productnumb.equals("") || productnumb == null) {
                            Toast.makeText(DashboardActivity.this, "Please Enter Coupon Number", Toast.LENGTH_SHORT).show();
                        } else if (productnumb.length() < 13 || productnumb.length() < 13) {
                            Toast.makeText(DashboardActivity.this, "Please Enter valid 13 digit Coupon Number", Toast.LENGTH_SHORT).show();
                        } else {
                            scan(productnumb);
                        }

                    }
                });
                alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
                alertDialog.setView(enteralert);
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();
            }
        });

        redeenPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scannednumber = new AlertDialog.Builder(DashboardActivity.this).create();
                points = LayoutInflater.from(DashboardActivity.this).inflate(R.layout.redeem_points, null);
                totalPoints = points.findViewById(R.id.tottalpoints);
                claimedPoints = points.findViewById(R.id.claimedpoints);
                balancedPoints = points.findViewById(R.id.blancedpoints);
                okReward = points.findViewById(R.id.oka_redeem);


//                redeenpoint(totalPoints, points);
//                newRewardPoint();

                Intent intent = new Intent(DashboardActivity.this, RewardPointsHistory.class);
                startActivity(intent);

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                onBackPressed();
                startActivity(new Intent(DashboardActivity.this, HomeScreenActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });
       /* home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashboardActivity.this, HomeScreenActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                );
            }
        });*/

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(DashboardActivity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub
                        switch (item.getItemId()) {
                            case R.id.homepop:
//                                Intent home = new Intent(HomeScreenActivity.this, HomeScreenActivity.class);
//                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(DashboardActivity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(DashboardActivity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(DashboardActivity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
                            case R.id.viewprofile:
                                startActivity(new Intent(DashboardActivity.this, ViewProfileActivity.class));
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(HomeScreenActivity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.scan:
                                startActivity(new Intent(DashboardActivity.this, ScanActivity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(DashboardActivity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(DashboardActivity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(DashboardActivity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(DashboardActivity.this, Distributor_Search_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(HomeScreenActivity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(DashboardActivity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();
            }
        });
    }

    private void newRewardPoint() {
        try {
            final ProgressDialog loading = ProgressDialog.show(DashboardActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

            APIService service = RetrofitClient.getApiService();

            Call<RedeemPoints> call = service.redeemPoints(mobileNo);

            call.enqueue(new Callback<RedeemPoints>() {
                @Override
                public void onResponse(Call<RedeemPoints> call, Response<RedeemPoints> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")){
                        String Total = response.body().getData();

                        SpannableString content = new SpannableString(response.body().getData());
                        content.setSpan(new UnderlineSpan(), 0, content.length(),0);

                        totalPoints.setText(content);
                        balancedPoints.setText(Total);

                        totalPoints.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                scannednumber.dismiss();
                                startActivity(new Intent(DashboardActivity.this, ScanHistoryLoyaltyActivity.class));
                                finish();
                            }
                        });

                        okReward.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                scannednumber.dismiss();
                            }
                        });

                        scannednumber.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
                        scannednumber.setView(points);
                        scannednumber.setCanceledOnTouchOutside(false);
                        scannednumber.show();


                    }else if (response.body().getResult().equals("No Record")){
                        alertbox.showAlert("No Recond Found !");
                    }else {
                        alertbox.showAlert("Please try again later");
                    }
                }

                @Override
                public void onFailure(Call<RedeemPoints> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showAlert(getString(R.string.server_error));
                }
            });

        }catch (Exception ex){
            ex.printStackTrace();
            alertbox.showAlert(getString(R.string.internet_error_msg));
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        startActivity(new Intent(DashboardActivity.this, HomeScreenActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    private void loginGatesEmployee() {
        try {
            final ProgressDialog loading = ProgressDialog.show(DashboardActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

            APIService service = RetrofitClient.getApiService();

            Call<GatesLogin> call = service.gatesLogin(
                    myShare.getString("id", ""),
                    UserNameGates,
                    PasswordGates,
                    PasswordGates,
                    userType);

            call.enqueue(new Callback<GatesLogin>() {
                @Override
                public void onResponse(Call<GatesLogin> call, Response<GatesLogin> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")) {

                        sharedEditor.putString("userNameGates", UserNameGates);
                        sharedEditor.putString("passwordGates", PasswordGates);
                        sharedEditor.putString("mobileNoGates", response.body().getData().getMobileNo());
                        sharedEditor.putString("emailGates", response.body().getData().getEmail());
                        sharedEditor.putString("id", String.valueOf(response.body().getData().getId()));
                        sharedEditor.putBoolean("gatesLogin", true);
                        sharedEditor.commit();
                        Intent intent = new Intent(DashboardActivity.this, DashBoardGatesActivity.class);
                        startActivity(intent);
                        finish();

                    } else if (response.body().getResult().equals("Not Success")) {
                        alertbox.showAlert("Check the User name and Password");
                    } else {
                        alertbox.showAlertWithBack("Please try again");
                    }
                }

                @Override
                public void onFailure(Call<GatesLogin> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showAlert("Please try again later");
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void redeenpoint(TextView totalPoints, View points) {

        final ProgressDialog loading = ProgressDialog.show(DashboardActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

        try {

            APIService service = RetrofitClient.getApiService();

            Call<RedeemPoints> call = service.redeemPoints(myShare.getString("MobNo", ""));

            call.enqueue(new Callback<RedeemPoints>() {
                @Override
                public void onResponse(Call<RedeemPoints> call, Response<RedeemPoints> response) {
                    loading.dismiss();

                    String point = String.valueOf(response.body().getData());

                    totalPoints.setText("Total points you earned: " + point);
                    Button validate = points.findViewById(R.id.oka_redeem);
                    validate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            scannednumber.dismiss();
                        }
                    });
                    scannednumber.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
                    scannednumber.setView(points);
                    scannednumber.setCanceledOnTouchOutside(false);
                    scannednumber.show();
                    if (response.body().getResult().equals("Success")) {

                    }
//                    alertbox.showAlert("Please try again later");
                    /*else if (response.body().getResult().equals("No Record")){
//                        alertbox.showAlert("You didn't Scan Any Product");
                    }*/
                }

                @Override
                public void onFailure(Call<RedeemPoints> call, Throwable t) {
//                    alertbox.showAlert(t.getMessage());
                    alertbox.showAlert("Please try again later");
                }
            });

        } catch (Exception e) {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {

            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                final AlertDialog scannednumber = new AlertDialog.Builder(DashboardActivity.this).create();
                final View scan = LayoutInflater.from(DashboardActivity.this).inflate(R.layout.scannednumber, null);
                final EditText scannnumber = scan.findViewById(R.id.scannnumberalert);
                Button validate = scan.findViewById(R.id.validatealert);
                scannnumber.setText(result.getContents());

                validate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String productnumb = scannnumber.getText().toString();
                        scannednumber.dismiss();
                        String p = productnumb.replaceAll("\t", "");
                        if (p.length() == 13) {
                            scan(p);
                        } else if (p.length() < 13 || p.length() > 13) {
                            Sucess("Invalid Coupon Code");
                        }
                    }
                });

                scannednumber.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
                scannednumber.setView(scan);
                scannednumber.setCanceledOnTouchOutside(false);
                scannednumber.show();
            }
        } else {
            if (requestCode == 200) {
                super.onActivityResult(requestCode, resultCode, data);
//            if (imagePicker.handleActivityResult(resultCode,requestCode,data!=null)) {
                if (resultCode == RESULT_OK) {
//                    imagePicker.handleActivityResult(resultCode, requestCode, data);

                }
            }
        }
    }

    private void scan(String p) {

        final ProgressDialog loading = ProgressDialog.show(DashboardActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

        try {
            APIService service = RetrofitClient.getApiService();
            Call<ScanLoyalty> call = service.scanLoyalty(
                    mobileNo,
                    p,
                    userType,
                    bussinessName,
                    state,
                    city,
                    address,
                    latitude,
                    longitude);

            call.enqueue(new Callback<ScanLoyalty>() {
                @Override
                public void onResponse(Call<ScanLoyalty> call, Response<ScanLoyalty> response) {
                    loading.dismiss();

                    if (response.body().getResult().equals("ScanSuccess")) {
//                        Sucess("Dear " + myShare.getString("Name", "") + ", You Earned " +
//                                "" + response.body().getData().getRewardPoint() + " Points");

                        Sucess("Dear" + " " + myShare.getString("Name", "") + ", You Earned " + response.body().getData().getRewardPoint() + " Points. This number is eligible for reward points(यह नंबर रिवार्ड पॉइंट्स के लिए योग्य है). Thank You.");

                    } else if (response.body().getResult().equals("AlreadScaned")) {
                        Sucess("Dear " + myShare.getString("Name", "") + ", This number is already scanned(यह संख्या पहले से ही स्कैन की गई है). You are not eligible for reward points. Thank You.");

                    } else if (response.body().getResult().equals("No Record")) {
                        Sucess("Dear " + myShare.getString("Name", "") + ", This number is not eligible for reward points(यह अंक रिवार्ड पॉइंट्स के लिए योग्य नहीं है). Thank You.");
                    } else {
                        Sucess("Dear " + myShare.getString("Name", "") + ", This number is not eligible for reward points(यह अंक रिवार्ड पॉइंट्स के लिए योग्य नहीं है). Thank You.");
                    }
                }

                @Override
                public void onFailure(Call<ScanLoyalty> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showAlert(getString(R.string.server_error));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            loading.dismiss();
            alertbox.showAlert(getString(R.string.internet_error_msg));
        }
    }

    public void Sucess(String s) {
        Alertbox alert = new Alertbox(DashboardActivity.this);
        alert.Scansuccess(s);
    }
}