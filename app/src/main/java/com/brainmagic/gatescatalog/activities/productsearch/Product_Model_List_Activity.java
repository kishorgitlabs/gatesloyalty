package com.brainmagic.gatescatalog.activities.productsearch;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.transition.ChangeBounds;
import androidx.transition.TransitionManager;

import com.brainmagic.gatescatalog.activities.loyalty.ViewProfileActivity;
import com.bumptech.glide.Glide;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.adapter.ListViewAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.StringListModel;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Product_Model_List_Activity extends Activity
//        implements ListViewAdapter.SelectModel
{

    ListView listView;
    SQLiteDatabase db;
//    private ImageView SearchBox;
    Cursor c;
    private Alertbox alertbox;
    private ImageView PdfDownload;
    private ImageView backArrow;
    private CheckBox mCheckbox;

//    private List<PdfFormatResult> data;
//    private MaterialSearchBar search_Box;
    private ListViewAdapter listViewAdapter;
//    private String modelSearch;
    ArrayList<String> model;
    private Integer CheckVal = -1;
    ArrayList<Integer> modelCount;
//    ArrayList<String> product_type;
//    ArrayList<String> producttype_id;
    String segmentid, oem_name, vehicle_type;
    private ImageView menuVs;
//    Bitmap bitmap;
//    private ImageView info_Search,info_Clear;
    SharedPreferences myshare;


    private Network_Connection_Activity network_connection_activity;
    SharedPreferences.Editor editor;
    String Email, Mobno, Usertype,Country;
    private Alertbox box = new Alertbox(Product_Model_List_Activity.this);
    private String selectQuery,strVal;
    public static List<String> userSelection = new ArrayList<>();
    public static ActionMode actionMode =null;
    public static boolean isActionmode=false;

    private TextView ProfileName,navicationtxt;
//    private ConstraintLayout signout_relativelayout;
    private LinearLayout signout_relativelayout;
    private FloatingActionButton back, home;
    private ViewGroup includeviewLayout;
    private ImageView profilePicture;
    private String makeName,makeStr;
//    private PdfForamt pdfGenerator;
//    private List<Model> selectModeList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_vechicle_list_);

        navicationtxt = (TextView) findViewById(R.id.navicationtxt);
        listView = (ListView) findViewById(R.id.listview);
//        mCheckbox = (CheckBox)findViewById(R.id.checkboxmodel);
//        SearchBox = (ImageView)findViewById(R.id.search);
        network_connection_activity = new Network_Connection_Activity(Product_Model_List_Activity.this);
        alertbox = new Alertbox(Product_Model_List_Activity.this);
//        PdfDownload = (ImageView)findViewById(R.id.btnPdf);
        backArrow = (ImageView)findViewById(R.id.backarrow);

//        pdfGenerator=new PdfForamt();

        includeviewLayout = findViewById(R.id.profile_logo);
        ProfileName = (TextView) includeviewLayout.findViewById(R.id.profilename);
        signout_relativelayout =  findViewById(R.id.signout);
        signout_relativelayout.setVisibility(View.GONE);
        profilePicture = (ImageView) includeviewLayout.findViewById(R.id.profilepicture);


        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        menuVs = (ImageView) findViewById(R.id.menu);

        model = new ArrayList<String>();
        modelCount = new ArrayList<Integer>();
//        search_Box = (MaterialSearchBar) findViewById(R.id.text_search_box1);

        oem_name = getIntent().getStringExtra("OEM");
        vehicle_type = getIntent().getStringExtra("SEGMENT");

        navicationtxt.setText(getIntent().getStringExtra("navitxt"));
        makeName = getIntent().getStringExtra("navitxt");
        makeStr = getIntent().getStringExtra("MAKENAME");

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
//        listView.setMultiChoiceModeListener(modeListener);
        Email = myshare.getString("email", "");
        Mobno = myshare.getString("MobNo", "");
        Usertype = myshare.getString("Usertype", "");
        Country = myshare.getString("Country", "");
        ProfileName.setText(myshare.getString("name", "")+" , "+Country);
        if (!myshare.getString("profile_path", "").equals(""))
        Glide.with(Product_Model_List_Activity.this)
                .load(new File(myshare.getString("profile_path", "")))
                .into(profilePicture);

        if(myshare.getBoolean("islogin",false))
            signout_relativelayout.setVisibility(View.VISIBLE);
        else
            signout_relativelayout.setVisibility(View.GONE);

//        search_Box.setOnSearchActionListener(this);

//       search_Box.addTextChangeListener(new TextWatcher() {
//           @Override
//           public void beforeTextChanged(CharSequence s, int start, int count, int after) {
////                listViewAdapter.getFilter().filter(s);
//
//           }

//           @Override
//           public void onTextChanged(CharSequence s, int start, int before, int count) {
//               strVal = search_Box.getText().toString();
////               if(strVal.isEmpty()){
////                   new Vechiclename().execute();
////               }else
////                   {
//                       listViewAdapter.filter(strVal);
//                       listViewAdapter.notifyDataSetChanged();
////               }
//           }
//
//           @Override
//           public void afterTextChanged(Editable s) {
//
//           }
//       });



       backArrow.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               isActionmode = false;
               actionMode =null;
              // new Vechiclename().execute();
               mCheckbox.setVisibility(View.GONE);
               mCheckbox.setChecked(false);
              // PdfDownload.setVisibility(View.GONE);
               backArrow.setVisibility(View.GONE);
               menuVs.setVisibility(View.VISIBLE);
               userSelection.clear();
//               listViewAdapter.clearIsChecked();
//               listViewAdapter.notifyDataSetChanged();

           }
       });





        signout_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("islogin",false).commit();

                final Snackbar mySnackbar = Snackbar.make(findViewById(R.id.Product_Model_List_Activity), getString(R.string.channel_network_sign_out), Snackbar.LENGTH_INDEFINITE);
                mySnackbar.setActionTextColor(getResources().getColor(R.color.red));
                mySnackbar.setAction(getString(R.string.okay_button), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mySnackbar.dismiss();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                });
                mySnackbar.show();
            }
        });

        final ImageView downArrow=includeviewLayout.findViewById(R.id.down_arrow);
        final RelativeLayout userData=includeviewLayout.findViewById(R.id.user_data);
//        ViewGroup homeLayout=findViewById(R.id.Product_Model_List_Activity);

        downArrow.setOnClickListener(new View.OnClickListener() {
            boolean visible;
            @Override
            public void onClick(View v) {
                ChangeBounds changeBounds=new ChangeBounds();
                changeBounds.setDuration(600L);
                TransitionManager.beginDelayedTransition(includeviewLayout,changeBounds);
                visible = !visible;

                if(visible)
                    downArrow.animate().rotation(180).setInterpolator(new LinearInterpolator()).setDuration(500);
                else
                    downArrow.animate().rotation(0).setInterpolator(new LinearInterpolator()).setDuration(500);
                userData.setVisibility(visible? View.VISIBLE: View.GONE);
            }
        });


//        new Vechiclename().execute();


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(Product_Model_List_Activity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        menuVs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Product_Model_List_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(Product_Model_List_Activity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;
                            case R.id.viewprofile:
                                startActivity(new Intent(Product_Model_List_Activity.this, ViewProfileActivity.class));
                                return true;
                            case R.id.aboutpop:
                                startActivity(new Intent(Product_Model_List_Activity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(Product_Model_List_Activity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Product_Model_List_Activity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Product_Model_List_Activity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Product_Model_List_Activity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(Product_Model_List_Activity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Product_Model_List_Activity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Product_Model_List_Activity.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Product_Model_List_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(Product_Model_List_Activity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Model_List_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();
            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent a = new Intent(Product_Model_List_Activity.this, Product_Grid_Activity.class);
                a.putExtra("OEM", oem_name);
                a.putExtra("MODEL", model.get(i).toString());
                a.putExtra("navitxt",navicationtxt.getText()+"  >  "+model.get(i).toString().trim());
                a.putExtra("SEGMENT", vehicle_type);
                startActivity(a);
            }
        });

        Animation makeInAnimation = AnimationUtils.makeInAnimation(getBaseContext(), false);
        makeInAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @SuppressLint("RestrictedApi")
            @Override
            public void onAnimationStart(Animation animation) {
                home.setVisibility(View.VISIBLE);
            }
        });

        checkInternet();
    }

    private void checkInternet() {
        Network_Connection_Activity network_connection_activity = new Network_Connection_Activity(Product_Model_List_Activity.this);
        if (network_connection_activity.CheckInternet()) {
            getModelList();
        } else {
            box.showAlertWithBack("Unable to connect Internet. Please check your Internet connection !");
        }
    }

    private void getModelList() {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(Product_Model_List_Activity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetrofitClient.getApiService();

            Call<StringListModel> call = service.getModelList(vehicle_type,oem_name);

            call.enqueue(new Callback<StringListModel>() {
                @Override
                public void onResponse(Call<StringListModel> call, Response<StringListModel> response) {
                    progressDialog.dismiss();

                    try {

                        if (response.isSuccessful()) {
                            if (response.body().getResult().equals("Success")) {

                                model.clear();
                                model.addAll(response.body().getData());
                                listViewAdapter = new ListViewAdapter(Product_Model_List_Activity.this, response.body().getData());
                                listView.setAdapter(listViewAdapter);


                            } else {
                                box.showAlertWithBack("No Record Found");
                            }
                        } else {
                            box.showAlertWithBack("Could not Connect to Server. Please try again Later");
                        }
                    } catch (Exception e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        box.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }


                @Override
                public void onFailure(Call<StringListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    box.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
            box.showAlertWithBack("Exception Occurred. Please try again Later");
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

//        if (myshare.getBoolean("islogin", false))
//            signout_relativelayout.setVisibility(View.VISIBLE);
//        else
//            signout_relativelayout.setVisibility(View.GONE);
//
//        userSelection.clear();
//        actionMode = null;
//        mCheckbox.setVisibility(View.GONE);
//      //  PdfDownload.setVisibility(View.GONE);
//        backArrow.setVisibility(View.GONE);
//        menuVs.setVisibility(View.VISIBLE);
//        mCheckbox.setChecked(false);
//
//        isActionmode = false;

    }

//    @Override
//    public void selectedModel(List<Model> modelList) {
//    if(!(modelList.size()==0)) {
//      pdfGenerator.setMake(makeStr);
////      this.selectModeList = modelList;
//      pdfGenerator.setSegment(vehicle_type);
//      pdfGenerator.setModel(modelList);
//          if(Country.equals("All Countries"))
//              pdfGenerator.setCountry("All");
//          else
//              pdfGenerator.setCountry(Country);
//    }else
//        pdfGenerator.setModel(null);
//    }

//    @Override
//    public void onSearchStateChanged(boolean enabled) {
//
//    }
//
//    @Override
//    public void onSearchConfirmed(CharSequence text) {
//
//    }
//
//    @Override
//    public void onButtonClicked(int buttonCode) {
//
//    }

//    class Searchmodel extends AsyncTask<String, Void, String> {
//        private ProgressDialog loading;
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            model.clear();
//            loading = ProgressDialog.show(Product_Model_List_Activity.this, getString(R.string.searching), getString(R.string.please_wait), false, false);
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//        String modelList = strings[0];
//            AppDatabase appDb = getAppDatabase(Product_Model_List_Activity.this);
//            SQLiteHelper dbHelper = new SQLiteHelper(Product_Model_List_Activity.this);
//            db = dbHelper.getReadableDatabase();
//            if (vehicle_type.equals("Passenger Car")) {
////                selectQuery = "SELECT distinct Model from product where  Segment in ('Passenger Car', 'Light Commercial')  and Make ='" + oem_name + "' order by  Model asc";
//                selectQuery = "SELECT distinct Model from product where  Segment in ('Passenger Car', 'Light Commercial','รถโดยสาร')  and Make ='" + oem_name + "' and Model like '%"+modelList+"%' order by  Model asc";
//            } else {
//                selectQuery = "SELECT distinct Model from product where  like Model '"+modelList+"%' and Segment ='" + vehicle_type + "' and Make ='" + oem_name + "' order by  Model asc";
//
//            }
//
//            Log.v("Vehicle Model", selectQuery);
//            c = db.rawQuery(selectQuery, null);
//
//            if (c.moveToFirst()) {
//                do {
//
//                    int count=0;
//                    model.add(c.getString(c.getColumnIndex("Model")));
//                    if (vehicle_type.equals("Passenger Car"))
//                    {
//                        count=appDb.ProductsDAO().getModelsNewProductsCount("Passenger Car","Light Commercial", c.getString(c.getColumnIndex("Model")),"");
//                    }
//                    else {
//                        count=appDb.ProductsDAO().getModelNewProductsCount(vehicle_type, c.getString(c.getColumnIndex("Model")),"");
//                    }
//
//                    modelCount.add(count);
//                }
//                while (c.moveToNext());
//
//            }
//            c.close();
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            loading.dismiss();
//            listView.setAdapter(new ListViewAdapter(Product_Model_List_Activity.this, model,modelCount,CheckVal,true));
//        }
//
//    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        isActionmode =false;
        super.onBackPressed();
    }
}
