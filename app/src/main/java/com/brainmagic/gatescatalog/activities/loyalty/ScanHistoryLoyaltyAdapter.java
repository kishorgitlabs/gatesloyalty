package com.brainmagic.gatescatalog.activities.loyalty;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.gatescatalog.api.models.scanhistoryloyalty.DataItem;
import com.brainmagic.gatescatalog.gatesloyalty.R;

import java.util.List;

public class ScanHistoryLoyaltyAdapter extends ArrayAdapter {

    Context mContext;
    List<DataItem> dataItemList;

    public ScanHistoryLoyaltyAdapter(Context context, List<DataItem> dataItems) {
        super(context, R.layout.scan_history_loyalty);
        this.mContext = context;
        this.dataItemList = dataItems;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = null;
        if (convertView == null) {

            convertView = LayoutInflater.from(mContext).inflate(R.layout.scan_history_loyalty, null);

            TextView date = convertView.findViewById(R.id.date_history_loyalty);
            TextView serialNumber = convertView.findViewById(R.id.p_number_history_loyalty);
            TextView status = convertView.findViewById(R.id.scan_result_history_loyalty);
            TextView point = convertView.findViewById(R.id.scan_point_history_loyalty);
            TextView validity = convertView.findViewById(R.id.validity_history_loyalty);

            String Date = dataItemList.get(position).getInsertDate();
            String getdate[] = Date.split("T");
            String datagetted = getdate[0];
            String dateorder[] = datagetted.split("-");

            String SerialNumber = dataItemList.get(position).getCouponcode();
            String Status = dataItemList.get(position).getScanstatus();
            String Point = dataItemList.get(position).getRewardPoint();
            String Validity = String.valueOf(dataItemList.get(position).getValidity());



            date.setText(dateorder[2] + "/" + dateorder[1] + "/" + dateorder[0]);
            serialNumber.setText(SerialNumber);
            status.setText(Status);
            if (Validity == null) {
                validity.setText("0");
            } else {
                validity.setText(Validity);
            }
            if (Point.equals("0")) {
                point.setText("-");
            } else {
                point.setText(Point);
            }

            if (Status.equals("Already Scanned")){
                point.setText("0");
            }else {
                point.setText(Point);
            }

           /* if (dataItemList.get(position).getScanstatus().equals("Already Scanned")){
//                points.setTextColor(R.color.red);
                status.setText(dataItemList.get(position).getScanstatus());
            }else if (dataItemList.get(position).getScanstatus().equals("Success")){
//                points.setTextColor(R.color.green);
                status.setText(dataItemList.get(position).getScanstatus());
            }else {
                status.setText(dataItemList.get(position).getScanstatus());
            }

            point.setText(dataItemList.get(position).getRewardPoint());*/

        }
        return convertView;
    }

    @Override
    public int getCount() {
        return dataItemList.size();
    }
}
