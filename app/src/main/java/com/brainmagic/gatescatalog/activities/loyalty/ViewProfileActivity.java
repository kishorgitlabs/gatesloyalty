package com.brainmagic.gatescatalog.activities.loyalty;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributor_Search_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.RegisterActivity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.editprofile.EditProfile;
import com.brainmagic.gatescatalog.api.models.newcheckregister.CheckRegisterMobileNo;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.plus.PlusOneDummyView.TAG;

public class ViewProfileActivity extends AppCompatActivity {


    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private LinearLayout gstNoLayout, shopNameLayout;
    TextView mobileno, name, busineesname, usertype, shopname, streetname;
    EditText state, city, zipCode, email, streetName, country, alternativeMobileNo, gstNo, shopName;
    Button updateProfileBtn;
    private ImageView menu;
    private FloatingActionButton back, home;
    String user_id, user_mobileno, user_name, user_email, user_businesname, user_type, user_country, user_state, user_streetname, user_city, user_zipcode;
    String State, City, Address, AlternativeMobileNo, MobileNo;

    private boolean permissionGranted = true;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private SettingsClient mSettingsClient;
    private Boolean mRequestingLocationUpdates;
    private FusedLocationProviderClient mFusedLocationClient;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 50000;
    private static final int REQUEST_CHECK_SETTINGS = 100;
    private static final int REQUEST_CHECK_SETTINGS_GPS = 100;
    private String locationaddress;

    private Alertbox alertbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        mobileno = findViewById(R.id.edit_profile_mobile_no);
        name = findViewById(R.id.edit_profile_name);
        email = findViewById(R.id.edit_profile_email);
        busineesname = findViewById(R.id.edit_profile_business_name);
        usertype = findViewById(R.id.edit_profile_user_type);
        state = findViewById(R.id.edit_profile_state);
        city = findViewById(R.id.edit_profile_city);
        country = findViewById(R.id.edit_profile_country);
        zipCode = findViewById(R.id.edit_profile_zip_code);
        streetName = findViewById(R.id.edit_profile_street_name);
        updateProfileBtn = findViewById(R.id.edit_profile_update_btn);
        alternativeMobileNo = findViewById(R.id.edit_profile_alternative_mobile_no);
        gstNoLayout = findViewById(R.id.gstin_layout);
        shopNameLayout = findViewById(R.id.shop_name_layout);
        gstNo = findViewById(R.id.edit_profile_gstin_no);
        shopName = findViewById(R.id.edit_profile_shop_name);
//        shopname = findViewById(R.id.shop_name);
//        streetname = findViewById(R.id.edit_profile_street_name);
        menu = findViewById(R.id.menu);
        back = findViewById(R.id.back);
        home = findViewById(R.id.home);
        alertbox = new Alertbox(ViewProfileActivity.this);

        try {
            user_id = (myshare.getString("id", ""));
            user_mobileno = myshare.getString("MobNo", "");
            user_name = myshare.getString("NameCheck", "");
            user_email = myshare.getString("Email", "");
            user_businesname = myshare.getString("Bname", "");
            user_type = myshare.getString("Usertype", "");
            user_streetname = myshare.getString("streetName", "");
            user_city = myshare.getString("city", "");
            user_state = myshare.getString("state", "");
            user_zipcode = myshare.getString("zipCode", "");
            user_country = myshare.getString("country", "");
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

        mobileno.setText(user_mobileno);
        name.setText(user_name);
//        email.setText(user_email);
        busineesname.setText(user_businesname);
        usertype.setText(user_type);
        state.setText(user_state);
        country.setText(user_country);
        city.setText(user_city);
        zipCode.setText(user_zipcode);
//        streetName.setText(user_streetname);

        if (usertype.getText().toString().equals("Dealer / Retailer")) {
            shopNameLayout.setVisibility(View.VISIBLE);
            gstNoLayout.setVisibility(View.VISIBLE);
        } else {
            shopNameLayout.setVisibility(View.GONE);
            gstNoLayout.setVisibility(View.GONE);
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ViewProfileActivity.this, HomeScreenActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                );
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(ViewProfileActivity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub
                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(ViewProfileActivity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(ViewProfileActivity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(ViewProfileActivity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(ViewProfileActivity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
                            case R.id.scan:
                                startActivity(new Intent(ViewProfileActivity.this, ScanActivity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(ViewProfileActivity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(ViewProfileActivity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(ViewProfileActivity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(ViewProfileActivity.this, Distributor_Search_Activity.class));
                                return true;
                            case R.id.viewprofile:
                                startActivity(new Intent(ViewProfileActivity.this, ViewProfileActivity.class));
                                return true;

                            case R.id.contactpop:
                                startActivity(new Intent(ViewProfileActivity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;
                            case R.id.gates_employee_logout:
                                /*MenuItem menuItem = menus.findItem(R.id.gates_employee_logout);
                                menuItem.setVisible(true);*/
                                editor.clear();
                                editor.commit();
                                startActivity(new Intent(ViewProfileActivity.this, RegisterActivity.class));
//                                alertbox.showAlert("Successful Logout");
                                return true;

                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();
            }
        });

        initPermission();

        updateProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                State = state.getText().toString();
                City = city.getText().toString();
                Address = streetName.getText().toString();
                MobileNo = mobileno.getText().toString();
                AlternativeMobileNo = alternativeMobileNo.getText().toString();

                if (State.equalsIgnoreCase("")) {
                    state.setError("Enter State");
                } else if (City.equalsIgnoreCase("")) {
                    city.setError("Enter City");
                } else if (Address.equalsIgnoreCase("")) {
                    streetName.setError("Enter Address");
                } else if (AlternativeMobileNo.equals(MobileNo)) {
                    Toast.makeText(ViewProfileActivity.this, "Registered Mobile No not same as Alternative Mobile No", Toast.LENGTH_LONG).show();
                } else {
                    updateProfile();
                }
            }
        });

        setProfile();
    }

    private void setProfile() {
        try {

            final ProgressDialog loading = ProgressDialog.show(ViewProfileActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();

            Call<CheckRegisterMobileNo> call = service.newcheckreg(user_mobileno);

            call.enqueue(new Callback<CheckRegisterMobileNo>() {
                @Override
                public void onResponse(Call<CheckRegisterMobileNo> call, Response<CheckRegisterMobileNo> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        name.setText(response.body().getData().getName());
                        email.setText(response.body().getData().getEmail());

                        alternativeMobileNo.setText(response.body().getData().getAlterMblno());
                        busineesname.setText(response.body().getData().getBusinessName());
                        country.setText(response.body().getData().getCountry());
                        state.setText(response.body().getData().getState());
                        city.setText(response.body().getData().getCity());

                        editor.putString("alternativeNo", response.body().getData().getAlterMblno());
                        editor.commit();
                    } else if (response.body().getResult().equals("Not Success")) {
                        Toast.makeText(ViewProfileActivity.this, "Please try again later", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<CheckRegisterMobileNo> call, Throwable t) {
                    loading.dismiss();
                    Toast.makeText(ViewProfileActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(ViewProfileActivity.this, getString(R.string.internet_error_msg), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void updateProfile() {
        try {
            final ProgressDialog loading = ProgressDialog.show(ViewProfileActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();

            Call<EditProfile> call = service.editProfile(
                    String.valueOf(user_id),
                    user_mobileno,
                    email.getText().toString(),
                    alternativeMobileNo.getText().toString(),
                    user_type,
                    State,
                    City,
                    Address
            );

            call.enqueue(new Callback<EditProfile>() {
                @Override
                public void onResponse(Call<EditProfile> call, Response<EditProfile> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")) {

                        alertbox.showAlertWithBack("Successfully Profile Updated");
                    } else if (response.body().getResult().equals("Not Success")) {
                        alertbox.showAlert("Please try later");
                    } else {
                        alertbox.showAlert("Please try later !");
                    }
                }

                @Override
                public void onFailure(Call<EditProfile> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showAlertWithBack(getString(R.string.server_error));

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            alertbox.showAlert(getString(R.string.internet_error_msg));
        }
    }

    private void initPermission() {
        if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(ViewProfileActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(ViewProfileActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 100);
        } else {
            permissionGranted = true;
            init();
            startLocationUpdate();
        }
    }

    private void startLocationUpdate() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");


                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());


                        updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(ViewProfileActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(ViewProfileActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                                break;
                            case LocationSettingsStatusCodes.SUCCESS:
                                permissionGranted = true;
                                init();
                        }
                        updateLocationUI();
                    }
                });
    }

    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            new GeocodeAsyncTask().execute(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        }
    }

    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {

        String errorMessage = "";

        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(ViewProfileActivity.this, Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);

                    Log.d(TAG, "doInBackground: ************");
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }
                if (addresses != null && addresses.size() > 0)

                    return addresses.get(0);


            }
            return null;
        }

        protected void onPostExecute(Address address) {

            if (address == null) {
                Log.d(TAG, "onPostExecute: *****");
            } else {
                String address1 = address.getAddressLine(0);


                String StreetName = address.getAddressLine(0);
                streetName.setText(StreetName);

                user_city = address.getLocality();
                city.setText(user_city);


                String city = address.getLocality();
                user_state = address.getAdminArea();
                state.setText(user_state);


                String Country = address.getCountryName();
                country.setText(Country);


                user_zipcode = address.getPostalCode();
                zipCode.setText(user_zipcode);


                //create your custom title
//                String title = city + "-" + state;
//                streetName.setText(address +
//                        "\n"
//                        + title);
//                locationaddress = addressss.getText().toString();
                Geocoder geocoder = new Geocoder(ViewProfileActivity.this);
                try {
                    ArrayList<Address> addresses = (ArrayList<Address>) geocoder.getFromLocationName("karur", 50);
                    for (Address address3 : addresses) {
                        double lat = address3.getLatitude();
                        double lon = address3.getLongitude();

                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }


            }

        }
    }

    private void init() {
        if (permissionGranted) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mSettingsClient = LocationServices.getSettingsClient(this);

            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    // location is received
                    mCurrentLocation = locationResult.getLastLocation();
//                    mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                    updateLocationUI();
                }
            };

            mRequestingLocationUpdates = false;
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setFastestInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(mLocationRequest);
            mLocationSettingsRequest = builder.build();
//        startLocationButtonClick();
        }
    }
}