package com.brainmagic.gatescatalog.activities.loyalty;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.gates.addretailer.AddRetailer;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddRetailerActivity extends AppCompatActivity {

    private EditText retailerName, retailerEmail, retailerGst, retailerMobileNo, retailerShopName;
    private Spinner country, state, city;
    private Button addRetailer;

    private String RetailerName, RetailerEmail, RetailerGst, RetailerMobileNo, RetailerShopName, Country, State, City;
    private String[] countryList = {"India", "India"};
    private String[] stateList = {"SELECT STATE", "Tamil Nadu", "karnataka", "Telungana"};
    private String[] cityList = {"Chennai", "Bangalore", "Hyderabad"};
    private String[] tamilNaduCity = {"Chennai", "Madurai"};
    private String[] karnatakaCity = {"Bangalore"};
    private String[] telunganaCity = {"Hyderabad"};

    private FloatingActionButton back, home;
    ArrayAdapter<String> cityAdapter;

    private Alertbox alertbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_retailer);
        country = findViewById(R.id.country_add_retailer);
        retailerEmail = findViewById(R.id.email_add_retailer);
        retailerGst = findViewById(R.id.gst_no_add_retailer);
        retailerName = findViewById(R.id.retailer_name_add_retailer);
        retailerMobileNo = findViewById(R.id.mobileno_add_retailer);
        retailerShopName = findViewById(R.id.shop_name_add_retailer);
        country = findViewById(R.id.country_add_retailer);
        state = findViewById(R.id.state_add_retailer);
        city = findViewById(R.id.city_add_retailer);
        addRetailer = findViewById(R.id.add_retailer);
        back = findViewById(R.id.back);
        home = findViewById(R.id.home);
        country.setEnabled(false);
        alertbox = new Alertbox(AddRetailerActivity.this);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent home = new Intent(AddRetailerActivity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });


        ArrayAdapter<String> countryAdapter = new ArrayAdapter<>(AddRetailerActivity.this, android.R.layout.simple_list_item_1, countryList);
        country.setAdapter(countryAdapter);

        ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(AddRetailerActivity.this, android.R.layout.simple_list_item_1, stateList);
        state.setAdapter(stateAdapter);

//        ArrayAdapter<String> cityAdapter = new ArrayAdapter<>(AddRetailerActivity.this, android.R.layout.simple_list_item_1, cityList);
//        city.setAdapter(cityAdapter);

        state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String item = adapterView.getItemAtPosition(i).toString();
                if (item.equals("Tamil Nadu")) {
                    cityAdapter = new ArrayAdapter<>(AddRetailerActivity.this, android.R.layout.simple_list_item_1, tamilNaduCity);

                } else if (item.equals("karnataka")) {
                    cityAdapter = new ArrayAdapter<>(AddRetailerActivity.this, android.R.layout.simple_list_item_1, karnatakaCity);
                } else if (item.equals("Telungana")) {
                    cityAdapter = new ArrayAdapter<>(AddRetailerActivity.this, android.R.layout.simple_list_item_1, telunganaCity);
                }

                city.setAdapter(cityAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        addRetailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    RetailerName = retailerName.getText().toString().trim();
                    RetailerEmail = retailerEmail.getText().toString().trim();
                    RetailerGst = retailerGst.getText().toString().trim();
                    RetailerMobileNo = retailerMobileNo.getText().toString().trim();
                    RetailerShopName = retailerShopName.getText().toString().trim();
                    Country = country.getSelectedItem().toString().trim();
                    State = state.getSelectedItem().toString().trim();
//                City = city.getSelectedItem().toString().trim();

                    if (RetailerName.equals("")) {
                        retailerName.setError("Enter the Retailer Name");
                    } else if (RetailerEmail.equals("")) {
                        retailerEmail.setError("Enter the Retailer Email");
                    } else if (RetailerMobileNo.equals("")) {
                        retailerMobileNo.setError("Enter the Mobile Number");
                    } else if (RetailerGst.equals("")) {
                        retailerGst.setError("Enter the Shop GST Number");
                    } else if (RetailerShopName.equals("")) {
                        retailerShopName.setError("Enter the Retailer Shop Name");
                    } else if (State.equals("")) {
                        Toast.makeText(AddRetailerActivity.this, "Select State Name", Toast.LENGTH_LONG).show();
                    } else if (city.getSelectedItem().toString().equals("")
                            || city.getSelectedItem().toString() == null) {
                        Toast.makeText(AddRetailerActivity.this, "Select City Name", Toast.LENGTH_SHORT).show();
                    } else {
                        addretailer();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void addretailer() {
        try {
            final ProgressDialog loading = ProgressDialog.show(AddRetailerActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

            APIService service = RetrofitClient.getApiService();

            Call<AddRetailer> call = service.gatesAddRetailer(
                    RetailerName,
                    RetailerMobileNo,
                    RetailerEmail,
                    RetailerShopName,
                    RetailerGst,
                    Country,
                    State,
                    City
            );

            call.enqueue(new Callback<AddRetailer>() {
                @Override
                public void onResponse(Call<AddRetailer> call, Response<AddRetailer> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        alertbox.showAlertWithBack("Successfully Created");
                    } else if (response.body().getResult().equals("Already Exist")) {
                        alertbox.showAlert("User already added");
                    } else {
                        alertbox.showAlert("Please try again");
                    }
                }

                @Override
                public void onFailure(Call<AddRetailer> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showAlert("Please try again");
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            alertbox.showAlert("Please try again");
        }
    }
}