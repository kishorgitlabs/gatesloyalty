package com.brainmagic.gatescatalog.activities.loyalty;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.Sample;
import com.brainmagic.gatescatalog.api.models.gates.approve.Approve;
import com.brainmagic.gatescatalog.api.models.gates.pendinglist.Datum;
import com.brainmagic.gatescatalog.api.models.gates.pendinglist.PendingList;
import com.brainmagic.gatescatalog.api.models.gates.reject.Reject;
import com.brainmagic.gatescatalog.gatesloyalty.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApprovalAdapter extends BaseAdapter {

    private Context mContext;
    private List<Datum> sampleList;
    private Alertbox alertbox;

    public ApprovalAdapter(Context mContext, List<Datum> sampleList) {
        this.mContext = mContext;
        this.sampleList = sampleList;
    }

    @Override
    public int getCount() {
        return sampleList.size();
    }

    @Override
    public Object getItem(int position) {
        return sampleList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.approval_pending_view_layout, viewGroup, false);

        alertbox = new Alertbox(mContext);
        TextView retailerName = view.findViewById(R.id.approval_name);
        TextView mobileno = view.findViewById(R.id.approval_mobile_no);
        TextView email = view.findViewById(R.id.approval_email);
        TextView userType = view.findViewById(R.id.approval_user_type);
        TextView state = view.findViewById(R.id.approval_state);
        TextView shopName = view.findViewById(R.id.approval_shop_name);
        Button appove = view.findViewById(R.id.approve_retailer);
        Button delete = view.findViewById(R.id.delete_retailer);

        String RetailerName = sampleList.get(position).getName();
        String MobileNo = sampleList.get(position).getMobileno();
        String Email = sampleList.get(position).getEmail();
        String UserType = sampleList.get(position).getUsertype();
        String State = String.valueOf(sampleList.get(position).getState());
        String ShopName = String.valueOf(sampleList.get(position).getShopName());
        String id = String.valueOf(sampleList.get(position).getId());
        String GSTNo = String.valueOf(sampleList.get(position).getGstin());
        String RegistrationType = sampleList.get(position).getRegistrationType();
        String Address = sampleList.get(position).getAddress();

        SpannableString content = new SpannableString(RetailerName);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);

        retailerName.setText(content);
        mobileno.setText(MobileNo);
        email.setText(Email);
        userType.setText(UserType);
        state.setText(State);
        shopName.setText(ShopName);

        retailerName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(mContext, RetailerName, Toast.LENGTH_SHORT).show();
                alertbox.moreDetails(
                        RetailerName,
                        MobileNo,
                        UserType,
                        Email,
                        State,
                        ShopName,
                        GSTNo,
                        RegistrationType,
                        Address

                );
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                sampleList.remove(position);
//                getData(position);
//                notifyDataSetChanged();
                try {

                    final ProgressDialog loading = ProgressDialog.show(mContext, mContext.getResources().getString(R.string.app_name), "Loading...", false, false);

                    APIService service = RetrofitClient.getApiService();

                    Call<Reject> call = service.rejectReg(id);

                    call.enqueue(new Callback<Reject>() {
                        @Override
                        public void onResponse(Call<Reject> call, Response<Reject> response) {
                            loading.dismiss();
                            if (response.body().getResult().equals("Success")){
                                alertbox.showAlert("Successfully Rejected");
                                sampleList.remove(position);
                                notifyDataSetChanged();
                            }else {
                                alertbox.showAlertWithBack("Please try again later");
                            }
                        }

                        @Override
                        public void onFailure(Call<Reject> call, Throwable t) {
                            loading.dismiss();
                            alertbox.showAlertWithBack(mContext.getString(R.string.server_error));
                        }
                    });

                }catch (Exception e){
                    e.printStackTrace();
                    alertbox.showAlertWithBack(mContext.getString(R.string.internet_error_msg));
                }
            }
        });

        appove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    final ProgressDialog loading = ProgressDialog.show(mContext, mContext.getResources().getString(R.string.app_name), "Loading...", false, false);

                    APIService service = RetrofitClient.getApiService();

                    Call<Approve> call = service.approveReg(id);

                    call.enqueue(new Callback<Approve>() {
                        @Override
                        public void onResponse(Call<Approve> call, Response<Approve> response) {
                            loading.dismiss();
                            if (response.body().getResult().equals("Success")){
                                alertbox.showAlert("Successfully Approved");
                                sampleList.remove(position);
                                notifyDataSetChanged();
                            }else {
                                alertbox.showAlert("Please try again later");
                            }
                        }

                        @Override
                        public void onFailure(Call<Approve> call, Throwable t) {
                            loading.dismiss();
                            alertbox.showAlertWithBack(mContext.getString(R.string.server_error));
                        }
                    });

                }catch (Exception e){
                    e.printStackTrace();
                    alertbox.showAlertWithBack(mContext.getString(R.string.internet_error_msg));
                }
            }
        });

        return view;
    }

    public int getData(int position){

        return position;
    }
}
