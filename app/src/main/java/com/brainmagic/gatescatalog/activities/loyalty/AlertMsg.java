package com.brainmagic.gatescatalog.activities.loyalty;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

import com.brainmagic.gatescatalog.gatesloyalty.R;

public class AlertMsg {

    private Context mContext;

    public AlertMsg(Context mContext) {
        this.mContext = mContext;
    }


    public void forgetPassword(String mobileNo){
        try{
            AlertDialog alertDialog = new AlertDialog.Builder(
                    mContext).create();

            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.change_password_layout, null);
            alertDialog.setView(dialogView);
            EditText enterMobile = dialogView.findViewById(R.id.new_password);
            enterMobile.setHint("Enter the Mobile Number");



            Window window = alertDialog.getWindow();
            window.setLayout(600, 600);
            alertDialog.show();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
