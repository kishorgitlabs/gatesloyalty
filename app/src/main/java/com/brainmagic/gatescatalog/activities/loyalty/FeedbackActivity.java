package com.brainmagic.gatescatalog.activities.loyalty;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributor_Search_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.feedback.FeedBack;
import com.brainmagic.gatescatalog.gatesloyalty.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackActivity extends AppCompatActivity {

    EditText name, email, mobileNo, messgage;
    String NameSP, Email, mobilenum;
    Button sent;
    ImageView back,menu, home;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Alertbox alertbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        name = findViewById(R.id.name_feedback);
        email = findViewById(R.id.email_feedback);
        mobileNo = findViewById(R.id.mobileno_feedback);
        messgage = findViewById(R.id.msg_feedback);
        sent = findViewById(R.id.sent_feedback);
        back = findViewById(R.id.back);
        home = findViewById(R.id.home);
        menu = findViewById(R.id.menu);
        alertbox = new Alertbox(FeedbackActivity.this);

        sharedPreferences = getSharedPreferences("Registration", MODE_PRIVATE);
        NameSP = sharedPreferences.getString("Name", "");
        Email = sharedPreferences.getString("Email", "");
        mobilenum = sharedPreferences.getString("MobNo", "");
        editor = sharedPreferences.edit();


        name.setText(NameSP);
        email.setText(Email);
        mobileNo.setText(mobilenum);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FeedbackActivity.this, HomeScreenActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                );
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(FeedbackActivity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub
                        switch (item.getItemId()) {
                            case R.id.homepop:
//                                Intent home = new Intent(HomeScreenActivity.this, HomeScreenActivity.class);
//                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(FeedbackActivity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(FeedbackActivity.this, Product_Activity.class));
                                return true;
                            case R.id.viewprofile:
                                startActivity(new Intent(FeedbackActivity.this, ViewProfileActivity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(FeedbackActivity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(HomeScreenActivity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.scan:
                                startActivity(new Intent(FeedbackActivity.this, ScanActivity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(FeedbackActivity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(FeedbackActivity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(FeedbackActivity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(FeedbackActivity.this, Distributor_Search_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(HomeScreenActivity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(FeedbackActivity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();
            }
        });

        sent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Name = name.getText().toString();
                String Email = email.getText().toString();
                String MobileNo = mobileNo.getText().toString();
                String Msg = messgage.getText().toString();


                if (Name.equals("") || Email.equals("") || MobileNo.equals("") || Msg.equals("")) {

                    alertbox.showAlert("Fill the Details");

                } else {
                    final ProgressDialog loading = ProgressDialog.show(FeedbackActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

                    try {

                        APIService service = RetrofitClient.getApiService();

                        Call<FeedBack> call = service.feedBack(Name, Email, MobileNo, Msg);

                        call.enqueue(new Callback<FeedBack>() {
                            @Override
                            public void onResponse(Call<FeedBack> call, Response<FeedBack> response) {
                                loading.dismiss();
                                if (response.body().getResult().equals("Success")) {


                                    final AlertDialog alertDialog = new AlertDialog.Builder(
                                            FeedbackActivity.this).create();

                                    LayoutInflater inflater = (FeedbackActivity.this).getLayoutInflater();
                                    View dialogView = inflater.inflate(R.layout.alertbox, null);
                                    alertDialog.setView(dialogView);
                                    ImageView sucess = (ImageView) dialogView.findViewById(R.id.success);
                                    TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                                    Button okay = (Button) dialogView.findViewById(R.id.okay);
                                    log.setText("You Successfully sent feedback");
                                    sucess.setImageResource(R.drawable.nointernet);
                                    okay.setOnClickListener(new View.OnClickListener() {

                                        @Override
                                        public void onClick(View arg0) {
                                            // TODO Auto-generated method stub
                                            alertDialog.dismiss();
                                            name.getText().clear();
                                            email.getText().clear();
                                            mobileNo.getText().clear();
                                            messgage.getText().clear();

                                        }
                                    });
                                    Window window = alertDialog.getWindow();
                                    window.setLayout(600, 600);
                                    alertDialog.show();


                                } else {
                                    alertbox.showAlert("Please try again..");
                                }
                            }

                            @Override
                            public void onFailure(Call<FeedBack> call, Throwable t) {
                                alertbox.showAlert(t.getMessage());
                            }
                        });

                    } catch (Exception e) {
                        loading.dismiss();
                        alertbox.showAlert(e.toString());
                    }


                }
            }
        });
    }
}
