package com.brainmagic.gatescatalog.activities.productsearch;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.gatescatalog.activities.loyalty.ViewProfileActivity;
import com.bumptech.glide.Glide;
import com.brainmagic.gatescatalog.Constants;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.ArrayList;

import uk.co.senab.photoview.PhotoView;


public class Product_More_Details_Activity extends Activity
//        implements ImageAdapter.GetImage
{
    private PopupWindow mPopupWindow;
    private RelativeLayout mRelativeLayout;
    private ImageButton closeButton;
    private SQLiteDatabase db;
//    private SwipeRefreshLayout swipeRefreshLayout;
    private Cursor c;
    private PhotoView photoView;
    private String vehicle_type, engine_code, year_from, year_to, month_from, month_to, stroke, addtional="", equipment1, equipment2, equipmentDateFrom, equipmentDateTo;
    private String oem_name, model_code, model_name, part_no,list_imagename,internetcheck;
//    private List<LogDetailResult> thumbImage;
    private String ImageList = Constants.ROOT_URL+"/ImageUpload/";
    private ImageView product_image;
    private ImageView menu;
//    int imgUrl;
    private RelativeLayout imageView;
    ImageView rifhtbtn,leftbtn;
//    HorizontalScrollView hsv;
//    private ImageView IMageView;
    private ArrayList<Integer> mImage = new ArrayList<>();
    private String MonthYearFrom,MonthYearTill;
    private TextView list_modelcode,  list_model, list_monthfrom,  list_monthtill, list_partdescription, list_oem, list_additional,
            list_gatespartno, mrp, list_engine_code, list_appAtt, list_stroke, list_cc, list_kw, list_equipment_from_date, list_vehicleType,  list_equipment_to_date;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String Email, Mobno, Usertype,Country;
    private Alertbox box = new Alertbox(Product_More_Details_Activity.this);
    private View customView;
    private TextView ProfileName;
//    private ConstraintLayout signout_relativelayout;
    private LinearLayout signout_relativelayout;
    private FloatingActionButton back, home;
    private ViewGroup includeviewLayout;
    private ImageView profilePicture;
    private TextView navicationtxt;
//    private String LocalPath;
//    private ScrollView scroll_list;
    private TextView note,supportedCountriesList;
    private String yearfromstng,yeartostng;
//    private String equipment1srng,equipment2srng;
    private RelativeLayout supportedCountriesLayout;
    private String fullImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_more_details);

        mRelativeLayout = (RelativeLayout)findViewById(R.id.imagecapture);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        menu = (ImageView) findViewById(R.id.menu);
        navicationtxt = (TextView) findViewById(R.id.navicationtxt);
     //   hsv = (HorizontalScrollView)findViewById(R.id.horilist);
        Intent i =getIntent();
//        imgUrl = i.getIntExtra("URL",0);
        imageView = (RelativeLayout)findViewById(R.id.tab5);
//        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe);
        includeviewLayout = findViewById(R.id.profile_logo);
        ProfileName = (TextView) includeviewLayout.findViewById(R.id.profilename);
        signout_relativelayout =  findViewById(R.id.signout);
        profilePicture = (ImageView) includeviewLayout.findViewById(R.id.profilepicture);
        signout_relativelayout.setVisibility(View.GONE);
        product_image = (ImageView) findViewById(R.id.product_image);
        product_image = (ImageView) findViewById(R.id.product_image);
        product_image = (ImageView) findViewById(R.id.product_image);
//        scroll_list = (ScrollView) findViewById(R.id.scroll_list);
        supportedCountriesList=findViewById(R.id.list_supportcountry);
        supportedCountriesLayout=findViewById(R.id.supported_country_layout);
        customView = inflater.inflate(R.layout.imagepopup, null);
        mPopupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
//        LocalPath = "/data/data/" + getPackageName() + "/app_gates/gates/";
        navicationtxt.setText(getIntent().getStringExtra("navtxt").toString());
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        if (Build.VERSION.SDK_INT >= 21) {

            mPopupWindow.setElevation(5.0f);

        }

        ImageButton closeButton = (ImageButton) customView.findViewById(R.id.close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopupWindow.dismiss();
            }
        });
        product_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
//
//                photoView = (PhotoView) customView.findViewById(R.id.pop_photo);
//
//
//                Picasso.with(Product_More_Details_Activity.this).load(fullImage).error(R.drawable.noimages).into(photoView);


            }
        });

        //To reload the image, user can

//        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
////                product_image.setImageResource(imgUrl);
//                finish();
//                startActivity(getIntent());
//              //  onRefresh();
////                fullImage=ImageList+list_imagename+".jpg";
////                Picasso.with(Product_More_Details_Activity.this).load(fullImage).error(R.drawable.noimages).into(product_image);
////                recreate();
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        swipeRefreshLayout.setRefreshing(false);
//
//                    }
//                },4000);
//            }
//        });
        Email = myshare.getString("email", "");
        Mobno = myshare.getString("MobNo", "");
        Usertype = myshare.getString("Usertype", "");
        Country = myshare.getString("Country", "");
        ProfileName.setText(myshare.getString("name", "")+" , "+Country);
        if (!myshare.getString("profile_path", "").equals(""))
            Glide.with(Product_More_Details_Activity.this)
                    .load(new File(myshare.getString("profile_path", "")))
                    .into(profilePicture);

        if (myshare.getBoolean("islogin", false))
            signout_relativelayout.setVisibility(View.VISIBLE);
        else
            signout_relativelayout.setVisibility(View.GONE);

        signout_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("islogin", false).commit();
                final Snackbar mySnackbar = Snackbar.make(findViewById(R.id.imagecapture), getString(R.string.channel_network_sign_out), Snackbar.LENGTH_INDEFINITE);
                mySnackbar.setActionTextColor(getResources().getColor(R.color.red));
                mySnackbar.setAction(getString(R.string.okay_button), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mySnackbar.dismiss();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                });
                mySnackbar.show();
            }
        });

        final ImageView downArrow=includeviewLayout.findViewById(R.id.down_arrow);
        final RelativeLayout userData=includeviewLayout.findViewById(R.id.user_data);
        ViewGroup homeLayout=findViewById(R.id.imagecapture);

//        downArrow.setOnClickListener(new View.OnClickListener() {
//            boolean visible;
//            @Override
//            public void onClick(View v) {
////                TransitionManager.beginDelayedTransition(includeviewLayout);
//                ChangeBounds changeBounds=new ChangeBounds();
//                changeBounds.setDuration(600L);
//                TransitionManager.beginDelayedTransition(includeviewLayout,changeBounds);
//                visible = !visible;
//                if(visible)
//                    downArrow.animate().rotation(180).setInterpolator(new LinearInterpolator()).setDuration(500);
//                else
//                    downArrow.animate().rotation(0).setInterpolator(new LinearInterpolator()).setDuration(500);
//                userData.setVisibility(visible? View.VISIBLE: View.GONE);
//            }
//        });


        list_oem = (TextView) findViewById(R.id.list_oem);
        list_vehicleType = (TextView) findViewById(R.id.list_vehicleType);
        list_modelcode = (TextView) findViewById(R.id.list_model_code);
        list_model = (TextView) findViewById(R.id.list_model);
        list_stroke = (TextView) findViewById(R.id.list_stroke);
        rifhtbtn =  findViewById(R.id.right);
        leftbtn =  findViewById(R.id.left);

        list_monthfrom = (TextView) findViewById(R.id.list_monthfrom);

        list_monthtill = (TextView) findViewById(R.id.list_monthtill);
        list_partdescription = (TextView) findViewById(R.id.list_partdescription);
        list_gatespartno = (TextView) findViewById(R.id.list_gatespartno);
        mrp = (TextView) findViewById(R.id.mrp);

        list_appAtt = (TextView) findViewById(R.id.app_att);
        list_stroke = (TextView) findViewById(R.id.stroke);
        list_engine_code = (TextView) findViewById(R.id.list_engine_code);


        list_cc = (TextView) findViewById(R.id.cc_value);
        list_kw = (TextView) findViewById(R.id.kw_value);
        list_additional = (TextView) findViewById(R.id.list_addtional);

        note = (TextView) findViewById(R.id.list_addtional);

        oem_name = getIntent().getStringExtra("OEM");
        model_code = getIntent().getStringExtra("MODEL_CODE");
        model_name = getIntent().getStringExtra("MODEL");
        part_no = getIntent().getStringExtra("GATESPART");
        engine_code = getIntent().getStringExtra("ENGINE_CODE");
        internetcheck = getIntent().getStringExtra("internet");
//        thumbImage = (List<LogDetailResult>) getIntent().getSerializableExtra("thumbuimage");
        list_oem.setText(getIntent().getStringExtra("OEM"));
        list_modelcode.setText(getIntent().getStringExtra("MODEL_CODE"));
        list_model.setText(getIntent().getStringExtra("MODEL"));
        list_partdescription.setText(getIntent().getStringExtra("PARTDESC"));
        list_gatespartno.setText(getIntent().getStringExtra("GATESPART"));
        list_appAtt.setText(getIntent().getStringExtra("APPATT"));
        list_stroke.setText(getIntent().getStringExtra("STROKE"));
        list_cc.setText(getIntent().getStringExtra("CC"));
        list_kw.setText(getIntent().getStringExtra("KW"));
      //  list_imagename =getIntent().getStringExtra("IMAGENAME");
        yearfromstng = getIntent().getStringExtra("yearfrom");
        yeartostng = getIntent().getStringExtra("yearto");

//        equipment1srng =  getIntent().getStringExtra("EQUIPMENT1");
//        equipment2srng =  getIntent().getStringExtra("EQUIPMENT2");

//        if(thumbImage==null){
//            imageView.setVisibility(View.GONE);
//        }else if(thumbImage.size() <4) {
//            rifhtbtn.setVisibility(View.INVISIBLE);
//            leftbtn.setVisibility(View.INVISIBLE);
//            if(thumbImage.size()==1)
//                imageView.setVisibility(View.GONE);
//            else
//                imageView.setVisibility(View.VISIBLE);
//        }else {
//            imageView.setVisibility(View.VISIBLE);
//        }

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(Product_More_Details_Activity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Product_More_Details_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(Product_More_Details_Activity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;
                            case R.id.viewprofile:
                                startActivity(new Intent(Product_More_Details_Activity.this, ViewProfileActivity.class));
                                return true;
                            case R.id.aboutpop:
                                startActivity(new Intent(Product_More_Details_Activity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(Product_More_Details_Activity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Product_More_Details_Activity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Product_More_Details_Activity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Product_More_Details_Activity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(Product_More_Details_Activity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Product_More_Details_Activity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Product_More_Details_Activity.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Product_More_Details_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(Product_More_Details_Activity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Model_List_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });


//        GetProduct_Deatils();
        initalImage();
    }
    private void initalImage() {

//        mImage.add((R.drawable.noimages));
//        mImage.add(R.drawable.editprofile);
//        mImage.add(R.drawable.gatesmap);
//        mImage.add(R.drawable.youtube);
//        mImage.add(R.drawable.bike);
//        mImage.add(R.drawable.title_image);
        initialRecycler();
    }

    private void initialRecycler() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        final RecyclerView recyclerView = findViewById(R.id.recycleview1);

        recyclerView.setLayoutManager(linearLayoutManager);
//        ImageAdapter imageAdapter = new ImageAdapter(this, thumbImage);
//        ImageAdapter imageAdapter = new ImageAdapter(this, productImageList);
//        recyclerView.setAdapter(imageAdapter);

        rifhtbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*recyclerView.setLayoutManager(linearLayoutManager);
                linearLayoutManager.scrollToPosition(mImage.size()+1);*/
                recyclerView.scrollBy(100,0);
            }
        });

        leftbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                  recyclerView.smoothScrollToPosition(0);
            }
        });

//        product_image.setImageResource(imgUrl);
//        Picasso.with(Product_More_Details_Activity.this).load(imgUrl).error(R.drawable.noimages).into(product_image);
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (myshare.getBoolean("islogin", false))
            signout_relativelayout.setVisibility(View.VISIBLE);
        else
            signout_relativelayout.setVisibility(View.GONE);


    }


    public File getImage(String imagename) {

        File mediaImage = null;
        Bitmap b = null;
        try {
            // String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(myshare.getString("ImagePath", ""));
            if (!myDir.exists())
                return null;

            mediaImage = new File(myDir.getPath() + "/" + imagename + ".jpg");
            if (mediaImage.getAbsolutePath() != null)
                b = BitmapFactory.decodeFile(mediaImage.getAbsolutePath());

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mediaImage;
    }




    private String getSupportedCountries(int singapore, int iscambodia, int isbrunei, int isindonesia, int malaysia, int ismyanmar, int isphilippines, int isthailand, int isvietnam ){

        String supportedCountries="";
        if(singapore==1)
        {
            supportedCountries+="Singapore";
        }
        if(iscambodia==1)
        {
            if(!supportedCountries.isEmpty())
            {
                supportedCountries+=", ";
            }
            supportedCountries+="Cambodia";
        }
        if(isbrunei==1)
        {
            if(!supportedCountries.isEmpty())
            {
                supportedCountries+=", ";
            }
            supportedCountries+="Brunei";
        }
        if(isindonesia==1)
        {
            if(!supportedCountries.isEmpty())
            {
                supportedCountries+=", ";
            }
            supportedCountries+="Indonesia";
        }
        if(malaysia==1)
        {
            if(!supportedCountries.isEmpty())
            {
                supportedCountries+=", ";
            }
            supportedCountries+="Malaysia";
        }
        if(ismyanmar==1)
        {
            if(!supportedCountries.isEmpty())
            {
                supportedCountries+=", ";
            }
            supportedCountries+="Myanmar";
        }
        if(isphilippines==1)
        {
            if(!supportedCountries.isEmpty())
            {
                supportedCountries+=", ";
            }
            supportedCountries+="Philippines";
        }
        if(isthailand==1)
        {
            if(!supportedCountries.isEmpty())
            {
                supportedCountries+=", ";
            }
            supportedCountries+="Thailand";
        }
        if(isvietnam==1)
        {
            if(!supportedCountries.isEmpty())
            {
                supportedCountries+=", ";
            }
            supportedCountries+="Vietnam";
        }

            return supportedCountries;
    }


//    @Override
//    public void setOnImageClick(String image, int position) {
//        fullImage=image;
//        Picasso.with(Product_More_Details_Activity.this).load(image).error(R.drawable.noimages).into(product_image);
////        product_image.setImageResource(p);
////        imageAdapter.notifyDataSetChanged();
//    }
}
