package com.brainmagic.gatescatalog.activities.productsearch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainmagic.gatescatalog.Constants;
import com.brainmagic.gatescatalog.activities.loyalty.ViewProfileActivity;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributor_Search_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.fragment.ModelSuitsFragment;
import com.brainmagic.gatescatalog.activities.productsearch.fragment.OtherResourcesFragment;
import com.brainmagic.gatescatalog.activities.productsearch.fragment.ProductSpecFragment;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.adapter.ViewPagerAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeResult;
import com.brainmagic.gatescatalog.api.models.moredetails.MoreDetailsModel;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.senab.photoview.PhotoView;

public class MoreDetailsActivity extends AppCompatActivity {

    private RelativeLayout mRelativeLayout;
    private SQLiteDatabase db;
    //    private SwipeRefreshLayout swipeRefreshLayout;
    private Cursor c;
    private PhotoView photoView;
    private String vehicle_type, engine_code, year_from, year_to, month_from, month_to, stroke, addtional="", equipment1, equipment2, equipmentDateFrom, equipmentDateTo;
    private String oem_name, model_code, model_name, part_no,list_imagename,internetcheck;
    //    private List<LogDetailResult> thumbImage;
    private String ImageList = Constants.ROOT_URL+"/ImageUpload/";
    private ImageView product_image;
    private ImageView menu;
    //    int imgUrl;
    private RelativeLayout imageView;
    ImageView rifhtbtn,leftbtn;
    //    HorizontalScrollView hsv;
//    private ImageView IMageView;
    private ArrayList<Integer> mImage = new ArrayList<>();
    private String MonthYearFrom,MonthYearTill;
    private TextView list_modelcode,  list_model, list_monthfrom,  list_monthtill, list_partdescription, list_oem, list_additional,
            list_gatespartno, list_engine_code, list_appAtt, list_stroke, list_cc, list_kw, list_equipment_from_date, list_vehicleType,  list_equipment_to_date;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String Email, Mobno, Usertype,Country;
    private Alertbox box = new Alertbox(MoreDetailsActivity.this);
    private TextView ProfileName;
    //    private ConstraintLayout signout_relativelayout;
    private LinearLayout signout_relativelayout;
    private FloatingActionButton back, home;
    private ViewGroup includeviewLayout;
    private ImageView profilePicture;
    private TextView navicationtxt;
    //    private String LocalPath;
//    private ScrollView scroll_list;
    private TextView note,supportedCountriesList;
    private String yearfromstng,yeartostng;
    //    private String equipment1srng,equipment2srng;
    private RelativeLayout supportedCountriesLayout;
    private String fullImage;
    private Alertbox alertBox;
    private ViewPagerAdapter adapter;
    private ViewPager pager;
    private TabLayout layouts;
    private Thread thread;

    List<String> productImageList =null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_details);

        layouts = findViewById(R.id.tabs);
        pager=findViewById(R.id.viewPager);
        menu=findViewById(R.id.menu);
        navicationtxt = (TextView) findViewById(R.id.navicationtxt);
        back =  findViewById(R.id.back);
        home =  findViewById(R.id.home);

        alertBox = new Alertbox(MoreDetailsActivity.this);


        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        FragmentManager manager=getSupportFragmentManager();
        adapter=new ViewPagerAdapter(manager);

        checkInternet();
        navicationtxt.setText(getIntent().getStringExtra("navtxt"));



        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(MoreDetailsActivity.this, view);


                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub
                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(MoreDetailsActivity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;
                            case R.id.viewprofile:
                                startActivity(new Intent(MoreDetailsActivity.this, ViewProfileActivity.class));
                                return true;
                            case R.id.aboutpop:
                                startActivity(new Intent(MoreDetailsActivity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(MoreDetailsActivity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(MoreDetailsActivity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(MoreDetailsActivity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.scan:
                                startActivity(new Intent(MoreDetailsActivity.this, ScanActivity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(MoreDetailsActivity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(MoreDetailsActivity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(MoreDetailsActivity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(MoreDetailsActivity.this, Distributor_Search_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(MoreDetailsActivity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(MoreDetailsActivity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();
            }
        });

//        ProductSpecFragment specFragment = new ProductSpecFragment();
//        EngineCodeResult data = (EngineCodeResult) getIntent().getSerializableExtra("data");
//        specFragment.setProductSearch("OeSearch".equals(getIntent().getStringExtra("searchType"))?true:false,data);
//
//        if("OeSearch".equals(getIntent().getStringExtra("searchType")))
//        {
//            adapter.addFragment(specFragment,"Product\nImages");
//        }
//        else
//        {
//            adapter.addFragment(specFragment,"Product\nSpecification");
//        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MoreDetailsActivity.this, HomeScreenActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK)
                );
            }
        });

    }

    private void checkInternet()
    {
        Network_Connection_Activity connection_activity =new Network_Connection_Activity(MoreDetailsActivity.this);
        if(connection_activity.CheckInternet())
        {
            if("OeSearch".equals(getIntent().getStringExtra("searchType")))
                callApiForOEMoreDetails();
            else if("WhatsNew".equals(getIntent().getStringExtra("searchType")))
//                callApiForMoreDetails();
            callApiForProductMoreDetails();
            else {
                callApiForProductMoreDetails();
            }
        }
        else {
            alertBox.showAlertWithBack("Unable to connect Internet. please check your Internet connection ! ");
        }
    }

    private void callApiForProductMoreDetails()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(MoreDetailsActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();

        try{

            String modelCode = getIntent().getStringExtra("modelCode");
            String engineCode = getIntent().getStringExtra("engineCode");
            final String partNo = getIntent().getStringExtra("partNumber");
            String yearFrom = getIntent().getStringExtra("yearFrom");
            String yearTo = getIntent().getStringExtra("yearTo");
            String make = getIntent().getStringExtra("OEM");
            String model = getIntent().getStringExtra("MODEL");
            double stroke = getIntent().getDoubleExtra("stroke",0);
            String cc = getIntent().getStringExtra("cc");
            String kw = getIntent().getStringExtra("kw");
            String level = getIntent().getStringExtra("level");
            String segment = getIntent().getStringExtra("segment");
            APIService service = RetrofitClient.getApiService();
            Call<MoreDetailsModel> call = service.getProductSearchMoreDetailList(
                   segment,make,model,engineCode,modelCode,yearFrom,yearTo,stroke,cc,kw,level,partNo
            );

            call.enqueue(new Callback<MoreDetailsModel>() {
                @Override
                public void onResponse(Call<MoreDetailsModel> call, final Response<MoreDetailsModel> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
//                                final Object object = new Object();
//                                synchronized (object){
//                                    thread = new Thread(new Runnable() {
//                                        @Override
//                                        public void run() {
                                productImageList = new LinkedList<>();
                                List<String> pdfList = new LinkedList<>();
                                List<String> videoList = new LinkedList<>();
                                for(com.brainmagic.gatescatalog.api.models.moredetails.ImageList imageItem: response.body().getImageLists())
                                {
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL1()) && imageItem.getProductthumbURL1()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL1());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL2()) && imageItem.getProductthumbURL2()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL2());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL3()) && imageItem.getProductthumbURL3()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL3());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL4()) && imageItem.getProductthumbURL4()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL4());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL5()) && imageItem.getProductthumbURL5()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL5());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL6()) && imageItem.getProductthumbURL6()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL6());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL7()) && imageItem.getProductthumbURL7()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL7());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL8()) && imageItem.getProductthumbURL8()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL8());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL9()) && imageItem.getProductthumbURL9()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL9());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL10()) && imageItem.getProductthumbURL10()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL10());
                                    }if(!TextUtils.isEmpty(imageItem.getProductthumbURL11()) && imageItem.getProductthumbURL11()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL11());
                                    }if(!TextUtils.isEmpty(imageItem.getProductthumbURL13()) && imageItem.getProductthumbURL13()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL13());
                                    }if(!TextUtils.isEmpty(imageItem.getProductthumbURL14()) && imageItem.getProductthumbURL14()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL14());
                                    }if(!TextUtils.isEmpty(imageItem.getProductthumbURL15()) && imageItem.getProductthumbURL15()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL15());
                                    }if(!TextUtils.isEmpty(imageItem.getProductthumbURL16()) && imageItem.getProductthumbURL16()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL16());
                                    }if(!TextUtils.isEmpty(imageItem.getProductthumbURL17()) && imageItem.getProductthumbURL17()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL17());
                                    }if(!TextUtils.isEmpty(imageItem.getProductthumbURL18()) && imageItem.getProductthumbURL18()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL18());
                                    }if(!TextUtils.isEmpty(imageItem.getProductthumbURL19()) && imageItem.getProductthumbURL19()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL19());
                                    }if(!TextUtils.isEmpty(imageItem.getProductthumbURL20()) && imageItem.getProductthumbURL20()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL20());
                                    }

                                    if("PDF".equalsIgnoreCase(imageItem.getURLtype1()))
                                    {
                                        pdfList.add(imageItem.getURL1());
                                    }
                                    else if("video".equalsIgnoreCase(imageItem.getURLtype1()))
                                    {
                                        videoList.add(imageItem.getURL1());
                                    }
                                    if("PDF".equalsIgnoreCase(imageItem.getURLtype2()))
                                    {
                                        pdfList.add(imageItem.getURL2());
                                    }
                                    else if("video".equalsIgnoreCase(imageItem.getURLtype2()))
                                    {
                                        videoList.add(imageItem.getURL3());
                                    }
                                    if("PDF".equalsIgnoreCase(imageItem.getURLtype3()))
                                    {
                                        pdfList.add(imageItem.getURL3());
                                    }
                                    else if("video".equalsIgnoreCase(imageItem.getURLtype3()))
                                    {
                                        videoList.add(imageItem.getURL3());
                                    }
                                    if("PDF".equalsIgnoreCase(imageItem.getURLtype4()))
                                    {
                                        pdfList.add(imageItem.getURL4());
                                    }
                                    else if("video".equalsIgnoreCase(imageItem.getURLtype4()))
                                    {
                                        videoList.add(imageItem.getURL4());
                                    }
//                                                object.notify();
                                }
//                                        }
//                                    });
//                                }


//                                thread.start();

                                ProductSpecFragment specFragment = new ProductSpecFragment();
                                List<EngineCodeResult> dataList = response.body().getEngineCodeResult();
                                EngineCodeResult data = null;
                                if(dataList.size()!=0)
                                    data = dataList.get(0);

                                specFragment.setProductSearch("OeSearch".equals(getIntent().getStringExtra("searchType"))?true:false,data);

                                if("OeSearch".equals(getIntent().getStringExtra("searchType")))
                                {
                                    adapter.addFragment(specFragment,"Product\nImages");
                                }
                                else
                                {
                                    adapter.addFragment(specFragment,"Product\nSpecification");
                                }

                                ModelSuitsFragment modelSuitsFragment = new ModelSuitsFragment();
                                modelSuitsFragment.setSuitsData(response.body().getData());
                                OtherResourcesFragment otherResourcesFragment = new OtherResourcesFragment();
//                                otherResourcesFragment.setResourceData(response.body().getData1(),response.body().getPdfModelsList());
                                otherResourcesFragment.setResourceData(videoList,pdfList);
                                adapter.addFragment(modelSuitsFragment,"Suits these\nModel");
                                adapter.addFragment(otherResourcesFragment,"Other\nResources");
//        adapter.addFragment(new ModelSuitsFragment(),"Videos");

//                                synchronized (object) {
//                                    if (!thread.isAlive())
//                                        specFragment.setImageList(productImageList);
//                                    else {
//                                        object.wait();
                                specFragment.setImageList(productImageList);
//                                    }

                                pager.setAdapter(adapter);
                                layouts.setupWithViewPager(pager);

//                                }
                            }
                            else if(response.body().getResult().equals("No Record")){
                                alertBox.showAlertWithBack("No Record Found for this Part Number "+partNo);
                            }
                            else {
                                alertBox.showAlertWithBack("Server Error. Please try again later");
                            }
                        }
                        else {
                            alertBox.showAlertWithBack("Invalid Connection String. Please try again Later");
                        }

                    }catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alertBox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                 }

                @Override
                public void onFailure(Call<MoreDetailsModel> call, Throwable t) {
                    progressDialog.dismiss();
                    alertBox.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alertBox.showAlertWithBack("Exception Occurred. Please try again Later");
        }

    }

    private void callApiForMoreDetails()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(MoreDetailsActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();

        try{

            APIService service = RetrofitClient.getApiService();
            Call<MoreDetailsModel> call = service.getProductMoreDetailList(getIntent().getStringExtra("partNo"));

            call.enqueue(new Callback<MoreDetailsModel>() {
                @Override
                public void onResponse(Call<MoreDetailsModel> call, final Response<MoreDetailsModel> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
//                                final Object object = new Object();
//                                synchronized (object){
//                                    thread = new Thread(new Runnable() {
//                                        @Override
//                                        public void run() {
                                            productImageList = new LinkedList<>();
                                            List<String> pdfList = new LinkedList<>();
                                            List<String> videoList = new LinkedList<>();
                                            for(com.brainmagic.gatescatalog.api.models.moredetails.ImageList imageItem: response.body().getImageLists())
                                            {
                                                if(!TextUtils.isEmpty(imageItem.getProductthumbURL1()) && imageItem.getProductthumbURL1()!=null)
                                                {
                                                    productImageList.add(imageItem.getProductthumbURL1());
                                                }
                                                if(!TextUtils.isEmpty(imageItem.getProductthumbURL2()) && imageItem.getProductthumbURL2()!=null)
                                                {
                                                    productImageList.add(imageItem.getProductthumbURL2());
                                                }
                                                if(!TextUtils.isEmpty(imageItem.getProductthumbURL3()) && imageItem.getProductthumbURL3()!=null)
                                                {
                                                    productImageList.add(imageItem.getProductthumbURL3());
                                                }
                                                if(!TextUtils.isEmpty(imageItem.getProductthumbURL4()) && imageItem.getProductthumbURL4()!=null)
                                                {
                                                    productImageList.add(imageItem.getProductthumbURL4());
                                                }
                                                if(!TextUtils.isEmpty(imageItem.getProductthumbURL5()) && imageItem.getProductthumbURL5()!=null)
                                                {
                                                    productImageList.add(imageItem.getProductthumbURL5());
                                                }
                                                if(!TextUtils.isEmpty(imageItem.getProductthumbURL6()) && imageItem.getProductthumbURL6()!=null)
                                                {
                                                    productImageList.add(imageItem.getProductthumbURL6());
                                                }
                                                if(!TextUtils.isEmpty(imageItem.getProductthumbURL7()) && imageItem.getProductthumbURL7()!=null)
                                                {
                                                    productImageList.add(imageItem.getProductthumbURL7());
                                                }
                                                if(!TextUtils.isEmpty(imageItem.getProductthumbURL8()) && imageItem.getProductthumbURL8()!=null)
                                                {
                                                    productImageList.add(imageItem.getProductthumbURL8());
                                                }
                                                if(!TextUtils.isEmpty(imageItem.getProductthumbURL9()) && imageItem.getProductthumbURL9()!=null)
                                                {
                                                    productImageList.add(imageItem.getProductthumbURL9());
                                                }
                                                if(!TextUtils.isEmpty(imageItem.getProductthumbURL10()) && imageItem.getProductthumbURL10()!=null)
                                            {
                                                productImageList.add(imageItem.getProductthumbURL10());
                                            }if(!TextUtils.isEmpty(imageItem.getProductthumbURL11()) && imageItem.getProductthumbURL11()!=null)
                                            {
                                                productImageList.add(imageItem.getProductthumbURL11());
                                            }if(!TextUtils.isEmpty(imageItem.getProductthumbURL13()) && imageItem.getProductthumbURL13()!=null)
                                            {
                                                productImageList.add(imageItem.getProductthumbURL13());
                                            }if(!TextUtils.isEmpty(imageItem.getProductthumbURL14()) && imageItem.getProductthumbURL14()!=null)
                                            {
                                                productImageList.add(imageItem.getProductthumbURL14());
                                            }if(!TextUtils.isEmpty(imageItem.getProductthumbURL15()) && imageItem.getProductthumbURL15()!=null)
                                            {
                                                productImageList.add(imageItem.getProductthumbURL15());
                                            }if(!TextUtils.isEmpty(imageItem.getProductthumbURL16()) && imageItem.getProductthumbURL16()!=null)
                                            {
                                                productImageList.add(imageItem.getProductthumbURL16());
                                            }if(!TextUtils.isEmpty(imageItem.getProductthumbURL17()) && imageItem.getProductthumbURL17()!=null)
                                            {
                                                productImageList.add(imageItem.getProductthumbURL17());
                                            }if(!TextUtils.isEmpty(imageItem.getProductthumbURL18()) && imageItem.getProductthumbURL18()!=null)
                                            {
                                                productImageList.add(imageItem.getProductthumbURL18());
                                            }if(!TextUtils.isEmpty(imageItem.getProductthumbURL19()) && imageItem.getProductthumbURL19()!=null)
                                            {
                                                productImageList.add(imageItem.getProductthumbURL19());
                                            }if(!TextUtils.isEmpty(imageItem.getProductthumbURL20()) && imageItem.getProductthumbURL20()!=null)
                                            {
                                                productImageList.add(imageItem.getProductthumbURL20());
                                            }

                                                if("PDF".equalsIgnoreCase(imageItem.getURLtype1()))
                                                {
                                                    pdfList.add(imageItem.getURL1());
                                                }
                                                else if("video".equalsIgnoreCase(imageItem.getURLtype1()))
                                                {
                                                    videoList.add(imageItem.getURL1());
                                                }
                                                if("PDF".equalsIgnoreCase(imageItem.getURLtype2()))
                                                {
                                                    pdfList.add(imageItem.getURL2());
                                                }
                                                else if("video".equalsIgnoreCase(imageItem.getURLtype2()))
                                                {
                                                    videoList.add(imageItem.getURL3());
                                                }
                                                if("PDF".equalsIgnoreCase(imageItem.getURLtype3()))
                                                {
                                                    pdfList.add(imageItem.getURL3());
                                                }
                                                else if("video".equalsIgnoreCase(imageItem.getURLtype3()))
                                                {
                                                    videoList.add(imageItem.getURL3());
                                                }
                                                if("PDF".equalsIgnoreCase(imageItem.getURLtype4()))
                                                {
                                                    pdfList.add(imageItem.getURL4());
                                                }
                                                else if("video".equalsIgnoreCase(imageItem.getURLtype4()))
                                                {
                                                    videoList.add(imageItem.getURL4());
                                                }
//                                                object.notify();
                                            }
//                                        }
//                                    });
//                                }


//                                thread.start();

                                ProductSpecFragment specFragment = new ProductSpecFragment();
                                EngineCodeResult data = (EngineCodeResult) getIntent().getSerializableExtra("data");
                                specFragment.setProductSearch("OeSearch".equals(getIntent().getStringExtra("searchType"))?true:false,data);

                                if("OeSearch".equals(getIntent().getStringExtra("searchType")))
                                {
                                    adapter.addFragment(specFragment,"Product\nImages");
                                }
                                else
                                {
                                    adapter.addFragment(specFragment,"Product\nSpecification");
                                }

                                ModelSuitsFragment modelSuitsFragment = new ModelSuitsFragment();
                                modelSuitsFragment.setSuitsData(response.body().getData());
                                OtherResourcesFragment otherResourcesFragment = new OtherResourcesFragment();
//                                otherResourcesFragment.setResourceData(response.body().getData1(),response.body().getPdfModelsList());
                                otherResourcesFragment.setResourceData(videoList,pdfList);
                                adapter.addFragment(modelSuitsFragment,"Suits these\nModel");
                                adapter.addFragment(otherResourcesFragment,"Other\nResources");
//        adapter.addFragment(new ModelSuitsFragment(),"Videos");

//                                synchronized (object) {
//                                    if (!thread.isAlive())
//                                        specFragment.setImageList(productImageList);
//                                    else {
//                                        object.wait();
                                        specFragment.setImageList(productImageList);
//                                    }

                                    pager.setAdapter(adapter);
                                    layouts.setupWithViewPager(pager);
//                                }
                            }
                            else if(response.body().getResult().equals("No Record")){
                                alertBox.showAlertWithBack("No Record Found for this Part Number "+getIntent().getStringExtra("partNo"));
                            }
                            else {
                                alertBox.showAlertWithBack("Server Error. Please try again later");
                            }
                        }
                        else {
                            alertBox.showAlertWithBack("Invalid Connection String. Please try again Later");
                        }

                    }catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alertBox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<MoreDetailsModel> call, Throwable t) {
                    progressDialog.dismiss();
                    alertBox.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alertBox.showAlertWithBack("Exception Occurred. Please try again Later");
        }


    }


    private void callApiForOEMoreDetails()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(MoreDetailsActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();

        try{

            APIService service = RetrofitClient.getApiService();
            Call<MoreDetailsModel> call = service.getOEProductMoreDetailList(getIntent().getStringExtra("partNo"));

            call.enqueue(new Callback<MoreDetailsModel>() {
                @Override
                public void onResponse(Call<MoreDetailsModel> call, final Response<MoreDetailsModel> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if(response.body().getResult().equals("Success"))
                            {
//                                final Object object = new Object();
//                                synchronized (object){
//                                    thread = new Thread(new Runnable() {
//                                        @Override
//                                        public void run() {
                                productImageList = new LinkedList<>();
                                List<String> pdfList = new LinkedList<>();
                                List<String> videoList = new LinkedList<>();
                                for(com.brainmagic.gatescatalog.api.models.moredetails.ImageList imageItem: response.body().getImageLists())
                                {
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL1()) && imageItem.getProductthumbURL1()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL1());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL2()) && imageItem.getProductthumbURL2()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL2());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL3()) && imageItem.getProductthumbURL3()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL3());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL4()) && imageItem.getProductthumbURL4()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL4());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL5()) && imageItem.getProductthumbURL5()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL5());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL6()) && imageItem.getProductthumbURL6()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL6());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL7()) && imageItem.getProductthumbURL7()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL7());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL8()) && imageItem.getProductthumbURL8()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL8());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL9()) && imageItem.getProductthumbURL9()!=null)
                                    {
                                        productImageList.add(imageItem.getProductthumbURL9());
                                    }
                                    if(!TextUtils.isEmpty(imageItem.getProductthumbURL10()) && imageItem.getProductthumbURL10()!=null)
                                {
                                    productImageList.add(imageItem.getProductthumbURL10());
                                }if(!TextUtils.isEmpty(imageItem.getProductthumbURL11()) && imageItem.getProductthumbURL11()!=null)
                                {
                                    productImageList.add(imageItem.getProductthumbURL11());
                                }if(!TextUtils.isEmpty(imageItem.getProductthumbURL13()) && imageItem.getProductthumbURL13()!=null)
                                {
                                    productImageList.add(imageItem.getProductthumbURL13());
                                }if(!TextUtils.isEmpty(imageItem.getProductthumbURL14()) && imageItem.getProductthumbURL14()!=null)
                                {
                                    productImageList.add(imageItem.getProductthumbURL14());
                                }if(!TextUtils.isEmpty(imageItem.getProductthumbURL15()) && imageItem.getProductthumbURL15()!=null)
                                {
                                    productImageList.add(imageItem.getProductthumbURL15());
                                }if(!TextUtils.isEmpty(imageItem.getProductthumbURL16()) && imageItem.getProductthumbURL16()!=null)
                                {
                                    productImageList.add(imageItem.getProductthumbURL16());
                                }if(!TextUtils.isEmpty(imageItem.getProductthumbURL17()) && imageItem.getProductthumbURL17()!=null)
                                {
                                    productImageList.add(imageItem.getProductthumbURL17());
                                }if(!TextUtils.isEmpty(imageItem.getProductthumbURL18()) && imageItem.getProductthumbURL18()!=null)
                                {
                                    productImageList.add(imageItem.getProductthumbURL18());
                                }if(!TextUtils.isEmpty(imageItem.getProductthumbURL19()) && imageItem.getProductthumbURL19()!=null)
                                {
                                    productImageList.add(imageItem.getProductthumbURL19());
                                }if(!TextUtils.isEmpty(imageItem.getProductthumbURL20()) && imageItem.getProductthumbURL20()!=null)
                                {
                                    productImageList.add(imageItem.getProductthumbURL20());
                                }

                                    if("PDF".equalsIgnoreCase(imageItem.getURLtype1()))
                                    {
                                        pdfList.add(imageItem.getURL1());
                                    }
                                    else if("video".equalsIgnoreCase(imageItem.getURLtype1()))
                                    {
                                        videoList.add(imageItem.getURL1());
                                    }
                                    if("PDF".equalsIgnoreCase(imageItem.getURLtype2()))
                                    {
                                        pdfList.add(imageItem.getURL2());
                                    }
                                    else if("video".equalsIgnoreCase(imageItem.getURLtype2()))
                                    {
                                        videoList.add(imageItem.getURL3());
                                    }
                                    if("PDF".equalsIgnoreCase(imageItem.getURLtype3()))
                                    {
                                        pdfList.add(imageItem.getURL3());
                                    }
                                    else if("video".equalsIgnoreCase(imageItem.getURLtype3()))
                                    {
                                        videoList.add(imageItem.getURL3());
                                    }
                                    if("PDF".equalsIgnoreCase(imageItem.getURLtype4()))
                                    {
                                        pdfList.add(imageItem.getURL4());
                                    }
                                    else if("video".equalsIgnoreCase(imageItem.getURLtype4()))
                                    {
                                        videoList.add(imageItem.getURL4());
                                    }
//                                                object.notify();
                                }
//                                        }
//                                    });
//                                }


//                                thread.start();

                                ProductSpecFragment specFragment = new ProductSpecFragment();
//                                String data = response.body().getEngineCodeResult().get(0).getArticle();
//                                EngineCodeResult data = (EngineCodeResult) getIntent().getSerializableExtra("Article");
                                List<EngineCodeResult> dataList = response.body().getEngineCodeResult();
                                EngineCodeResult data=null;
                                if(dataList.size()!=0)
                                    data = dataList.get(0);
//                                String dataq = dataList.get(0).getmMRP();
                                specFragment.setProductSearch("OeSearch".equals(getIntent().getStringExtra("searchType"))?true:false,data);

                                if("OeSearch".equals(getIntent().getStringExtra("searchType")))
                                {
                                    adapter.addFragment(specFragment,"Product\nImages");
                                }
                                else
                                {
                                    adapter.addFragment(specFragment,"Product\nSpecification");
                                }

                                ModelSuitsFragment modelSuitsFragment = new ModelSuitsFragment();
                                modelSuitsFragment.setSuitsData(response.body().getData());
                                OtherResourcesFragment otherResourcesFragment = new OtherResourcesFragment();
//                                otherResourcesFragment.setResourceData(response.body().getData1(),response.body().getPdfModelsList());
                                otherResourcesFragment.setResourceData(videoList,pdfList);
                                adapter.addFragment(modelSuitsFragment,"Suits these\nModel");
                                adapter.addFragment(otherResourcesFragment,"Other\nResources");

//        adapter.addFragment(new ModelSuitsFragment(),"Videos");

//                                synchronized (object) {
//                                    if (!thread.isAlive())
//                                        specFragment.setImageList(productImageList);
//                                    else {
//                                        object.wait();
                                specFragment.setImageList(productImageList);
//                                    }

                                pager.setAdapter(adapter);
                                layouts.setupWithViewPager(pager);
//                                }
                            }
                            else if(response.body().getResult().equals("No Record")){
                                alertBox.showAlertWithBack("No Record Found for this Part Number "+getIntent().getStringExtra("partNo"));
                            }
                            else {
                                alertBox.showAlertWithBack("Server Error. Please try again later");
                            }
                        }
                        else {
                            alertBox.showAlertWithBack("Invalid Connection String. Please try again Later");
                        }

                    }catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alertBox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<MoreDetailsModel> call, Throwable t) {
                    progressDialog.dismiss();
                    alertBox.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alertBox.showAlertWithBack("Exception Occurred. Please try again Later");
        }


    }


}
