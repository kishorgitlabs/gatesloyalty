package com.brainmagic.gatescatalog.activities.productsearch.fragment;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.gatescatalog.api.models.moredetails.MoreDetailsModel;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.adapter.ImageAdapter;
import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeResult;
import com.brainmagic.gatescatalog.touchimageview.ZoomImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import uk.co.senab.photoview.PhotoView;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class ProductSpecFragment extends Fragment
//        implements ImageAdapter.GetImage
{

    private boolean isOESearch = false;
    private ImageView rightBtn, leftbtn, productImage;
    private EngineCodeResult data;
    private List<String> productImageList;
    private String fullImage;
    private RelativeLayout thumbNailLayout;
    private ImageButton closeButton;
    private PopupWindow mPopupWindow;
    private View customView;
    private RelativeLayout mRelativeLayout;
    private PhotoView photoView;
    private EngineCodeResult model;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        if (isOESearch) {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_product_image, container, false);
            TextView gatesPartNoOESearch =  view.findViewById(R.id.list_gatespartno_oe_search);
            TextView gatesMrpOESearch = view.findViewById(R.id.mrp_oe_search);
            LinearLayout gatesMrpOESearchLayout = view.findViewById(R.id.gates_part_no_layout_mrp);
            LinearLayout gatesPartNoOESearchLayout =  view.findViewById(R.id.gates_part_no_layout);

            if(data != null)
            {
                gatesPartNoOESearchLayout.setVisibility(View.VISIBLE);
                gatesMrpOESearchLayout.setVisibility(View.VISIBLE);
                if (data.getmMRP() == null){
                    gatesMrpOESearch.setText("र  "+"0.0");
                }else {
                    gatesMrpOESearch.setText("र  "+data.getmMRP());
                }


                gatesPartNoOESearch.setText(data.getArticle());


            }

        } else {
            view = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_product_spec, container, false);

            TextView gatesPartNo =  view.findViewById(R.id.list_gatespartno);
            TextView partDesc =  view.findViewById(R.id.list_partdescription);
            TextView model =  view.findViewById(R.id.list_model);
            TextView tvMake =  view.findViewById(R.id.tv_make);
            TextView engineCode =  view.findViewById(R.id.list_engine_code);
            TextView modelCode =  view.findViewById(R.id.list_model_code);
            TextView monthFrom =  view.findViewById(R.id.list_monthfrom);
            TextView monthTill =  view.findViewById(R.id.list_monthtill);
            TextView appAtt =  view.findViewById(R.id.app_att);
            TextView stroke =  view.findViewById(R.id.stroke);
            TextView ccValue =  view.findViewById(R.id.cc_value);
            TextView kkValue =  view.findViewById(R.id.kw_value);
            TextView mrp = view.findViewById(R.id.mrp);

            if(data != null) {
                gatesPartNo.setText(data.getArticle());
                partDesc.setText(data.getArticlegroup());
                model.setText(data.getModel());
                tvMake.setText(data.getMake());
                engineCode.setText(data.getEnginecode());
                modelCode.setText(data.getModelcode());
                monthFrom.setText(data.getMonthFrom()+"/"+data.getYearFrom());
                monthTill.setText(data.getMonthTill()+"/"+data.getYearTill());
                appAtt.setText(data.getApplicationAttributes());
                stroke.setText(String.valueOf(data.getStroke()));
                ccValue.setText(data.getCC());
                kkValue.setText(data.getKW());

                if (data.getmMRP() == null){
                    mrp.setText("र  "+"0.0");
                }else {
                    mrp.setText("र  "+data.getmMRP());
                }

//                mrp.setText("र  "+data.getmMRP());
            }
            else {
                LinearLayout productSpecLayout = view.findViewById(R.id.product_spec_layout);
                LinearLayout errorLayout = view.findViewById(R.id.error_layout);

                productSpecLayout.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);

            }
        }

        mRelativeLayout = view.findViewById(R.id.image_container);
        LayoutInflater inflaters = (LayoutInflater) container.getContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        customView = inflaters.inflate(R.layout.imagepopup, null);
        mPopupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
//        LinearLayout productSpecLayout = view.findViewById(R.id.product_spec_layout);
        productImage = view.findViewById(R.id.product_image);
        thumbNailLayout =  view.findViewById(R.id.thumb_nail_layout);
        rightBtn = view.findViewById(R.id.right);
        leftbtn = view.findViewById(R.id.left);
//        if (isOESearch) {
//            productSpecLayout.setVisibility(View.GONE);
//        } else {
//            productSpecLayout.setVisibility(View.VISIBLE);
//        }

        ImageButton closeButton = (ImageButton) customView.findViewById(R.id.close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopupWindow.dismiss();
            }
        });

        productImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
                View view = LayoutInflater.from(container.getContext()).inflate(R.layout.pop_up_image, null);
//                ZoomImageView imageView = view.findViewById(R.id.customImageView);
//                Picasso.with(container.getContext()).load(fullImage).error(R.drawable.noimages).into(imageView);

                photoView = (PhotoView) customView.findViewById(R.id.pop_photo);
                Picasso.with(container.getContext()).load(fullImage).error(R.drawable.noimages).into(photoView);
            }
        });
        initialRecycler(view);
        return view;
    }

    public void setProductSearch(boolean isOESearch, EngineCodeResult data) {
        this.isOESearch = isOESearch;
        this.data =  data;
    }

    private void initialRecycler(View view) {

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        final RecyclerView recyclerView = view.findViewById(R.id.recycleview1);

        if(productImageList!=null)
        {
            if(productImageList.size()!=0)
            {
                Picasso.with(getActivity()).load(productImageList.get(0)).error(R.drawable.noimages).into(productImage);

                if(productImageList.size() <4) {
                    rightBtn.setVisibility(View.INVISIBLE);
                    leftbtn.setVisibility(View.INVISIBLE);
                }
                if(productImageList.size()==1)
                    thumbNailLayout.setVisibility(View.GONE);
                else
                    thumbNailLayout.setVisibility(View.VISIBLE);
            }
            else {
                thumbNailLayout.setVisibility(View.GONE);
                rightBtn.setVisibility(View.INVISIBLE);
                leftbtn.setVisibility(View.INVISIBLE);
            }
        }
        else {
            thumbNailLayout.setVisibility(View.GONE);
            rightBtn.setVisibility(View.INVISIBLE);
            leftbtn.setVisibility(View.INVISIBLE);
        }

        recyclerView.setLayoutManager(linearLayoutManager);
//        ImageAdapter imageAdapter = new ImageAdapter(this, thumbImage);
        ImageAdapter imageAdapter = new ImageAdapter(getActivity(),productImageList);
        imageAdapter.setImageClickListener(new ImageAdapter.GetImage() {
            @Override
            public void setOnImageClick(String image, int pos) {
                fullImage=image;
                Picasso.with(getActivity()).load(image).error(R.drawable.noimages).into(productImage);
            }
        });
        recyclerView.setAdapter(imageAdapter);

        rightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.setLayoutManager(linearLayoutManager);
                linearLayoutManager.scrollToPosition(productImageList.size()+1);
                recyclerView.scrollBy(100, 0);
            }
        });

        leftbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                recyclerView.smoothScrollToPosition(0);
                LinearLayoutManager lm = (LinearLayoutManager) recyclerView.getLayoutManager();
                lm.scrollToPositionWithOffset(lm.findFirstCompletelyVisibleItemPosition() - 1, recyclerView.getChildCount());
            }
        });

//        product_image.setImageResource(imgUrl);
//        Picasso.with(Product_More_Details_Activity.this).load(imgUrl).error(R.drawable.noimages).into(product_image);
    }

    public void setImageList(List<String> productImageList) {
        if(productImageList.size() !=0)
        {
            fullImage = productImageList.get(0);
            this.productImageList = productImageList;
        }
    }

//    @Override
//    public void setOnImageClick(String image, int pos) {
//        fullImage=image;
//        Picasso.with(getActivity()).load(image).error(R.drawable.noimages).into(productImage);
//        product_image.setImageResource(p);
//        imageAdapter.notifyDataSetChanged();
//    }
}
