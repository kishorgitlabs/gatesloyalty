package com.brainmagic.gatescatalog.activities.loyalty;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.Sample1;
import com.brainmagic.gatescatalog.api.models.gates.approvedlist.ApprovedList;
import com.brainmagic.gatescatalog.api.models.gates.approvedlist.Datum;
import com.brainmagic.gatescatalog.api.models.gates.searchapproved.SearchApproved;
import com.brainmagic.gatescatalog.api.models.gates.usertype.UserType;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApprovedListActivity extends AppCompatActivity {

    private List<Datum> list;
    private List<com.brainmagic.gatescatalog.api.models.gates.searchapproved.Datum> datumList;
    private List<String> userTypeList;
    private ListView approvedList;
    private Alertbox alertbox;
    private ApprovedAdapter approvedAdapter;
    private Spinner userType;
    private String[] users = {"Type of User", "Mechanic", "Dealer / Retailer"};

    private FloatingActionButton back, home;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approved_list);
        approvedList = findViewById(R.id.approved_list);
        back = findViewById(R.id.back);
        home = findViewById(R.id.home);
        userType = findViewById(R.id.user_type_spinner);
        alertbox = new Alertbox(ApprovedListActivity.this);
        list = new ArrayList<>();
        userTypeList = new ArrayList<>();
        datumList = new ArrayList<>();

        ArrayAdapter<String> userTypeAdpater = new ArrayAdapter<>(ApprovedListActivity.this,
                android.R.layout.simple_list_item_1, users);
        userType.setAdapter(userTypeAdpater);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent home = new Intent(ApprovedListActivity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });

        userType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String item = adapterView.getItemAtPosition(i).toString();
                if (item.equals("Type of User")) {
                    getData();
                    approvedList.setVisibility(View.VISIBLE);
                } else {
                    getFilter(item);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

//        getUserType();

//        getData();
    }

    private void getUserType() {
        try {
            final ProgressDialog loading = ProgressDialog.show(ApprovedListActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

            APIService service = RetrofitClient.getApiService();

            Call<UserType> call = service.getUserType();

            call.enqueue(new Callback<UserType>() {
                @Override
                public void onResponse(Call<UserType> call, Response<UserType> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")){
                        ArrayAdapter<String> userTypeAdpater = new ArrayAdapter<>(ApprovedListActivity.this,
                                android.R.layout.simple_list_item_1, response.body().getData());
                        userType.setAdapter(userTypeAdpater);
                    }else {
                        alertbox.showAlertWithBack("Please try again later");
                    }
                }

                @Override
                public void onFailure(Call<UserType> call, Throwable t) {

                }
            });



        }catch (Exception e){
            e.printStackTrace();
            alertbox.showAlertWithBack("Please try again later");
        }
    }

    private void getFilter(String item) {
        try {
            final ProgressDialog loading = ProgressDialog.show(ApprovedListActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

            APIService service = RetrofitClient.getApiService();

            Call<SearchApproved> call = service.serachApproved(item);

            call.enqueue(new Callback<SearchApproved>() {
                @Override
                public void onResponse(Call<SearchApproved> call, Response<SearchApproved> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")){
                        SearchApprovedAdapter approvedAdapter = new SearchApprovedAdapter(ApprovedListActivity.this,
                                response.body().getData());
                        approvedList.setAdapter(approvedAdapter);

                    }else {
                        alertbox.showAlertWithBack("Please try again later");
                    }
                }

                @Override
                public void onFailure(Call<SearchApproved> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showAlertWithBack(getString(R.string.server_error));
                }
            });


        }catch (Exception e){
            e.printStackTrace();
            alertbox.showAlertWithBack("Please try again later");
        }
    }

    private void getData() {
        try {
            final ProgressDialog loading = ProgressDialog.show(ApprovedListActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

            APIService service = RetrofitClient.getApiService();

            Call<ApprovedList> call = service.getApprovedList();

            call.enqueue(new Callback<ApprovedList>() {
                @Override
                public void onResponse(Call<ApprovedList> call, Response<ApprovedList> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        list = response.body().getData();
                        approvedAdapter = new ApprovedAdapter(ApprovedListActivity.this, list);
                        approvedList.setAdapter(approvedAdapter);
                    } else {
                        alertbox.showAlertWithBack("No Record Found !");
                    }
                }

                @Override
                public void onFailure(Call<ApprovedList> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showAlertWithBack(getString(R.string.server_error));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            alertbox.showAlertWithBack("Please try again later");
        }
    }
}