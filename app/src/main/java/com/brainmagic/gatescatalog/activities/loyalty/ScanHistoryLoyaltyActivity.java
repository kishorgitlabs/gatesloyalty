package com.brainmagic.gatescatalog.activities.loyalty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributor_Search_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.scanhistoryloyalty.DataItem;
import com.brainmagic.gatescatalog.api.models.scanhistoryloyalty.ScanHistoryLoyalty;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScanHistoryLoyaltyActivity extends AppCompatActivity {

    private ListView listView;
    private SharedPreferences myShare;
    private SharedPreferences.Editor editor;
    private String Mobno;
    //    private ScanHistoryAdapter scanHistoryAdapter;
    private List<DataItem> data;
    ProgressDialog progressDialog;
    ImageView back,menu, home;
    String Usertype;

    private TextView couponCode;
    private Spinner userTypeSpinner;
    String[] types = {"Type Of User", "Dealer / Retailer", "Mechanic"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_history_loyalty);

        listView = findViewById(R.id.scan_history_loyalty_list);
        back = findViewById(R.id.back);
        home = findViewById(R.id.home);

        back=findViewById(R.id.back);
        menu=findViewById(R.id.menu);
        couponCode = findViewById(R.id.coupon_code);
        userTypeSpinner = findViewById(R.id.user_type_spinner);
        myShare = getSharedPreferences("Registration",MODE_PRIVATE);
        editor = myShare.edit();

        Mobno = myShare.getString("MobNo","");
        Usertype = myShare.getString("Usertype", "");

        if (Usertype.equals("Mechanic")){
            couponCode.setText("Coupon Code");
        }else if (Usertype.equals("Dealer / Retailer")){
            couponCode.setText("MRP Label");
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(ScanHistoryLoyaltyActivity.this,
                android.R.layout.simple_list_item_1, types);
        userTypeSpinner.setAdapter(arrayAdapter);

        userTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String item = adapterView.getItemAtPosition(i).toString();
                if (item.equals("Dealer / Retailer")){
                    couponCode.setText("MRP Label");
                }else if (item.equals("Mechanic")){
                    couponCode.setText("Coupon Code");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        history();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ScanHistoryLoyaltyActivity.this, HomeScreenActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                );
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(ScanHistoryLoyaltyActivity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub
                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(ScanHistoryLoyaltyActivity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(ScanHistoryLoyaltyActivity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.viewprofile:
                                startActivity(new Intent(ScanHistoryLoyaltyActivity.this, ViewProfileActivity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(ScanHistoryLoyaltyActivity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(ScanHistoryLoyaltyActivity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(HomeScreenActivity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.scan:
                                startActivity(new Intent(ScanHistoryLoyaltyActivity.this, ScanActivity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(ScanHistoryLoyaltyActivity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(ScanHistoryLoyaltyActivity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(ScanHistoryLoyaltyActivity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(ScanHistoryLoyaltyActivity.this, Distributor_Search_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(HomeScreenActivity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(ScanHistoryLoyaltyActivity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();
            }
        });
    }

    private void history() {

        progressDialog  = progressDialog.show(ScanHistoryLoyaltyActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
        APIService service = RetrofitClient.getApiService();

        Call<ScanHistoryLoyalty> call = service.historyLoyalty(Mobno);

        call.enqueue(new Callback<ScanHistoryLoyalty>() {
            @Override
            public void onResponse(Call<ScanHistoryLoyalty> call, Response<ScanHistoryLoyalty> response) {
                try {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")){
                        data = response.body().getData();
                        if (data.size() == 0){
                            showMessage("No Data Available");
                        }else {
                            ScanHistoryLoyaltyAdapter scanHistoryLoyaltyAdapter = new ScanHistoryLoyaltyAdapter(ScanHistoryLoyaltyActivity.this, data);
                            listView.setAdapter(scanHistoryLoyaltyAdapter);
                        }

                    }else {
                        showMessage("Please try again later.. ");
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ScanHistoryLoyalty> call, Throwable t) {
//                showMessage(t.getMessage());
                showMessage("Please try again later.. ");
            }
        });
    }

    public void showMessage(String s) {

        Alertbox alert=new Alertbox(ScanHistoryLoyaltyActivity.this);
        alert.showAlertWithBack(s);
    }
}
