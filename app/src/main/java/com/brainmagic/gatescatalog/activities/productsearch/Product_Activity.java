package com.brainmagic.gatescatalog.activities.productsearch;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainmagic.gatescatalog.activities.loyalty.ViewProfileActivity;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.services.BackgroundService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;


public class Product_Activity extends Activity {
    private RelativeLayout product_passengercar_relativelayout;
    private RelativeLayout product_hcv_relativelayout, product_update_relativelayout,
    product_oe_relativelayout,product_2w_relativelayout;
    private ImageView menu, search;
    private MultiAutoCompleteTextView searchBox;
//    private View productsearch_relativelayout;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String Email, Mobno, Usertype, Country;
    private Alertbox box = new Alertbox(Product_Activity.this);


//    private Alertbox alert = new Alertbox(Product_Activity.this);
//    private ProgressDialog loading;
//    private List<Products> products;
//    private SQLiteDatabase database;

    private static final String TAG = "Product_Activity";

    private ViewGroup includeviewLayout;
    private TextView ProfileName;
//    private ConstraintLayout signout_relativelayout;
    private LinearLayout signout_relativelayout;
    private FloatingActionButton home,back;
    private ImageView profilePicture;
    private String[] countrytypes = {"Select Country", "All", "Brunei", "Cambodia", "India", "Indonesia", "Malaysia", "Myanmar", "Philippines", "Singapore", "Thailand", "Vietnam"};
    private String SelectedCountry;
    private ImageView info_Search,info_Clear;
    private ArrayList<String> PredictionList;
    private TextView passengerCarText,heavyCommercialText,wheelerText;
    private String nameLog, mobileNoLog, userTypeLog, stateLog, cityLog, zipCodeLog, locationLog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        product_passengercar_relativelayout = (RelativeLayout) findViewById(R.id.product_passenger_car_relativelayout);

        product_hcv_relativelayout = (RelativeLayout) findViewById(R.id.product_hcv_relativelayout);

//        passengerCarText=(TextView) findViewById(R.id.cart_badge_passenger);
//        heavyCommercialText=(TextView)findViewById(R.id.cart_badge_heavy);
//        wheelerText=(TextView)findViewById(R.id.cart_badge_2_wheeler);

        product_2w_relativelayout = (RelativeLayout) findViewById(R.id.product_2w_relativelayout);
        product_oe_relativelayout = (RelativeLayout) findViewById(R.id.product_oe_relativelayout);

//        searchBox = (MultiAutoCompleteTextView) findViewById(R.id.text_search_box);
//        search = (ImageView) findViewById(R.id.search);
//        info_Search = (ImageView) findViewById(R.id.info_search);
//        info_Clear = (ImageView) findViewById(R.id.clear_search);
//
        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        menu = (ImageView) findViewById(R.id.menu);
//
//        includeviewLayout = findViewById(R.id.profile_logo);
//        ProfileName = (TextView) includeviewLayout.findViewById(R.id.profilename);
//        signout_relativelayout =  findViewById(R.id.signout);
//        profilePicture = (ImageView) includeviewLayout.findViewById(R.id.profilepicture);
//        signout_relativelayout.setVisibility(View.GONE);
//
//        PredictionList = new ArrayList<String>();
//
////        info_Search.setOnClickListener(new OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                box.showAlert(getString(R.string.search_info));
////            }
////        });
//        info_Clear.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                searchBox.setText("");
//            }
//        });
//
//
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);

        nameLog = myshare.getString("nameLogs", "");
        mobileNoLog = myshare.getString("MobNo", "");
        userTypeLog = myshare.getString("Usertype", "");
        stateLog = myshare.getString("state", "");
        cityLog = myshare.getString("city", "");
        zipCodeLog = myshare.getString("zipCode", "");
        locationLog = myshare.getString("streetName", "");

        editor = myshare.edit();
//
//        Email = myshare.getString("email", "");
//        Mobno = myshare.getString("MobNo", "");
//        Usertype = myshare.getString("Usertype", "");
//        Country = myshare.getString("Country", "");
//        ProfileName.setText(myshare.getString("name", "")+" , "+Country);
//
//        if (!myshare.getString("profile_path", "").equals(""))
//            Glide.with(Product_Activity.this)
//                    .load(new File(myshare.getString("profile_path", "")))
//                    .into(profilePicture);
//
//        final ImageView downArrow=includeviewLayout.findViewById(R.id.down_arrow);
//        final RelativeLayout userData=includeviewLayout.findViewById(R.id.user_data);
//        ViewGroup homeLayout=findViewById(R.id.product_layout);
//
//        downArrow.setOnClickListener(new OnClickListener() {
//            boolean visible;
//            @Override
//            public void onClick(View v) {
//                ChangeBounds changeBounds=new ChangeBounds();
//                changeBounds.setDuration(600L);
//                TransitionManager.beginDelayedTransition(includeviewLayout,changeBounds);
//                visible = !visible;
//                if(visible)
//                    downArrow.animate().rotation(180).setInterpolator(new LinearInterpolator()).setDuration(500);
//                else
//                    downArrow.animate().rotation(0).setInterpolator(new LinearInterpolator()).setDuration(500);
//                userData.setVisibility(visible? View.VISIBLE: View.GONE);
//            }
//        });
//
//        if (myshare.getBoolean("islogin", false))
//            signout_relativelayout.setVisibility(View.VISIBLE);
//        else
//            signout_relativelayout.setVisibility(View.GONE);
//
//
//        signout_relativelayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                editor.putBoolean("islogin", false).commit();
//
//                final Snackbar mySnackbar = Snackbar.make(findViewById(R.id.product_layout), getString(R.string.channel_network_sign_out), Snackbar.LENGTH_INDEFINITE);
//                mySnackbar.setActionTextColor(getResources().getColor(R.color.red));
//                mySnackbar.setAction(getString(R.string.okay_button), new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        mySnackbar.dismiss();
//                        Intent intent = getIntent();
//                        finish();
//                        startActivity(intent);
//
//                    }
//                });
//                mySnackbar.show();
//
//            }
//        });
//
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(Product_Activity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
//
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        menu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Product_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(Product_Activity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;
                            case R.id.viewprofile:
                                startActivity(new Intent(Product_Activity.this, ViewProfileActivity.class));
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(Product_Activity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
//                                startActivity(new Intent(Product_Activity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Product_Activity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Product_Activity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Product_Activity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(Product_Activity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Product_Activity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Product_Activity.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Product_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(Product_Activity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });


        product_passengercar_relativelayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
//                sendLog("Segment","Passenger Car & Light Commercial");
                sendLog("Passenger Car & Light Commercial",
                        nameLog,"", "", mobileNoLog, userTypeLog, stateLog, cityLog,
                        zipCodeLog, locationLog, "Android");
                Intent a = new Intent(Product_Activity.this, Product_Make_List_Activity.class);
                a.putExtra("TYPE_OF_VEHICLE", "Passenger Car & Light Commercial");
                a.putExtra("navitxt", "PC & LCV ");
                a.putExtra("TITLE", "Passenger Car");
                startActivity(a);
            }
        });


        product_2w_relativelayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
//                sendLog("Segment","2 Wheeler");
                sendLog("2 Wheeler",
                        nameLog,"", "", mobileNoLog, userTypeLog, stateLog, cityLog,
                        zipCodeLog, locationLog, "Android");
                Intent a = new Intent(Product_Activity.this, Product_Make_List_Activity.class);
                a.putExtra("TYPE_OF_VEHICLE", "2 Wheeler");
                a.putExtra("navitxt", "2 Wheeler ");
                a.putExtra("TITLE", "2 Wheeler ");

                startActivity(a);
            }
        });

        product_oe_relativelayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(Product_Activity.this, Product_OE_Activity.class);
                startActivity(a);
            }
        });

        product_hcv_relativelayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
//                sendLog("Segment","Heavy Commercial");
                sendLog("Heavy Commercial",
                        nameLog,"", "", mobileNoLog, userTypeLog, stateLog, cityLog,
                        zipCodeLog, locationLog, "Android");
                Intent a = new Intent(Product_Activity.this, Product_Make_List_Activity.class);
                a.putExtra("TYPE_OF_VEHICLE", "Heavy Commercial");
                a.putExtra("navitxt", "HCV ");
                a.putExtra("TITLE", "Heavy Commercial");

                startActivity(a);
            }
        });
//
//        search.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (searchBox.getText().length() == 0) {
//                    box.showAlert(getString(R.string.text_search));
//                } else {
//                    Intent search = new Intent(Product_Activity.this, Product_Text_Search_Activity.class);
//                    search.putExtra("searchword", searchBox.getText().toString());
//                    startActivity(search);
//                }
//
//            }
//        });

//        int passandLight=getPassengerCarandLightCommercialCount();
//        int heavyCom=getheavyCommercialCount();
//        int wheeler=getWheelerCount();
//
//        if(passandLight>0)
//        {
//            passengerCarText.setVisibility(View.VISIBLE);
//            passengerCarText.setText(passandLight+"");
//        }
//        if(heavyCom>0)
//        {
//            heavyCommercialText.setVisibility(View.VISIBLE);
//            heavyCommercialText.setText(heavyCom+"");
//
//        }
//        if(wheeler>0)
//        {
//            wheelerText.setVisibility(View.VISIBLE);
//            wheelerText.setText(wheeler+"");
//        }


//        new GetPredction().execute();
    }







   /* private void DeleteProduct()
    {

        SQLiteHelper sqLiteHelper = new SQLiteHelper(Product_Activity.this);
        database = sqLiteHelper.getReadableDatabase();
        database.execSQL("delete from "+ PRODUCT_TABLE_NAME);
        database.close();

        GetProductFromURL(Country);
    }*/


//    private void GetProductFromURL(String country) {
//
//
//        final RestAdapter adapter = new RestAdapter.Builder()
//                .setEndpoint(ROOT_URL) //Setting the Root URL
//                .build();
//
//        //Creating object for our interface
//        APIServices api = adapter.create(APIServices.class);
//
//        api.getCountryProducts(country, new Callback<List<Products>>() {
//            @Override
//            public void success(List<Products> productsList, Response response) {
//                // accecss the items from you shop list here
//
//                products = productsList;
//                // showList();
//                if (!products.isEmpty()) {
//                    Log.v("Model", products.get(1).getModel());
//                    insertProductRecord();
//                } else {
////                    loading.dismiss();
//                    final Snackbar mySnackbar = Snackbar.make(findViewById(R.id.product_layout), "No products available for " + Country + " country !", Snackbar.LENGTH_INDEFINITE);
//                    mySnackbar.setActionTextColor(getResources().getColor(R.color.red));
//                    mySnackbar.setAction("Okay", new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            mySnackbar.dismiss();
//                        }
//                    });
//                    mySnackbar.show();
//
//                }
//
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//                error.printStackTrace();
//
////                loading.dismiss();
//                error.printStackTrace();
//                final Snackbar mySnackbar = Snackbar.make(findViewById(R.id.product_layout), "Something went wrong !", Snackbar.LENGTH_INDEFINITE);
//                mySnackbar.setActionTextColor(getResources().getColor(R.color.red));
//                mySnackbar.setAction("Okay", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        mySnackbar.dismiss();
//                    }
//                });
//                mySnackbar.show();
//
//            }
//        });
//
//    }

//    public void insertProductRecord() {
//
////        SQLiteHelper sqLiteHelper = new SQLiteHelper(Product_Activity.this);
////        database = sqLiteHelper.getReadableDatabase();
////
////        ContentValues contentValues = new ContentValues();
////        for (int i = 0; i < products.size(); i++) {
////            contentValues.put(COLUMN_ID, products.get(i).getId());
////            contentValues.put(COLUMN_SEGMENT, products.get(i).getSegment());
////            contentValues.put(COLUMN_MODEL, products.get(i).getModel());
////            contentValues.put(COLUMN_MAKE, products.get(i).getMake());
////            contentValues.put(COLUMN_STORKE, products.get(i).getStroke());
////            contentValues.put(COLUMN_MODELCODE, products.get(i).getModelcode());
////            contentValues.put(COLUMN_ENGINECODE, products.get(i).getEnginecode());
////            contentValues.put(COLUMN_YEAR_FROM, products.get(i).getYear_From());
////            contentValues.put(COLUMN_MONTH_FROM, products.get(i).getMonth_From());
////            contentValues.put(COLUMN_YEAR_TILL, products.get(i).getYear_Till());
////            contentValues.put(COLUMN_MONTH_TILL, products.get(i).getMonth_Till());
////            contentValues.put(COLUMN_PART_DESCRIPTION, products.get(i).getPart_Description());
////            contentValues.put(COLUMN_GATES_PART_NUMBER, products.get(i).getGates_Part_Number());
////            contentValues.put(COLUMN_EQUIPMENT, products.get(i).getEquipment());
////            contentValues.put(COLUMN_EQUIPMENT, products.get(i).getEquipment());
////            contentValues.put(COLUMN_EQUIPMENT_2, products.get(i).getEquipment_2());
////            contentValues.put(COLUMN_EQUIPMENT_DATE_FROM, products.get(i).getEquipment_Date_From());
////            contentValues.put(COLUMN_EQUIPMENT_DATE_TO, products.get(i).getEquipment_Date_To());
////            database.insert(PRODUCT_TABLE_NAME, null, contentValues);
////        }
//
////        loading.dismiss();
////
////        database.close();
////
////        Date cDate = new Date();
////        String fDate = new SimpleDateFormat("dd/MM/yyyy").format(cDate);
////        Log.v("download data", fDate);
////        editor.putString("lastdownloaddata", fDate);
////        editor.commit();
////
////        box.showAlert("Updated successfully");
//    }

   /* private void sendLog(String productName, String productDetail)
    {
        Network_Connection_Activity network_connection_activity = new Network_Connection_Activity(Product_Activity.this);
        if (network_connection_activity.CheckInternet()) {
            startService(new Intent(Product_Activity.this, BackgroundService.class)
                    .putExtra("mobileNo", myshare.getString("MobNo", "000"))
                    .putExtra("productName",productName)
                    .putExtra("productDetail",productDetail)
            );
        }
    }*/

    private void sendLog(String segment, String userName, String vehicleName, String partNo, String mobileNumber, String userTypes, String stateLog,
                         String CityLog, String ZipCodeLog, String location, String deviceType)
    {
        Network_Connection_Activity network_connection_activity = new Network_Connection_Activity(Product_Activity.this);
        if (network_connection_activity.CheckInternet()) {
            startService(new Intent(Product_Activity.this, BackgroundService.class)
                    .putExtra("segmentLog", segment)
                    .putExtra("nameLog",userName)
                    .putExtra("vehicleNameLog",vehicleName)
                    .putExtra("partNoLog", partNo)
                    .putExtra("mobileNoLog",mobileNumber)
                    .putExtra("userTypeLog",userTypes)
                    .putExtra("stateLog", stateLog)
                    .putExtra("cityLog",CityLog)
                    .putExtra("zipCodeLog",ZipCodeLog)
                    .putExtra("locationLog", location)
                    .putExtra("deviceTypeLog",deviceType)
            );
        }
    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }


//    class GetPredction extends AsyncTask<String, Void, String> {
//
//        private ProgressDialog loading;
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//           loading = ProgressDialog.show(Product_Activity.this,"",getString(R.string.loadings),false,false);
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            getRecords();
//            return "success";
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//
//           /* // Sorting
//
//            Comparator<String> c = new Comparator<String>() {
//                @Override
//                public int compare(String object1, String object2) {
//                    Matcher m = matcher(object1);
//                    Integer number1 = null;
//                    if (!m.find()) {
//                        return object1.getVideoname().compareTo(object2.getVideoname());
//                    }
//                    else {
//                        Integer number2 = null;
//                        number1 = Integer.parseInt(m.group());
//                        m = p.matcher(object2.getVideoname());
//                        if (!m.find()) {
//                            return object1.getVideoname().compareTo(object2.getVideoname());
//                        }
//                        else {
//                            number2 = Integer.parseInt(m.group());
//                            int comparison = number1.compareTo(number2);
//                            if (comparison != 0) {
//                                return comparison;
//                            }
//                            else {
//                                return object1.getVideoname().compareTo(object2.getVideoname());
//                            }
//                        }
//                    }
//                }
//            };*/
//
//           // Collections.sort(PredictionList);
////            System.out.println(PredictionList);
//
//            loading.dismiss();
//            ProductTextSearchAdapter adapter = new ProductTextSearchAdapter(Product_Activity.this, R.layout.simple_dropdown_transparent_item,R.id.text1,PredictionList);
//            searchBox.setAdapter(adapter);
//            searchBox.setThreshold(1);
//            //searchBox.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
//            searchBox.setTokenizer(new PlusTokenizer());
//
//
//          /*AutoCompleteAdapter adapter = new AutoCompleteAdapter(Product_Activity.this, R.layout.simple_dropdown_transparent_item,R.id.text1,PredictionList);
//            searchBox.setThreshold(1);
//            searchBox.setAdapter(adapter);
//            //searchBox.setTokenizer(new PlusTokenizer());*/
//        }
//    }

//    private int getPassengerCarandLightCommercialCount()
//    {
//        AppDatabase db = getAppDatabase(Product_Activity.this);
//        return (db.ProductsDAO().getNewProductsCount("Passenger Car")+db.ProductsDAO().getNewProductsCount("Light Commercial"));
//    }
//
//    private int getheavyCommercialCount()
//    {
//        AppDatabase db = getAppDatabase(Product_Activity.this);
//        return db.ProductsDAO().getNewProductsCount("Heavy Commercial");
//    }
//
//    private int getWheelerCount()
//    {
//        AppDatabase db = getAppDatabase(Product_Activity.this);
//        return db.ProductsDAO().getNewProductsCount("2 Wheeler");
//    }

//    public void getRecords() {
//         AppDatabase db = getAppDatabase(Product_Activity.this);
//
////        List<Products> data = db.ProductsDAO().getPredictionList();
////
////        List<Object> Make = (List<Object>) CollectionUtils.collect(data, TransformerUtils.invokerTransformer("getMake"));
////        List<Object> Model = (List<Object>) CollectionUtils.collect(data, TransformerUtils.invokerTransformer("getModel"));
////        List<Object> Enginecode = (List<Object>) CollectionUtils.collect(data, TransformerUtils.invokerTransformer("getEnginecode"));
////        List<Object> Modelcode = (List<Object>) CollectionUtils.collect(data, TransformerUtils.invokerTransformer("getModelcode"));
////        List<Object> Gates_Part_Number = (List<Object>) CollectionUtils.collect(data, TransformerUtils.invokerTransformer("getGates_Part_Number"));
////
////
////
////        List<Object> allPrediction = new ArrayList<>();
////        allPrediction.addAll(Make);
////        allPrediction.addAll(Model);
////        allPrediction.addAll(Enginecode);
////        allPrediction.addAll(Modelcode);
////        allPrediction.addAll(Gates_Part_Number);
////
////
////
////        List<String> allPredictionString = new ArrayList<>();
////        for (Object object : allPrediction) {
////            allPredictionString.add(object != null ? object.toString() : null);
////        }
////
////        PredictionList.addAll(allPredictionString);
////        PredictionList = new ArrayList<String>(new HashSet<String>(PredictionList));
//
//
//        try {
//            List<String> makes = db.ProductsDAO().getMakes();
//            List<String> models = db.ProductsDAO().getModelss();
//            List<String> modelCode = db.ProductsDAO().getModelCode();
//            List<String> engineCode = db.ProductsDAO().getEngineCode();
//            List<String> partNumber = db.ProductsDAO().getGatesPartNumber();
//
//
//
//        List<String> productList=new ArrayList<>();
//        productList.addAll(makes);
//        productList.addAll(models);
//        productList.addAll(modelCode);
//        productList.addAll(engineCode);
//        productList.addAll(partNumber);
//
//        PredictionList = new ArrayList<String>(new HashSet<String>(productList));
//        }catch (Exception e)
//        {
//            e.printStackTrace();
//        }
////        ArrayList<String> maked=new ArrayList<>(new HashSet<String>(makes));
////        ArrayList<String> modeled=new ArrayList<>(new HashSet<String>(models));
////        ArrayList<String> modelCoded=new ArrayList<>(new HashSet<String>(modelCode));
////        ArrayList<String> engineCoded=new ArrayList<>(new HashSet<String>(engineCode));
////        ArrayList<String> partNumbered=new ArrayList<>(new HashSet<String>(partNumber));
//
////        ArrayList<String> removeDup=new ArrayList<>(new HashSet<String>(productList));
////        ArrayList<String> exl=new ArrayList<>();
////        Collections.sort(removeDup);
//
//
////        for(String name:removeDup)
////        {
////            if(!PredictionList.contains(name))
////            {
////                exl.add(name);
////            }
////        }
//
//
//        Log.d(TAG, "getRecords: ");
//
//
//    }


    @Override
    protected void onResume() {
        super.onResume();

//        if (myshare.getBoolean("islogin", false))
//            signout_relativelayout.setVisibility(View.VISIBLE);
//        else
//            signout_relativelayout.setVisibility(View.GONE);

    }

}
