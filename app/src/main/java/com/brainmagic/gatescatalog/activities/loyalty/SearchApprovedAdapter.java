package com.brainmagic.gatescatalog.activities.loyalty;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.brainmagic.gatescatalog.api.models.gates.searchapproved.Datum;
import com.brainmagic.gatescatalog.gatesloyalty.R;

import java.util.List;

public class SearchApprovedAdapter extends BaseAdapter {

    private Context mContext;
    private List<Datum> datumList;

    public SearchApprovedAdapter(Context mContext, List<Datum> datumList) {
        this.mContext = mContext;
        this.datumList = datumList;
    }

    @Override
    public int getCount() {
        return datumList.size();
    }

    @Override
    public Object getItem(int i) {
        return datumList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.approved_view_layout, viewGroup, false);
        TextView approvedName = view.findViewById(R.id.approved_name);
        TextView approvedMobileNo = view.findViewById(R.id.approved_mobile_no);
        TextView approvedEmail = view.findViewById(R.id.approved_email);
        TextView approvedUserType = view.findViewById(R.id.approved_user_type);
        TextView approvedShopName = view.findViewById(R.id.approved_shop_name);
        TextView approvedStatus = view.findViewById(R.id.approved_status);
        TextView approvedRegistrationType = view.findViewById(R.id.registration_type);

        String ApprovedName = datumList.get(i).getName();
        String ApprovedMobileNo = datumList.get(i).getMobileno();
        String ApprovedEmail = datumList.get(i).getEmail();
        String ApprovedUserType = datumList.get(i).getUsertype();
        String ApprovedShopName = String.valueOf(datumList.get(i).getShopName());
        String ApprovedStatus = datumList.get(i).getStatus();
        String ApprovedRegistrationType = datumList.get(i).getRegistrationType();


        approvedName.setText(ApprovedName);
        approvedMobileNo.setText(ApprovedMobileNo);
        approvedEmail.setText(ApprovedEmail);
        approvedUserType.setText(ApprovedUserType);
        approvedShopName.setText(ApprovedShopName);
        approvedStatus.setText(ApprovedStatus);
        approvedRegistrationType.setText(ApprovedRegistrationType);


        return view;
    }
}
