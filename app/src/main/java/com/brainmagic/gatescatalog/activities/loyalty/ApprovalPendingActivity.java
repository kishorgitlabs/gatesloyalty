package com.brainmagic.gatescatalog.activities.loyalty;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.Sample;
import com.brainmagic.gatescatalog.api.models.gates.newsearchpending.Datum;
import com.brainmagic.gatescatalog.api.models.gates.newsearchpending.NewSearchPending;
import com.brainmagic.gatescatalog.api.models.gates.pendinglist.PendingList;
import com.brainmagic.gatescatalog.api.models.gates.searchpending.SearchPending;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApprovalPendingActivity extends AppCompatActivity {

    private ListView listView;
    private List<Datum> sampleList;
    private Button search;
    private ApprovalAdapter approvalAdapter;
    private Spinner userTypeSpinner, stateWiseSpinner;
    private String[] userType = {"Type Of User", "Dealer / Retailer", "Mechanic"};
    private String[] stateWise = {"SELECT STATE", "TamilNadu", "Telungana"};

    //    private String[] userType = {"", "Dealer / Retailer", "Mechanic"};
//    private String[] stateWise = {"", "TamilNadu", "Telungana"};
    private String State, UserType;
    private Button reject, approval;
    //    private  SearchPendingAdapter adapter;
    private SearchPendingAdapter adapter;

    private FloatingActionButton back, home;
    private Alertbox alertbox;
    private Network_Connection_Activity connectionActivity;

    SharedPreferences myShare;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval_pending);
        listView = findViewById(R.id.approval_pending_list);
        userTypeSpinner = findViewById(R.id.user_type_spinner);
        stateWiseSpinner = findViewById(R.id.state_wise_spinner);
        alertbox = new Alertbox(ApprovalPendingActivity.this);
        reject = findViewById(R.id.delete_retailer);
        approval = findViewById(R.id.approve_retailer);
        back = findViewById(R.id.back);
        home = findViewById(R.id.home);
        sampleList = new ArrayList<>();
        search = findViewById(R.id.pending_search);
        connectionActivity = new Network_Connection_Activity(ApprovalPendingActivity.this);
        myShare = getSharedPreferences("Registration", MODE_PRIVATE);


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent home = new Intent(ApprovalPendingActivity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ApprovalPendingActivity.this, android.R.layout.simple_list_item_1, userType);
        userTypeSpinner.setAdapter(adapter);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(ApprovalPendingActivity.this, android.R.layout.simple_list_item_1, stateWise);
        stateWiseSpinner.setAdapter(arrayAdapter);



        /*userTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                UserType = adapterView.getItemAtPosition(i).toString();
                State = "";
//                approvalPendingList(State, UserType);
                String item = adapterView.getItemAtPosition(i).toString();
                if (item.equals("Type Of User")){
                    checkInternet();
                }else {
                    approvalPendingListFilter(State, UserType);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        stateWiseSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                State = adapterView.getItemAtPosition(i).toString();
                UserType = "";
//                approvalPendingList(State, UserType);
                String item = adapterView.getItemAtPosition(i).toString();
                if (item.equals("SELECT STATE")){
                    checkInternet();
                }else {
                    approvalPendingListFilter(State, UserType);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

//        checkInternet();

//        State  = stateWiseSpinner.getSelectedItem().toString();
//        UserType = userTypeSpinner.getSelectedItem().toString();

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                newApprovedPending(userTypeSpinner.getSelectedItem().toString(), stateWiseSpinner.getSelectedItem().toString());

                if (stateWiseSpinner.getSelectedItem().toString().equals("SELECT STATE")) {
                    State = "";
                    newApprovedPending(userTypeSpinner.getSelectedItem().toString(), State);
                } else if (userTypeSpinner.getSelectedItem().equals("Type Of User")) {
                    UserType = "";
                    newApprovedPending(UserType, stateWiseSpinner.getSelectedItem().toString());
                } else if (stateWiseSpinner.getSelectedItem().toString().equals("Type Of User") ) {
                    State = "";
                    UserType = "";
                    newApprovedPending(UserType, State);
                } else if ( userTypeSpinner.getSelectedItem().toString().equals("SELECT STATE")){

                }

                else {
                    newApprovedPending(userTypeSpinner.getSelectedItem().toString(),
                            stateWiseSpinner.getSelectedItem().toString());
                }

              /* if (stateWiseSpinner.getSelectedItem().toString().equals("SELECT STATE")){
                   State = "";
                   UserType = "";
                   newApprovedPending(UserType, State);
               }else if (userTypeSpinner.getSelectedItem().toString().equals("Type Of User")){
                   State = "";
                   UserType = "";
                   newApprovedPending(UserType, State);
               }else if (userTypeSpinner.getSelectedItem().toString().equals("Type Of User")){
                   UserType = "";
                   newApprovedPending(UserType, stateWiseSpinner.getSelectedItem().toString());
               }else if (stateWiseSpinner.getSelectedItem().toString().equals("SELECT STATE")){
                   State = "";
                   newApprovedPending(userTypeSpinner.getSelectedItem().toString(), State);
               }else {
                   newApprovedPending(userTypeSpinner.getSelectedItem().toString(),
                           stateWiseSpinner.getSelectedItem().toString());
               }*/
            }
        });

        if (stateWiseSpinner.getSelectedItem().toString().equals("SELECT STATE") &&
                userTypeSpinner.getSelectedItem().equals("Type Of User")){
            State = "";
            UserType = "";
            newApprovedPending(UserType, State);
        }

//        newApprovedPending(userTypeSpinner.getSelectedItem().toString(), stateWiseSpinner.getSelectedItem().toString());
//
//        if (!State.equals("SELECT STATE") || !UserType.equals("Type Of User")){
//            approvalPendingListFilter(State, UserType);
//        }
    }

    private void newApprovedPending(String userType, String state) {
        try {
            final ProgressDialog loading = ProgressDialog.show(ApprovalPendingActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

            APIService service = RetrofitClient.getApiService();

            Call<NewSearchPending> call = service.serachPending(
                    userType,
                    state
            );

            call.enqueue(new Callback<NewSearchPending>() {
                @Override
                public void onResponse(Call<NewSearchPending> call, Response<NewSearchPending> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        sampleList = response.body().getData();
                        adapter = new SearchPendingAdapter(ApprovalPendingActivity.this,
                                sampleList);
                        listView.setAdapter(adapter);
                    } else if (response.body().getResult().equals("Not Success")) {
                        alertbox.showAlertWithBack("No Record Found !");
                    } else {
                        alertbox.showAlertWithBack("Please try again later");
                    }
                }

                @Override
                public void onFailure(Call<NewSearchPending> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showAlertWithBack(getString(R.string.server_error));
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
            alertbox.showAlertWithBack(getString(R.string.internet_error_msg));
        }
    }


   /* private void checkInternet() {
        if (connectionActivity.CheckInternet()){
//            approvalPendingList();
        }else {
            alertbox.showAlertWithBack(getString(R.string.internet_error_msg));
        }
    }

    private void approvalPendingList() {
       try {

           final ProgressDialog loading = ProgressDialog.show(ApprovalPendingActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

           APIService service = RetrofitClient.getApiService();

           Call<PendingList> call = service.getApprovalList();

           call.enqueue(new Callback<PendingList>() {
               @Override
               public void onResponse(Call<PendingList> call, Response<PendingList> response) {
                   loading.dismiss();
                   if (response.body().getResult().equals("Success")){
                       sampleList = response.body().getData();

                       approvalAdapter = new ApprovalAdapter(ApprovalPendingActivity.this, sampleList);
                       listView.setAdapter(approvalAdapter);
                   }else if (response.body().getResult().equals("Not Success")){
                       alertbox.showAlertWithBack("No Record Found !");
                   }*//*else {
                       alertbox.showAlertWithBack("Please try again later");
                   }*//*
               }

               @Override
               public void onFailure(Call<PendingList> call, Throwable t) {
                   loading.dismiss();
                   alertbox.showAlertWithBack(getString(R.string.server_error));
               }
           });


       }catch (Exception e){
           e.printStackTrace();
           alertbox.showAlertWithBack("Please try again later");
       }
    }*/
}