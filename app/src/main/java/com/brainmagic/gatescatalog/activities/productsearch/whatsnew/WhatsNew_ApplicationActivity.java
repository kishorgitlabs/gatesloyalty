package com.brainmagic.gatescatalog.activities.productsearch.whatsnew;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.gatescatalog.activities.loyalty.ViewProfileActivity;
import com.bumptech.glide.Glide;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributor_Search_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.MoreDetailsActivity;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.adapter.Whats_New_Application_Adapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeModel;
import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeResult;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WhatsNew_ApplicationActivity extends AppCompatActivity {


    ImageView menu;

    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String Email, Mobno, Usertype, Country;
    private HorizontalScrollView horizontalScrollView;
    private ProgressDialog loading;
    private Whats_New_Application_Adapter adapter;
    private Alertbox alertBox;
    private ListView gridview;

    private List<String> idList;
    private List<String> SegmentList;
    private List<String> MakeList;
    private List<String> ModelList;

    private List<String> StrokeList;
    private List<String> ModelcodeList;
    private List<String> EnginecodeList;
    private List<String> Year_FromList;

    private List<String> Month_FromList;
    private List<String> Year_TillList;
    private List<String> Month_TillList;
    private List<String> Part_DescriptionList;

    private List<String> Gates_Part_NumberList;
    private List<String> appAtt;
    private List<String> ccList;
    private List<String> kwList;
    private List<String> yearTo;
    private List<String> IsApplicationList, ProductURL;
    private List<String> ProductAdditionalInfo,whatsNewApplication,supportedCountryList;
    private TextView ProfileName;
    private LinearLayout signout_relativelayout;
//    private ConstraintLayout signout_relativelayout;
    private FloatingActionButton back, home;
    private ViewGroup includeviewLayout;
    private ImageView profilePicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new__application);

        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        menu = (ImageView) findViewById(R.id.menu);

        includeviewLayout = findViewById(R.id.profile_logo);
        ProfileName = (TextView) includeviewLayout.findViewById(R.id.profilename);
        profilePicture = (ImageView) includeviewLayout.findViewById(R.id.profilepicture);
        signout_relativelayout =  findViewById(R.id.signout);
        signout_relativelayout.setVisibility(View.GONE);

        horizontalScrollView = (HorizontalScrollView) findViewById(R.id.horilist);
        horizontalScrollView.setVisibility(View.GONE);

        gridview = (ListView) findViewById(R.id.gridView);

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();

        Email = myshare.getString("email", "");
        Mobno = myshare.getString("MobNo", "");
        Usertype = myshare.getString("Usertype", "");
        Country = myshare.getString("Country", "");
        ProfileName.setText(myshare.getString("name", "")+" , "+Country);

        if (!myshare.getString("profile_path", "").equals(""))
            Glide.with(WhatsNew_ApplicationActivity.this)
                    .load(new File(myshare.getString("profile_path", "")))
                    .into(profilePicture);

        if (myshare.getBoolean("islogin", false))
            signout_relativelayout.setVisibility(View.VISIBLE);
        else
            signout_relativelayout.setVisibility(View.GONE);

        alertBox = new Alertbox(WhatsNew_ApplicationActivity.this);

//        signout_relativelayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                editor.putBoolean("islogin", false).commit();
//
//                final Snackbar mySnackbar = Snackbar.make(findViewById(R.id.new_application), getString(R.string.channel_network_sign_out), Snackbar.LENGTH_INDEFINITE);
//                mySnackbar.setActionTextColor(getResources().getColor(R.color.red));
//                mySnackbar.setAction(getString(R.string.okay_button), new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        mySnackbar.dismiss();
//                        Intent intent = getIntent();
//                        finish();
//                        startActivity(intent);
//
//                    }
//                });
//                mySnackbar.show();
//            }
//        });

        final ImageView downArrow=includeviewLayout.findViewById(R.id.down_arrow);
        final RelativeLayout userData=includeviewLayout.findViewById(R.id.user_data);
//        ViewGroup homeLayout=findViewById(R.id.new_application);

//        downArrow.setOnClickListener(new View.OnClickListener() {
//            boolean visible;
//            @Override
//            public void onClick(View v) {
//                ChangeBounds changeBounds=new ChangeBounds();
//                changeBounds.setDuration(600L);
//                TransitionManager.beginDelayedTransition(includeviewLayout,changeBounds);
//                visible = !visible;
//                if(visible)
//                    downArrow.animate().rotation(180).setInterpolator(new LinearInterpolator()).setDuration(500);
//                else
//                    downArrow.animate().rotation(0).setInterpolator(new LinearInterpolator()).setDuration(500);
//                userData.setVisibility(visible? View.VISIBLE: View.GONE);
//            }
//        });


        idList = new ArrayList<String>();
        MakeList = new ArrayList<String>();
        ModelList = new ArrayList<String>();
        SegmentList = new ArrayList<String>();
        StrokeList = new ArrayList<String>();
        ModelcodeList = new ArrayList<String>();
        EnginecodeList = new ArrayList<String>();
        Year_FromList = new ArrayList<String>();

        Month_FromList = new ArrayList<String>();
        Year_TillList = new ArrayList<String>();
        Month_TillList = new ArrayList<String>();
        Part_DescriptionList = new ArrayList<String>();

        Gates_Part_NumberList = new ArrayList<String>();
        appAtt = new ArrayList<String>();
        ccList = new ArrayList<String>();
        kwList = new ArrayList<String>();
        yearTo = new ArrayList<String>();
        IsApplicationList = new ArrayList<String>();
        ProductURL = new ArrayList<String>();
        ProductAdditionalInfo = new ArrayList<String>();
        whatsNewApplication = new ArrayList<String>();
        supportedCountryList = new ArrayList<String>();
        menu = (ImageView) findViewById(R.id.menu);


        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
              startIntent((EngineCodeResult) parent.getAdapter().getItem(position));
            }
        });


        checkinternet();


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(WhatsNew_ApplicationActivity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(WhatsNew_ApplicationActivity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
//                                Intent home = new Intent(HomeScreenActivity.this, HomeScreenActivity.class);
//                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(WhatsNew_ApplicationActivity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.viewprofile:
                                startActivity(new Intent(WhatsNew_ApplicationActivity.this, ViewProfileActivity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(WhatsNew_ApplicationActivity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(WhatsNew_ApplicationActivity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(WhatsNew_ApplicationActivity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.scan:
                                startActivity(new Intent(WhatsNew_ApplicationActivity.this, ScanActivity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(WhatsNew_ApplicationActivity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(WhatsNew_ApplicationActivity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(WhatsNew_ApplicationActivity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(WhatsNew_ApplicationActivity.this, Distributor_Search_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(WhatsNew_ApplicationActivity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(WhatsNew_ApplicationActivity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });

//        Animation makeInAnimation = AnimationUtils.makeInAnimation(getBaseContext(), false);
//        makeInAnimation.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationEnd(Animation animation) {
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//            }
//
//            @Override
//            public void onAnimationStart(Animation animation) {
//                home.setVisibility(View.VISIBLE);
//            }
//        });
//
//        Animation makeOutAnimation = AnimationUtils.makeOutAnimation(getBaseContext(), true);
//        makeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                home.setVisibility(View.INVISIBLE);
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//            }
//
//            @Override
//            public void onAnimationStart(Animation animation) {
//            }
//        });


//        if (home.isShown()) {
//            home.startAnimation(makeOutAnimation);
//            back.startAnimation(makeOutAnimation);
//        }
//
//        if (!home.isShown()) {
//            home.startAnimation(makeInAnimation);
//            back.startAnimation(makeInAnimation);
//        }


    }

//    private void LoadListView() {
//        idList.add("1");
//        SegmentList.add("PCV");
//        MakeList.add("Tata");
//        ModelList.add("Tata Ace");
//        StrokeList.add("1.2");
//        ModelcodeList.add("Tata Ace");
//        EnginecodeList.add("475");
//        Year_FromList.add("2005");
//        Month_FromList.add("01");
//        Year_TillList.add("Tata Ace");
//        Month_TillList.add("12");
//        Part_DescriptionList.add("Tata Ace");
//        Gates_Part_NumberList.add("5PK354");
//        ProductAdditionalInfo.add("Tata Ace PCV");
//        appAtt.add("ALT");
//        yearTo.add("2020");
//        ccList.add("1193");
//        kwList.add("66");
////        loading.dismiss();
//        adapter = new Whats_New_Application_Adapter(WhatsNew_ApplicationActivity.this, idList,MakeList,ModelList,StrokeList,
//                ModelcodeList,EnginecodeList,Gates_Part_NumberList
//        );
//        adapter.notifyDataSetChanged();
//        gridview.setAdapter(adapter);
//        horizontalScrollView.setVisibility(View.VISIBLE);
//    }

    private void startIntent(EngineCodeResult data) {
        startActivity(new Intent(WhatsNew_ApplicationActivity.this, MoreDetailsActivity.class)
                .putExtra("navtxt","What's New > "+data.getArticle())
                .putExtra("partNumber",data.getArticle())
                .putExtra("segment",data.getSegment())
                .putExtra("OEM",data.getMake())
                .putExtra("MODEL",data.getModel())
                .putExtra("engineCode",data.getEnginecode())
                .putExtra("modelCode",data.getModelcode())
                .putExtra("yearFrom",data.getYearFrom())
                .putExtra("yearTo",data.getYearTill())
                .putExtra("stroke",data.getStroke())
                .putExtra("cc",data.getCC())
                .putExtra("kw",data.getKW())
                .putExtra("level",data.getLevel1())
                .putExtra("searchType","WhatsNew")
                .putExtra("data",data)
        );




//        try {
//            String Partnumber = Gates_Part_NumberList.get(p);
//
//            loading = ProgressDialog.show(WhatsNew_ApplicationActivity.this,"",getString(R.string.loadings),false,false);
//
//            APIServices service = RetroClient.getApiService();
//
//            Call<ThumbImage> call = service.MASTER_IMAGE_CALL(Partnumber);
//            call.enqueue(new retrofit2.Callback<ThumbImage>() {
//                @Override
//                public void onResponse(Call<ThumbImage> call, retrofit2.Response<ThumbImage> response) {
//
//                    if (response.body().getResult().equals("Success")) {
//                        List<LogDetailResult> imageList=response.body().getData();
//
//                        Intent gomore = new Intent(WhatsNew_ApplicationActivity.this, Whats_New_More_Details.class);
//                        gomore.putExtra("OEM", MakeList.get(p));
//                        gomore.putExtra("MODEL", ModelList.get(p));
//                        gomore.putExtra("MODEL_CODE", ModelcodeList.get(p));
//                        gomore.putExtra("yearfrom", Year_FromList.get(p));
//                        gomore.putExtra("ENGINE_CODE", EnginecodeList.get(p));
//                        gomore.putExtra("PARTDESC", Part_DescriptionList.get(p));
//                        gomore.putExtra("GATESPART", Gates_Part_NumberList.get(p));
//                        gomore.putExtra("APPATT", appAtt.get(p));
//                        gomore.putExtra("CC", ccList.get(p));
//                        gomore.putExtra("KW", kwList.get(p));
//                        gomore.putExtra("yearto", yearTo.get(p));
//                        gomore.putExtra("STROKE", StrokeList.get(p));
//                        gomore.putExtra("MONTHFROM", Month_FromList.get(p));
//                        gomore.putExtra("YEARTILL", Year_TillList.get(p));
//                        gomore.putExtra("MONTHTILL", Month_TillList.get(p));
//                        gomore.putExtra("SEGMENT", SegmentList.get(p));
////                        gomore.putExtra("ProductURL", ProductURL.get(p));
//                        gomore.putExtra("thumbuimage", (Serializable) null);
//                        gomore.putExtra("ProductAdditionalInfo", ProductAdditionalInfo.get(p));
//                        gomore.putExtra("naviText", "What's New > Tata ");
//                        gomore.putExtra("supportedCountryList", supportedCountryList.get(p));
//
//
//                        startActivity(gomore);
//                        loading.dismiss();
//
//                    } else {
//                        //Toast.makeText(getApplicationContext(), "Error in Fetch LogDetailResult", Toast.LENGTH_LONG).show();
//
//                        Intent gomore = new Intent(WhatsNew_ApplicationActivity.this, Whats_New_More_Details.class);
//                        gomore.putExtra("OEM", MakeList.get(p));
//                        gomore.putExtra("MODEL", ModelList.get(p));
//                        gomore.putExtra("MODEL_CODE", ModelcodeList.get(p));
//                        gomore.putExtra("YEARFROM", Year_FromList.get(p));
//                        gomore.putExtra("ENGINE_CODE", EnginecodeList.get(p));
//                        gomore.putExtra("PARTDESC", Part_DescriptionList.get(p));
//                        gomore.putExtra("GATESPART", Gates_Part_NumberList.get(p));
//                        gomore.putExtra("EQUIPMENT1", EquipmentList.get(p));
//                        gomore.putExtra("EQUIPMENT2", Equipment_2List.get(p));
//                        gomore.putExtra("EQUIPMENTDATEF", Equipment_Date_FromList.get(p));
//                        gomore.putExtra("EQUIPMENTDATEFT", Equipment_Date_ToList.get(p));
//                        gomore.putExtra("STROKE", StrokeList.get(p));
//                        gomore.putExtra("MONTHFROM", Month_FromList.get(p));
//                        gomore.putExtra("YEARTILL", Year_TillList.get(p));
//                        gomore.putExtra("MONTHTILL", Month_TillList.get(p));
//                        gomore.putExtra("SEGMENT", SegmentList.get(p));
//                        gomore.putExtra("ProductURL", ProductURL.get(p));
//                        gomore.putExtra("thumbuimage", (Serializable) null);
//                        gomore.putExtra("ProductAdditionalInfo", ProductAdditionalInfo.get(p));
//                        gomore.putExtra("supportedCountryList", supportedCountryList.get(p));
//
//                        startActivity(gomore);
//                        loading.dismiss();
//
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ThumbImage> call, Throwable t) {
//                    Intent gomore = new Intent(WhatsNew_ApplicationActivity.this, Whats_New_More_Details.class);
//                    gomore.putExtra("OEM", MakeList.get(p));
//                    gomore.putExtra("MODEL", ModelList.get(p));
//                    gomore.putExtra("MODEL_CODE", ModelcodeList.get(p));
//                    gomore.putExtra("YEARFROM", Year_FromList.get(p));
//                    gomore.putExtra("ENGINE_CODE", EnginecodeList.get(p));
//                    gomore.putExtra("PARTDESC", Part_DescriptionList.get(p));
//                    gomore.putExtra("GATESPART", Gates_Part_NumberList.get(p));
//                    gomore.putExtra("EQUIPMENT1", EquipmentList.get(p));
//                    gomore.putExtra("EQUIPMENT2", Equipment_2List.get(p));
//                    gomore.putExtra("EQUIPMENTDATEF", Equipment_Date_FromList.get(p));
//                    gomore.putExtra("EQUIPMENTDATEFT", Equipment_Date_ToList.get(p));
//                    gomore.putExtra("STROKE", StrokeList.get(p));
//                    gomore.putExtra("MONTHFROM", Month_FromList.get(p));
//                    gomore.putExtra("YEARTILL", Year_TillList.get(p));
//                    gomore.putExtra("MONTHTILL", Month_TillList.get(p));
//                    gomore.putExtra("SEGMENT", SegmentList.get(p));
//                    gomore.putExtra("ProductURL", ProductURL.get(p));
//                    gomore.putExtra("thumbuimage", (Serializable) null);
//                    gomore.putExtra("ProductAdditionalInfo", ProductAdditionalInfo.get(p));
//                    gomore.putExtra("supportedCountryList", supportedCountryList.get(p));
//
//                    startActivity(gomore);
//                    loading.dismiss();
//
//
//                }
//            });
//        } catch (Exception e) {
//            loading.dismiss();
//            Intent gomore = new Intent(WhatsNew_ApplicationActivity.this, Whats_New_More_Details.class);
//            gomore.putExtra("OEM", MakeList.get(p));
//            gomore.putExtra("MODEL", ModelList.get(p));
//            gomore.putExtra("MODEL_CODE", ModelcodeList.get(p));
//            gomore.putExtra("YEARFROM", Year_FromList.get(p));
//            gomore.putExtra("ENGINE_CODE", EnginecodeList.get(p));
//            gomore.putExtra("PARTDESC", Part_DescriptionList.get(p));
//            gomore.putExtra("GATESPART", Gates_Part_NumberList.get(p));
//            gomore.putExtra("EQUIPMENT1", EquipmentList.get(p));
//            gomore.putExtra("EQUIPMENT2", Equipment_2List.get(p));
//            gomore.putExtra("EQUIPMENTDATEF", Equipment_Date_FromList.get(p));
//            gomore.putExtra("EQUIPMENTDATEFT", Equipment_Date_ToList.get(p));
//            gomore.putExtra("STROKE", StrokeList.get(p));
//            gomore.putExtra("MONTHFROM", Month_FromList.get(p));
//            gomore.putExtra("YEARTILL", Year_TillList.get(p));
//            gomore.putExtra("MONTHTILL", Month_TillList.get(p));
//            gomore.putExtra("SEGMENT", SegmentList.get(p));
//            gomore.putExtra("ProductURL", ProductURL.get(p));
//            gomore.putExtra("thumbuimage", (Serializable) null);
//            gomore.putExtra("ProductAdditionalInfo", ProductAdditionalInfo.get(p));
//            gomore.putExtra("supportedCountryList", supportedCountryList.get(p));
//
//            startActivity(gomore);
//            e.printStackTrace();
//
//
//        }

    }


    @Override
    protected void onResume() {
        super.onResume();

//        if (myshare.getBoolean("islogin", false))
//            signout_relativelayout.setVisibility(View.VISIBLE);
//        else
//            signout_relativelayout.setVisibility(View.GONE);


    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }


    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(WhatsNew_ApplicationActivity.this);

        if (connection.CheckInternet()) {
            callApiServiceForWhatsNewApplication();
//            LoadListView();
        } else {
            alertBox.showAlertWithBack(getString(R.string.internet_error_msg));
        }


    }

    private void callApiServiceForWhatsNewApplication()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(WhatsNew_ApplicationActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetrofitClient.getApiService();
            Call<EngineCodeModel> call = service.getProductDetailWhatsNew();

            call.enqueue(new Callback<EngineCodeModel>() {
                @Override
                public void onResponse(Call<EngineCodeModel> call, Response<EngineCodeModel> response) {
                    progressDialog.dismiss();

                    try{
                        if("Success".equals(response.body().getResult()))
                        {
//                            adapter =new Notification_Adapter(WhatsNew_ApplicationActivity.this,response.body().getData());
                            adapter = new Whats_New_Application_Adapter(WhatsNew_ApplicationActivity.this, response.body().getData());
                            gridview.setAdapter(adapter);
                            horizontalScrollView.setVisibility(View.VISIBLE);
                        }
                        else {
                            alertBox.showAlertWithBack("No Record Found");
                        }
                    }catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alertBox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<EngineCodeModel> call, Throwable t) {
                    progressDialog.dismiss();
                    alertBox.showAlertWithBack("Failed to Reach the Server. Please try again Later");

                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alertBox.showAlertWithBack("Exception Occurred. Please try again Later");
        }
    }

    private void GetWhatsNewProducts() {
//        loading = ProgressDialog.show(this, getString(R.string.searching), getString(R.string.please_wait), false, false);
//        final RestAdapter adapter = new RestAdapter.Builder()
//                .setEndpoint(ApiUtils.ROOT_URL) //Setting the Root URL
//                .build();
//
//        //Creating object for our interface
//        APIServices api = adapter.create(APIServices.class);
//
//        if(Usertype.equals("Gates Employee") && Country.equals("All Countries"))
//        {
//            Country="All";
//        }
//        api.getWhatsNewApplication(Country, new Callback<Response>() {
//
//
//            @Override
//            public void success(Response result, Response response2) {
//
//
//                BufferedReader reader = null;
//
//                //An string to store output from the server
//                String outputstring = "";
//                try {
//                    //Initializing buffered reader
//                    reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
//                    outputstring = reader.readLine();
//                    JSONObject jsonObj = new JSONObject(outputstring);
//                    // Getting JSON Array node
//                    Log.d("Result", jsonObj.get("result").toString());
//                    if (jsonObj.get("result").toString().equals("Success")) {
//                        JSONArray data = jsonObj.getJSONArray("data");
//                        // looping through All Contacts
//                        for (int i = 0; i < data.length(); i++) {
//                            String supportedCountry="";
//                            JSONObject c = data.getJSONObject(i);
//                            if (c.getString("id") != null)
//                                idList.add(c.getString("id"));
//                            if (c.getString("Segment") != null)
//                                SegmentList.add(c.getString("Segment"));
//                            if (c.getString("Make") != null)
//                                MakeList.add(c.getString("Make"));
//                            if (c.getString("Model") != null)
//                                ModelList.add(c.getString("Model"));
//                            if (c.getString("Stroke") != null)
//                                StrokeList.add(c.getString("Stroke"));
//                            if (c.getString("Modelcode") != null)
//                                ModelcodeList.add(c.getString("Modelcode"));
//                            if (c.getString("Enginecode") != null)
//                                EnginecodeList.add(c.getString("Enginecode"));
//                            if (c.getString("Year_From") != null)
//                                Year_FromList.add(c.getString("Year_From"));
//                            if (c.getString("Month_From") != null)
//                                Month_FromList.add(c.getString("Month_From"));
//                            if (c.getString("Year_Till") != null)
//                                Year_TillList.add(c.getString("Year_Till"));
//                            if (c.getString("Month_Till") != null)
//                                Month_TillList.add(c.getString("Month_Till"));
//
//                            if (c.getString("PartDescription") != null)
//                                Part_DescriptionList.add(c.getString("PartDescription"));
//                            if (c.getString("Gates_Part_Number") != null)
//                                Gates_Part_NumberList.add(c.getString("Gates_Part_Number"));
//                            if (c.getString("Equipment") != null)
//                                EquipmentList.add(c.getString("Equipment"));
//                            if (c.getString("Equipment_2") != null)
//                                Equipment_2List.add(c.getString("Equipment_2"));
//                            if (c.getString("Equipment_Date_From") != null)
//                                Equipment_Date_FromList.add(c.getString("Equipment_Date_From"));
//                            if (c.getString("Equipment_Date_To") != null)
//                                Equipment_Date_ToList.add(c.getString("Equipment_Date_To"));
//                            if (c.getString("ProductURL") != null)
//                                ProductURL.add(c.getString("ProductURL"));
//                            if (c.getString("ProductAdditionalInfo") != null)
//                                ProductAdditionalInfo.add(c.getString("ProductAdditionalInfo"));
//                            if (c.getString("New") != null)
//                                whatsNewApplication.add(c.getString("New"));
//                            if(c.getBoolean("ISIndonesia"))
//                            {
//                                supportedCountry="Indonesia";
//                            }
//                            if(c.getBoolean("ISBrunei"))
//                            {
//                                if(!TextUtils.isEmpty(supportedCountry))
//                                {
//                                    supportedCountry+=", ";
//                                }
//                                supportedCountry+="Brunei";
//                            }
//                            if(c.getBoolean("ISCambodia"))
//                            {
//                                if(!TextUtils.isEmpty(supportedCountry))
//                                {
//                                    supportedCountry+=", ";
//                                }
//                                supportedCountry+="Cambodia";
//                            }
//                            if(c.getBoolean("ISMalaysia"))
//                            {
//                                if(!TextUtils.isEmpty(supportedCountry))
//                                {
//                                    supportedCountry+=", ";
//                                }
//                                supportedCountry+="Malaysia";
//                            }
//                            if(c.getBoolean("ISMyanmar"))
//                            {
//                                if(!TextUtils.isEmpty(supportedCountry))
//                                {
//                                    supportedCountry+=", ";
//                                }
//                                supportedCountry+="Myanmar";
//                            }
//                            if(c.getBoolean("ISPhilippines"))
//                            {
//                                if(!TextUtils.isEmpty(supportedCountry))
//                                {
//                                    supportedCountry+=", ";
//                                }
//                                supportedCountry+="Philippines";
//                            }
//                            if(c.getBoolean("ISSingapore"))
//                            {
//                                if(!TextUtils.isEmpty(supportedCountry))
//                                {
//                                    supportedCountry+=", ";
//                                }
//                                supportedCountry+="Singapore";
//                            }
//                            if(c.getBoolean("ISThailand"))
//                            {
//                                if(!TextUtils.isEmpty(supportedCountry))
//                                {
//                                    supportedCountry+=", ";
//                                }
//                                supportedCountry+="Thailand";
//                            }
//                            if(c.getBoolean("ISVietnam"))
//                            {
//                                if(!TextUtils.isEmpty(supportedCountry))
//                                {
//                                    supportedCountry+=", ";
//                                }
//                                supportedCountry+="Vietnam";
//                            }
//
//                            supportedCountryList.add(supportedCountry);
//
//                        }
//                        if (!idList.isEmpty()) {
//                            LoadListView();
//                        } else {
//                            loading.dismiss();
//                            box.showAlertWithBack(getString(R.string.no_new_application) + Country + getString(R.string.country));
//                        }
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                loading.dismiss();
//                error.printStackTrace();
//                box.showAlertWithBack(getResources().getString(R.string.server_error));
//            }
//        });

    }




}
