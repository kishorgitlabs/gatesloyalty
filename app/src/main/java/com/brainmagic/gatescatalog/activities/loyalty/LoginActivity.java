package com.brainmagic.gatescatalog.activities.loyalty;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.alternativemobile.AlternativeMobileNo;
import com.brainmagic.gatescatalog.api.models.changepassword.ChangePassword;
import com.brainmagic.gatescatalog.api.models.forgetpassword.ForgetPassword;
import com.brainmagic.gatescatalog.api.models.gateslogin.GatesLogin;
import com.brainmagic.gatescatalog.api.models.scanlogin.ScanLogin;
import com.brainmagic.gatescatalog.gatesloyalty.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    public static final String TAG = LoginActivity.class.getSimpleName();

    private EditText username, password, newPassword;
    private String UserName, Password, ForgetPassword, AlternativeNo;
    private Button loginbtn, changePassBtn;
    private Alertbox alertbox;
    private TextView header, updatedPassword, forgetPassword, didNotRecieveLogin;
    private ImageView back, home, close;
    AlertDialog alertDialog;

    private boolean isPasswordChange = false;

    private AlertMsg alertMsg;

    private String Id, Name, MobileNo, BussinessName, UserType, Address, Latitude, Longitude;

    SharedPreferences myShare;
    SharedPreferences.Editor sharedEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        username = findViewById(R.id.user_name_gates);
        password = findViewById(R.id.password_gates);
        loginbtn = findViewById(R.id.login_gates);
        header = findViewById(R.id.flowtext);
        back = findViewById(R.id.back);
        home = findViewById(R.id.home);
        forgetPassword = findViewById(R.id.forget_password);
        didNotRecieveLogin = findViewById(R.id.did_not_recieve_details);
        alertbox = new Alertbox(LoginActivity.this);
        alertMsg = new AlertMsg(LoginActivity.this);
        back.setVisibility(View.GONE);


        myShare = getSharedPreferences("Registration", MODE_PRIVATE);

        AlternativeNo = myShare.getString("alternativeNo","");

        sharedEditor = myShare.edit();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, HomeScreenActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });



        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.brainmagic.gatescatalog.gates",
                    PackageManager.GET_SIGNATURES);
            Log.d("created", "afterpackage");
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.wtf("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("KeyHash", e.getMessage());

        } catch (NoSuchAlgorithmException e) {
            Log.d(TAG, "KeyHash: ", e);
        }

        try {
            Id = myShare.getString("id", "");
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

        MobileNo = myShare.getString("MobNo", "");
        Name = myShare.getString("Name", "");
        UserType = myShare.getString("Usertype", "");
        BussinessName = myShare.getString("Bname", "");
        Address = myShare.getString("address", "");
        Longitude = myShare.getString("longitude", "");
        Latitude = myShare.getString("latitude", "");

        if (UserType.equals("Gates Employee")) {
            header.setText(getString(R.string.gates_login));
            forgetPassword.setVisibility(View.GONE);
            didNotRecieveLogin.setVisibility(View.GONE);
        } else if (UserType.equals("Mechanic")) {
            header.setText(R.string.mechanic_login);
            forgetPassword.setVisibility(View.VISIBLE);
            didNotRecieveLogin.setVisibility(View.VISIBLE);
        } else if (UserType.equals("Dealer / Retailer")) {
            header.setText(R.string.retailer_login);
            forgetPassword.setVisibility(View.VISIBLE);
            didNotRecieveLogin.setVisibility(View.VISIBLE);
        }


        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserName = username.getText().toString();
                Password = password.getText().toString();

                if (UserName.equals("") && Password.equals("")) {
                    Toast.makeText(LoginActivity.this, "Enter user name and password", Toast.LENGTH_LONG).show();
                } else if (UserName.equals("")) {
//                    userNameGates.setFocusable(true);
                    username.setError("Enter User Name");
                } else if (Password.equals("")) {
//                    passwordGates.setFocusable(true);
                    password.setError("Enter Password");
                } else {
                    if (UserType.equals("Gates Employee")) {
                        login();
                    } else {
                        otherLogin();
                    }
                }
            }
        });

        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgetPass();
            }
        });

        didNotRecieveLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                didNotRecieve();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void didNotRecieve() {
        try {
            AlertDialog alertDialog = new AlertDialog.Builder(
                    LoginActivity.this).create();

            LayoutInflater inflater = ((Activity) LoginActivity.this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.did_not_recieve_layout, null);
            alertDialog.setView(dialogView);
            ImageView close = dialogView.findViewById(R.id.close);
            EditText alterMobile = dialogView.findViewById(R.id.alternative_mobile_no);
            TextView update = dialogView.findViewById(R.id.updated);
            Button sent = dialogView.findViewById(R.id.sent_btn);

//            alterMobile.setText(AlternativeNo);

            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            sent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String AlterMobile = alterMobile.getText().toString();

                    if (AlterMobile.equals("")){
                        alterMobile.setError("Enter Alternative Mobile No");
                    }else {
                        final ProgressDialog loading = ProgressDialog.show(LoginActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

                        APIService service = RetrofitClient.getApiService();

                        Call<AlternativeMobileNo> call = service.alterMobileNo(
                                Id,
                                AlterMobile,
                                UserType
                        );

                        call.enqueue(new Callback<AlternativeMobileNo>() {
                            @Override
                            public void onResponse(Call<AlternativeMobileNo> call, Response<AlternativeMobileNo> response) {
                                loading.dismiss();
                                if (response.body().getResult().equals("Success")){
                                    alterMobile.setVisibility(View.GONE);
                                    update.setVisibility(View.VISIBLE);
                                    update.setText("Login Details Sent");
                                    sent.setVisibility(View.GONE);
                                }else if (response.body().getResult().equals("Not Success")){
                                    alterMobile.setVisibility(View.GONE);
                                    update.setVisibility(View.VISIBLE);
                                    update.setText("Please Try Again Later");
                                    sent.setVisibility(View.GONE);
                                }else {
                                    alterMobile.setVisibility(View.GONE);
                                    update.setVisibility(View.VISIBLE);
                                    update.setText(getString(R.string.internet_error_msg));
                                    sent.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onFailure(Call<AlternativeMobileNo> call, Throwable t) {
                                loading.dismiss();
                                alterMobile.setVisibility(View.GONE);
                                update.setVisibility(View.VISIBLE);
                                update.setText(getString(R.string.server_error));
                                sent.setVisibility(View.GONE);
                            }
                        });
                    }
                }
            });


            Window window = alertDialog.getWindow();
            window.setLayout(600, 600);
            alertDialog.show();
        }catch (Exception ex){
            ex.printStackTrace();
            alertbox.showAlert(getString(R.string.internet_error_msg));
        }
    }

    private void forgetPass() {
        try {
            AlertDialog alertDialog = new AlertDialog.Builder(
                    LoginActivity.this).create();

            LayoutInflater inflater = ((Activity) LoginActivity.this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.change_password_layout, null);
            alertDialog.setView(dialogView);
            ImageView close = dialogView.findViewById(R.id.close);
            EditText enterMobile = dialogView.findViewById(R.id.new_password);
            TextView update = dialogView.findViewById(R.id.updated_password);
            Button sent = dialogView.findViewById(R.id.change_password_btn);
            sent.setText("Sent");
            enterMobile.setHint("Enter Mobile No");

            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            sent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String EnterMobile = enterMobile.getText().toString();
                    if (EnterMobile.equals("")){
                        enterMobile.setError("Enter Mobile Number");
                    }else {
                        final ProgressDialog loading = ProgressDialog.show(LoginActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

                        APIService service = RetrofitClient.getApiService();

                        Call<ForgetPassword> call = service.forgetPassword(
                                Id,
                                EnterMobile,
                                UserType
                        );

                        call.enqueue(new Callback<ForgetPassword>() {
                            @Override
                            public void onResponse(Call<ForgetPassword> call, Response<ForgetPassword> response) {
                                loading.dismiss();
                                if (response.body().getResult().equals("Success")){
                                    enterMobile.setVisibility(View.GONE);
                                    update.setVisibility(View.VISIBLE);
                                    update.setText("Password Sent");
                                    sent.setVisibility(View.GONE);
                                }else if (response.body().getResult().equals("Not Success")){
                                    enterMobile.setVisibility(View.GONE);
                                    update.setVisibility(View.VISIBLE);
                                    update.setText("Please Try Again Later");
                                    sent.setVisibility(View.GONE);
                                }else {
                                    enterMobile.setVisibility(View.GONE);
                                    update.setVisibility(View.VISIBLE);
                                    update.setText("Please Try Again Later");
                                    sent.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onFailure(Call<ForgetPassword> call, Throwable t) {
                                loading.dismiss();
                                enterMobile.setVisibility(View.GONE);
                                update.setVisibility(View.VISIBLE);
                                update.setText(getString(R.string.server_error));
                                sent.setVisibility(View.GONE);
                            }
                        });
                    }
                }
            });


            Window window = alertDialog.getWindow();
            window.setLayout(600, 600);
            alertDialog.show();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void otherLogin() {
        try {

            final ProgressDialog loading = ProgressDialog.show(LoginActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

            APIService service = RetrofitClient.getApiService();

            Call<ScanLogin> call = service.scanLogin(
                    Id,
                    UserName,
                    Password,
                    UserType
            );
            call.enqueue(new Callback<ScanLogin>() {
                @Override
                public void onResponse(Call<ScanLogin> call, Response<ScanLogin> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")) {

                        if (!UserName.equals(Password)) {
//                            Toast.makeText(LoginActivity.this, "Success", Toast.LENGTH_SHORT).show();

                            if (UserType.equalsIgnoreCase("Dealer / Retailer")) {
                                sharedEditor.putString("UserName", UserName);
                                sharedEditor.putString("Password", Password);
                                sharedEditor.putString("MobileNumber", response.body().getData().getMobileno());
//                        sharedEditor.putString("emailGates", response.body().getData().getEmail());
                                sharedEditor.putLong("ID", response.body().getData().getId());
                                sharedEditor.putBoolean("gatesLogin", true);
                                sharedEditor.commit();
                                Intent dash = new Intent(getApplicationContext(), ScanActivity.class);
                                startActivity(dash);
                                finish();
                            } else if (UserType.equalsIgnoreCase("Mechanic")) {
                                sharedEditor.putString("UserName", UserName);
                                sharedEditor.putString("Password", Password);
                                sharedEditor.putString("MobileNumber", response.body().getData().getMobileno());
//                        sharedEditor.putString("emailGates", response.body().getData().getEmail());
                                sharedEditor.putLong("ID", response.body().getData().getId());
                                sharedEditor.putBoolean("gatesLogin", true);
                                sharedEditor.commit();
                                Intent dash = new Intent(getApplicationContext(), DashboardActivity.class);
                                startActivity(dash);
                                finish();

                            } else {
                                alertbox.showAlertWithBack("Please try again");
                            }
                        } else {
                            changePassword();
                        }

                    } else if (response.body().getResult().equals("Not Success")) {
                        alertbox.showAlert("Invalid Username and Password");
                    } else {
                        alertbox.showAlertWithBack("Please try again");
                    }
                }

                @Override
                public void onFailure(Call<ScanLogin> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showAlert(getString(R.string.server_error));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            alertbox.showAlert(getString(R.string.internet_error_msg));
        }
    }

    private void changePassword() {
        try {
            alertDialog = new AlertDialog.Builder(
                    LoginActivity.this).create();

            LayoutInflater inflater = ((Activity) LoginActivity.this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.change_password_layout, null);
            alertDialog.setView(dialogView);
            close = dialogView.findViewById(R.id.close);
            newPassword = dialogView.findViewById(R.id.new_password);
            updatedPassword = dialogView.findViewById(R.id.updated_password);

            changePassBtn = dialogView.findViewById(R.id.change_password_btn);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            changePassBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String NewPassword = newPassword.getText().toString();

                    if (NewPassword.equals("")) {
                        newPassword.setError("Enter new Password");
                    } else {
                        newPassChange(NewPassword);
                    }
                }
            });

            Window window = alertDialog.getWindow();
            window.setLayout(600, 600);
            alertDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
            alertbox.showAlert(getString(R.string.internet_error_msg));
        }
    }

    private void newPassChange(String NewPass) {
        try {

            final ProgressDialog loading = ProgressDialog.show(LoginActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

            APIService service = RetrofitClient.getApiService();

            Call<ChangePassword> call = service.changePassword(
                    Id,
                    UserName,
                    NewPass
            );

            call.enqueue(new Callback<ChangePassword>() {
                @Override
                public void onResponse(Call<ChangePassword> call, Response<ChangePassword> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        newPassword.setVisibility(View.GONE);
                        updatedPassword.setVisibility(View.VISIBLE);
                        changePassBtn.setVisibility(View.GONE);
                        updatedPassword.setText("Password Updated");
//                        username.getText().clear();
                        password.getText().clear();
                        alertDialog.dismiss();
                    } else if (response.body().getResult().equals("Not Success")) {
                        alertbox.showAlert("Please try again later");
                    } else {
                        alertbox.showAlert("Please try again");
                    }
                }

                @Override
                public void onFailure(Call<ChangePassword> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showAlert(getString(R.string.server_error));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            alertbox.showAlert(getString(R.string.internet_error_msg));
        }
    }

    private void login() {
        try {
            final ProgressDialog loading = ProgressDialog.show(LoginActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

            APIService service = RetrofitClient.getApiService();

            Call<GatesLogin> call = service.gatesLogin(
                    Id,
                    UserName,
                    Password,
                    Password,
                    UserType);

            call.enqueue(new Callback<GatesLogin>() {
                @Override
                public void onResponse(Call<GatesLogin> call, Response<GatesLogin> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")) {

                        sharedEditor.putString("userNameGates", UserName);
                        sharedEditor.putString("passwordGates", Password);
                        sharedEditor.putString("mobileNoGates", response.body().getData().getMobileNo());
                        sharedEditor.putString("emailGates", response.body().getData().getEmail());
                        sharedEditor.putLong("id", response.body().getData().getId());
                        sharedEditor.putBoolean("gatesLogin", true);
                        sharedEditor.commit();
                        /*Intent intent = new Intent(LoginActivity.this, DashBoardGatesActivity.class);
                        startActivity(intent);
                        finish();*/
                        /*if (UserType.equalsIgnoreCase("Dealer / Retailer")) {
                            Intent dash = new Intent(getApplicationContext(), ScanActivity.class);
                            startActivity(dash);
                            finish();
                        } else if (UserType.equalsIgnoreCase("Mechanic")) {
                            Intent dash = new Intent(getApplicationContext(), DashboardActivity.class);
                            startActivity(dash);
                            finish();

                        } else*/
                        if (UserType.equalsIgnoreCase("Gates Employee")) {
                            Intent dash = new Intent(getApplicationContext(), DashBoardGatesActivity.class);
                            startActivity(dash);
                            finish();
                        } else {
                            alertbox.showAlertWithBack("Please try again");
                        }

                    } else if (response.body().getResult().equals("Not Success")) {
                        alertbox.showAlert("Invalid Username and Password");
                    } else {
                        alertbox.showAlertWithBack("Please try again");
                    }
                }

                @Override
                public void onFailure(Call<GatesLogin> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showAlertWithBack("Please try again later");
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            alertbox.showAlert(getString(R.string.internet_error_msg));
        }
    }
}