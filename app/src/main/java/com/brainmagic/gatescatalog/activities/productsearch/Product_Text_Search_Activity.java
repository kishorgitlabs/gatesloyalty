package com.brainmagic.gatescatalog.activities.productsearch;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.transition.ChangeBounds;
import androidx.transition.TransitionManager;

import com.bumptech.glide.Glide;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;


public class Product_Text_Search_Activity extends Activity {


    private ImageView menu;
//    private TextView navicationtxt;
    private SQLiteDatabase db;
    private ListView listView;
    private Cursor c;
    private ArrayList<String> make_List, model_List, engine_codeList, model_codeList, year_fromList, year_tillList, month_fromList,
            month_tillList, stokeList, fromyearList, toyearList, partDescriptionList, gates_Part_List, equipment1List, equipment2List,SegmentList;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String Email, Mobno, Usertype, Country, SelectedYearFrom = "", SelectedYearTo = "", vehicle_type, oem_name, vehicle_model;
//    private Product_Text_Grid_Adapter adapter;
//    private Spinner fromyearspinner, toyearspinner;
//    private ImageView moreImage;
    private Alertbox box = new Alertbox(Product_Text_Search_Activity.this);
    private String searchWord;
    private ProgressDialog loading;

    private TextView ProfileName;
//    private ConstraintLayout signout_relativelayout;
    private LinearLayout signout_relativelayout;
    private FloatingActionButton back, home;
    private ViewGroup includeviewLayout;
    private ImageView profilePicture;
//    private ArrayList<Products> productList;
//    private ArrayList<Products> arraylist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_text_search);

        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        menu = (ImageView) findViewById(R.id.menu);

        includeviewLayout = findViewById(R.id.profile_logo);
        ProfileName = (TextView) includeviewLayout.findViewById(R.id.profilename);
        profilePicture = (ImageView) includeviewLayout.findViewById(R.id.profilepicture);
        signout_relativelayout =  findViewById(R.id.signout);
        signout_relativelayout.setVisibility(View.GONE);

        listView = (ListView) findViewById(R.id.listview);


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();

        Email = myshare.getString("email", "");
        Mobno = myshare.getString("MobNo", "");
        Usertype = myshare.getString("Usertype", "");
        Country = myshare.getString("Country", "");
        ProfileName.setText(myshare.getString("name", "")+" , "+Country);
        final ImageView downArrow=includeviewLayout.findViewById(R.id.down_arrow);
        final RelativeLayout userData=includeviewLayout.findViewById(R.id.user_data);


        if (!myshare.getString("profile_path", "").equals(""))
            Glide.with(Product_Text_Search_Activity.this)
                    .load(new File(myshare.getString("profile_path", "")))
                    .into(profilePicture);

        if (myshare.getBoolean("islogin", false))
            signout_relativelayout.setVisibility(View.VISIBLE);
        else
            signout_relativelayout.setVisibility(View.GONE);
        signout_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("islogin", false).commit();
                final Snackbar mySnackbar = Snackbar.make(findViewById(R.id.Product_Text_Search_Activity), "You have successfully signed out !", Snackbar.LENGTH_INDEFINITE);
                mySnackbar.setActionTextColor(getResources().getColor(R.color.red));
                mySnackbar.setAction("Okay", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mySnackbar.dismiss();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);

                    }
                });
                mySnackbar.show();

            }
        });

        downArrow.setOnClickListener(new View.OnClickListener() {
            boolean visible;
            @Override
            public void onClick(View v) {
                ChangeBounds changeBounds=new ChangeBounds();
                changeBounds.setDuration(600L);
                TransitionManager.beginDelayedTransition(includeviewLayout,changeBounds);
                visible = !visible;
                if(visible)
                    downArrow.animate().rotation(180).setInterpolator(new LinearInterpolator()).setDuration(500);
                else
                    downArrow.animate().rotation(0).setInterpolator(new LinearInterpolator()).setDuration(500);
                userData.setVisibility(visible? View.VISIBLE: View.GONE);
            }
        });

        make_List = new ArrayList<String>();
        model_List = new ArrayList<String>();

        engine_codeList = new ArrayList<String>();
        model_codeList = new ArrayList<String>();
        year_fromList = new ArrayList<String>();

        fromyearList = new ArrayList<String>();
        toyearList = new ArrayList<String>();

        year_tillList = new ArrayList<String>();
        month_fromList = new ArrayList<String>();
        month_tillList = new ArrayList<String>();


        partDescriptionList = new ArrayList<String>();
        gates_Part_List = new ArrayList<String>();
        equipment1List = new ArrayList<String>();
        equipment2List = new ArrayList<String>();

        stokeList = new ArrayList<String>();

//        arraylist = new ArrayList<Products>();
//        productList = new ArrayList<Products>();
        SegmentList= new ArrayList<String>();

        searchWord = getIntent().getStringExtra("searchword");

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(Product_Text_Search_Activity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Product_Text_Search_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

//                        switch (item.getItemId()) {
//                            case R.id.homepop:
//                                Intent home = new Intent(Product_Text_Search_Activity.this, HomeScreenActivity.class);
//                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(home);
//                                return true;
//
//                            case R.id.aboutpop:
//                                startActivity(new Intent(Product_Text_Search_Activity.this, About_Gate_Activity.class));
//                                return true;
//                            case R.id.ptoductpop:
//                                startActivity(new Intent(Product_Text_Search_Activity.this, Product_Activity.class));
//                                return true;
//                            case R.id.otherlinkpop:
//                                Intent a = new Intent(Product_Text_Search_Activity.this, Other_Links_Activity.class);
//                                startActivity(a);
//                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Product_Text_Search_Activity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
//                            case R.id.notifipop:
//                                startActivity(new Intent(Product_Text_Search_Activity.this, Notification_Activity.class));
//                                return true;
//                            case R.id.promo_scheem_pop:
//
//                                startActivity(new Intent(Product_Text_Search_Activity.this, Schemes_Offers_Page.class));
//
//                                return true;
//                            case R.id.whatpop:
//                                startActivity(new Intent(Product_Text_Search_Activity.this, WhatNew_SpiltActivity.class));
//                                return true;
//                            case R.id.disnetpop:
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Product_Text_Search_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert("You are not Authorized");
//                                }
//                                return true;
//                            case R.id.contactpop:
//                                startActivity(new Intent(Product_Text_Search_Activity.this, Contact_Details_Activity.class));
//                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Text_Search_Activity.this, EditProfileActivity.class));
//                                return true;
//                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Network_Connection_Activity net=new Network_Connection_Activity(Product_Text_Search_Activity.this);
                if(net.CheckInternet()){
                    sendActivityWithInternet(position);
                }
                else {
                    sendActivity(position);
                }
            }
        });

//        new GetProductDetails().execute();

    }

    private void sendActivityWithInternet(int position){
//        String Partnumber = gates_Part_List.get(position);
//
//        try {
//            loading = ProgressDialog.show(Product_Text_Search_Activity.this, "", getString(R.string.loadings), false, false);
//
//            APIServices service = RetroClient.getApiService();
//            Call<ThumbImage> call = service.MASTER_IMAGE_CALL(Partnumber);
//            call.enqueue(new Callback<ThumbImage>() {
//                @Override
//                public void onResponse(Call<ThumbImage> call, Response<ThumbImage> response) {
//
//                    try{
//                        List<LogDetailResult> imageList=response.body().getData();
//                        loading.dismiss();
//                        Intent a = new Intent(Product_Text_Search_Activity.this, Product_More_Details_Activity.class);
//                        a.putExtra("MODEL_CODE", model_codeList.get(position).toString());
//                        a.putExtra("MODEL", model_List.get(position));
//                        a.putExtra("OEM", make_List.get(position));
//                        a.putExtra("PARTDESC", partDescriptionList.get(position));
//                        a.putExtra("GATESPART", gates_Part_List.get(position));
//                        a.putExtra("ENGINE_CODE", engine_codeList.get(position));
//                        a.putExtra("yearfrom", year_fromList.get(position).toString());
//                        a.putExtra("yearto", year_tillList.get(position).toString());
//                        a.putExtra("EQUIPMENT1", equipment1List.get(position));
//                        a.putExtra("EQUIPMENT2", equipment2List.get(position));
//                        a.putExtra("thumbuimage",(Serializable) imageList);
//                        a.putExtra("internet","Success");
//
//                        if(SegmentList.get(position).equals("Passenger Car") || SegmentList.get(position).equals("Light Commercial"))
//                        {
//                            a.putExtra("navtxt", "PC & LCV  > " + make_List.get(position) + "  >  " + model_List.get(position)+ "  >  " + model_codeList.get(position));
//                        }
//                        else if(SegmentList.get(position).equals("Heavy Commercial"))
//                        {
//                            a.putExtra("navtxt", "  HCV  >  " + make_List.get(position) + "  >  " + model_List.get(position)+ "  >  " + model_codeList.get(position));
//                        }
//                        else
//                        {
//                            a.putExtra("navtxt", SegmentList.get(position)+"  >  " + make_List.get(position) + "  >  " + model_List.get(position)+ "  >  " + model_codeList.get(position));
//                        }
//                        startActivity(a);
//                    }
//                    catch (Exception e)
//                    {
//                        sendActivity(position);
//                    }
//
//                }
//
//                @Override
//                public void onFailure(Call<ThumbImage> call, Throwable t) {
//
//                    sendActivity(position);
//                }
//            });
//        }catch (Exception e)
//        {
//            e.printStackTrace();
//        }
    }

    private void sendActivity(int position){
        Intent a = new Intent(Product_Text_Search_Activity.this, Product_More_Details_Activity.class);
        a.putExtra("MODEL_CODE", model_codeList.get(position).toString());
        a.putExtra("MODEL", model_List.get(position));
        a.putExtra("OEM", make_List.get(position));
        a.putExtra("PARTDESC", partDescriptionList.get(position));
        a.putExtra("GATESPART", gates_Part_List.get(position));
        a.putExtra("ENGINE_CODE", engine_codeList.get(position));
        a.putExtra("yearfrom", year_fromList.get(position).toString());
        a.putExtra("yearto", year_tillList.get(position).toString());
        a.putExtra("EQUIPMENT1", equipment1List.get(position));
        a.putExtra("EQUIPMENT2", equipment2List.get(position));
        a.putExtra("thumbuimage",(Serializable) null);
        a.putExtra("internet","Nointernet");

        if(SegmentList.get(position).equals("Passenger Car") || SegmentList.get(position).equals("Light Commercial"))
        {
            a.putExtra("navtxt", "PC & LCV  > " + make_List.get(position) + "  >  " + model_List.get(position)+ "  >  " + model_codeList.get(position));
        }
        else if(SegmentList.get(position).equals("Heavy Commercial"))
        {
            a.putExtra("navtxt", "  HCV  >  " + make_List.get(position) + "  >  " + model_List.get(position)+ "  >  " + model_codeList.get(position));
        }
        else
        {
            a.putExtra("navtxt", SegmentList.get(position)+"  >  " + make_List.get(position) + "  >  " + model_List.get(position)+ "  >  " + model_codeList.get(position));
        }
        startActivity(a);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (myshare.getBoolean("islogin", false))
            signout_relativelayout.setVisibility(View.VISIBLE);
        else
            signout_relativelayout.setVisibility(View.GONE);


    }


//    class GetProducts extends AsyncTask<String, Void, String> {
//        @Override
//        protected String doInBackground(String... params) {
//
//            SQLiteHelper dbHelper = new SQLiteHelper(Product_Text_Search_Activity.this);
//            db = dbHelper.getReadableDatabase();
//            productList = new ArrayList<Products>();
//
//            String query = "SELECT distinct * FROM product ";
//            c = db.rawQuery(query, null);
//            if (c.moveToFirst()) {
//                do {
//                    Products products = new Products();
//
//                    products.setMake(c.getString(c.getColumnIndex("Make")));
//                    products.setModel(c.getString(c.getColumnIndex("Model")));
//                    products.setEnginecode(c.getString(c.getColumnIndex("Enginecode")));
//                    products.setModelcode(c.getString(c.getColumnIndex("Modelcode")));
//                    products.setPart_Description(c.getString(c.getColumnIndex("Part_Description")));
//                    products.setGates_Part_Number(c.getString(c.getColumnIndex("Gates_Part_Number")));
//                    productList.add(products);
//                    arraylist.add(products);
//                }
//                while (c.moveToNext());
//            }
//
//
//            return "";
//        }
//    }


//    class GetProductDetails extends AsyncTask<String, Void, String> {
//
//        ArrayList<String> make_, model_, engine_code, model_code, year_from, year_till, month_from,
//                month_till, stoke;
//
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//         //   loading = ProgressDialog.show(Product_Text_Search_Activity.this, "Fetching RegistrationData", "Please wait...", false, true);
//
//            make_ = new ArrayList<String>();
//            model_ = new ArrayList<String>();
//            engine_code = new ArrayList<String>();
//            model_code = new ArrayList<String>();
//            year_from = new ArrayList<String>();
//            year_till = new ArrayList<String>();
//            month_from = new ArrayList<String>();
//            month_till = new ArrayList<String>();
//            stoke = new ArrayList<String>();
//
//            if (adapter != null)
//                adapter.clear();
//
//        }
//
//        @Override
//        protected String doInBackground(String... str) {
//
//            SQLiteHelper dbHelper = new SQLiteHelper(Product_Text_Search_Activity.this);
//            db = dbHelper.getReadableDatabase();
//
//            if (searchWord.contains("+")) {
//
//                make_List.clear();
//                model_List.clear();
//                engine_codeList.clear();
//                model_codeList.clear();
//                year_fromList.clear();
//                year_tillList.clear();
//                month_fromList.clear();
//                month_tillList.clear();
//                stokeList.clear();
//
//
//                List<String> searchWordList = Arrays.asList(searchWord.split("\\+"));
//                for (int i = 1, j=0; i <= searchWordList.size(); i++,j++) {
//
//                    if(i==1)
//                    {
//                        String query = " CREATE TEMP TABLE TextSearchFirst AS select distinct Make,Model,Segment,Enginecode,Modelcode,Part_Description,Gates_Part_Number,Year_From,Year_Till,Equipment,Equipment_2 from product where   " +
//                                "Model like '%" + searchWordList.get(0).trim() + "%' or Make like '%" + searchWordList.get(0).trim() + "%'  or Modelcode like '%" + searchWordList.get(0).trim() + "%' or Enginecode like '%" + searchWordList.get(0).trim() + "%' or Gates_Part_Number like '%" + searchWordList.get(0).trim() + "%'   ";
//                        db.execSQL(query);
//                        String query2 = " CREATE TEMP TABLE TextSearchSecond AS select distinct Make,Model,Segment,Enginecode,Modelcode,Part_Description,Gates_Part_Number,Year_From,Year_Till,Equipment,Equipment_2 from product where   " +
//                                "Model like '%" + searchWordList.get(0).trim() + "%' or Make like '%" + searchWordList.get(0).trim() + "%'  or Modelcode like '%" + searchWordList.get(0).trim() + "%' or Enginecode like '%" + searchWordList.get(0).trim() + "%' or Gates_Part_Number like '%" + searchWordList.get(0).trim() + "%'   ";
//                        db.execSQL(query2);
//
//                    }
//                     if(i!= searchWordList.size())
//                    {
//                        db.execSQL("DROP TABLE IF EXISTS TextSearchSecond");
//                        String query = " CREATE TEMP TABLE TextSearchSecond AS  select distinct Make,Model,Segment,Enginecode,Modelcode,Part_Description,Gates_Part_Number,Year_From,Year_Till,Equipment,Equipment_2 from TextSearchFirst where   " +
//                                "Model like '%" + searchWordList.get(j).trim() + "%' or Make like '%" + searchWordList.get(j).trim() + "%'  or Modelcode like '%" + searchWordList.get(j).trim() + "%' or Enginecode like '%" + searchWordList.get(j).trim() + "%' or Gates_Part_Number like '%" + searchWordList.get(j).trim() + "%'   ";
//                        db.execSQL(query);
//
//                        db.execSQL("DROP TABLE IF EXISTS TextSearchFirst");
//                        String query2 = " CREATE TEMP TABLE  TextSearchFirst AS select distinct Make,Model,Segment,Enginecode,Modelcode,Part_Description,Gates_Part_Number,Year_From,Year_Till,Equipment,Equipment_2 from TextSearchSecond where   " +
//                                "Model like '%" + searchWordList.get(j).trim() + "%' or Make like '%" + searchWordList.get(j).trim() + "%'  or Modelcode like '%" + searchWordList.get(j).trim() + "%' or Enginecode like '%" + searchWordList.get(j).trim() + "%' or Gates_Part_Number like '%" + searchWordList.get(j).trim() + "%'   ";
//                        db.execSQL(query2);
//
//                    }
//                    else
//                    {
//                        String query = "select distinct Make,Model,Segment,Enginecode,Modelcode,Part_Description,Gates_Part_Number,Year_From,Year_Till,Equipment,Equipment_2,Segment from TextSearchFirst where   " +
//                                "Model like '%" + searchWordList.get(j).trim() + "%' or Make like '%" + searchWordList.get(j).trim() + "%'  or Modelcode like '%" + searchWordList.get(j).trim() + "%' or Enginecode like '%" + searchWordList.get(j).trim() + "%' or Gates_Part_Number like '%" + searchWordList.get(j).trim() + "%'   ";
//
//                        Log.v("Grid  query =====", query);
//                        c = db.rawQuery(query, null);
//                        if (c.moveToFirst()) {
//                            do {
//
//                                make_List.add(c.getString(c.getColumnIndex("Make")));
//                                model_List.add(c.getString(c.getColumnIndex("Model")));
//                                engine_codeList.add(c.getString(c.getColumnIndex("Enginecode")));
//                                model_codeList.add(c.getString(c.getColumnIndex("Modelcode")));
//                                partDescriptionList.add(c.getString(c.getColumnIndex("Part_Description")));
//                                gates_Part_List.add(c.getString(c.getColumnIndex("Gates_Part_Number")));
//                                year_fromList.add(c.getString(c.getColumnIndex("Year_From")));
//                                year_tillList.add(c.getString(c.getColumnIndex("Year_Till")));
//                                equipment1List.add(c.getString(c.getColumnIndex("Equipment")));
//                                equipment2List.add(c.getString(c.getColumnIndex("Equipment_2")));
//                                SegmentList.add(c.getString(c.getColumnIndex("Segment")));
//
//                            }
//                            while (c.moveToNext());
//                        }
//                    }
//
//                }
//
//            } else {
//                List<String> searchWordList = Arrays.asList(searchWord.split(","));
//
//                for (int j = 0; j < searchWordList.size(); j++) {
//                    String querywithcomma = "select distinct Make,Model,Modelcode,Segment,Enginecode,Year_From,Year_Till,Month_From,Month_Till,Stroke,\n" +
//                            "ProductAdditionalInfo,Equipment,Equipment_2,Equipment_Date_From,Equipment_Date_To,Part_Description,Part_Description1,Gates_Part_Number from product where   Model like '%" + searchWordList.get(j).trim() + "%' or Make like '%" + searchWordList.get(j).trim() + "%'  or Modelcode like '%" + searchWordList.get(j).trim() + "%' or Enginecode like '%" + searchWordList.get(j).trim() + "%' or Gates_Part_Number like '%" + searchWordList.get(j).trim() + "%'   ";
//                    Log.v("Grid  query =====", querywithcomma);
//                    c = db.rawQuery(querywithcomma, null);
//                    if (c.moveToFirst()) {
//                        do {
//
//                            make_List.add(c.getString(c.getColumnIndex("Make")));
//                            model_List.add(c.getString(c.getColumnIndex("Model")));
//
//                            engine_codeList.add(c.getString(c.getColumnIndex("Enginecode")));
//                            model_codeList.add(c.getString(c.getColumnIndex("Modelcode")));
//                            year_fromList.add(c.getString(c.getColumnIndex("Year_From")));
//                            year_tillList.add(c.getString(c.getColumnIndex("Year_Till")));
//                            month_fromList.add(c.getString(c.getColumnIndex("Month_From")));
//                            month_tillList.add(c.getString(c.getColumnIndex("Month_Till")));
//                            stokeList.add(c.getString(c.getColumnIndex("Stroke")));
//
//                            partDescriptionList.add(c.getString(c.getColumnIndex("Part_Description")));
//                            gates_Part_List.add(c.getString(c.getColumnIndex("Gates_Part_Number")));
//                            equipment1List.add(c.getString(c.getColumnIndex("Equipment")));
//                            equipment2List.add(c.getString(c.getColumnIndex("Equipment_2")));
//                            SegmentList.add(c.getString(c.getColumnIndex("Segment")));
//
//                        }
//                        while (c.moveToNext());
//
//                    }
//                }
//            }
//            if (make_List.size() == 0) {
//                c.close();
//                db.close();
//                return "nodate";
//            } else {
//                c.close();
//                db.close();
//                return "sucess";
//            }
//
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//           // loading.dismiss();
//            if (s.equals("sucess")) {
//                adapter = new Product_Text_Grid_Adapter(Product_Text_Search_Activity.this, make_List, model_List, engine_codeList, model_codeList,
//                        partDescriptionList, gates_Part_List);
//                adapter.notifyDataSetChanged();
//                listView.setAdapter(adapter);
//            } else {
//                box.showAlertWithBack(getString(R.string.no_data_found));
//            }
//        }
//    }

    // Filter Class
//    public void filter(String charText) {
//        //charText = charText.toLowerCase(Locale.getDefault());
//
//        productList.clear();
//        if (charText.equals("")) {
//            productList.addAll(productList);
//
//            for (int j = 0; j < productList.size(); j++) {
//
//                make_List.add(productList.get(j).getMake());
//                model_List.add(productList.get(j).getModel());
//
//                engine_codeList.add(productList.get(j).getEnginecode());
//                model_codeList.add(productList.get(j).getModelcode());
//
//                partDescriptionList.add(productList.get(j).getPart_Description());
//                gates_Part_List.add(productList.get(j).getGates_Part_Number());
//            }
//        }
//        else
//        {
//            for (Products wp : arraylist)
//            {
//                if (wp.getMake().contains(charText))
//                {
//                    productList.add(wp);
//                }
//                if(wp.getModel().contains(charText))
//                {
//                    productList.add(wp);
//                }
//                if(wp.getGates_Part_Number().contains(charText))
//                {
//                    productList.add(wp);
//                }
//                if(wp.getPart_Description().contains(charText))
//                {
//                    productList.add(wp);
//                }
//                if(wp.getEnginecode().contains(charText))
//                {
//                    productList.add(wp);
//                }
//                if(wp.getModelcode().contains(charText))
//                {
//                    productList.add(wp);
//                }
//            }
//
//            arraylist.clear();
//            arraylist.addAll(productList);
//
//            make_List.clear();
//            model_List.clear();
//            engine_codeList.clear();
//            model_codeList.clear();
//
//            partDescriptionList.clear();
//            gates_Part_List.clear();
//
//            for (int j = 0; j < productList.size(); j++) {
//
//                make_List.add(productList.get(j).getMake());
//                model_List.add(productList.get(j).getModel());
//
//                engine_codeList.add(productList.get(j).getEnginecode());
//                model_codeList.add(productList.get(j).getModelcode());
//
//                partDescriptionList.add(productList.get(j).getPart_Description());
//                gates_Part_List.add(productList.get(j).getGates_Part_Number());
//            }
//
//
//        }
//
//    }


}
