package com.brainmagic.gatescatalog.activities.loyalty;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.gates.addmechanic.AddMechanic;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMechanicActivity extends AppCompatActivity {

    private EditText mechanicName, mechanicEmail, mechanicGst, mechanicMobileNo, mechanicShopName;
    private Spinner country, state, city;
    private Button addMechanic;

    private String MechanicName, MechanicEmail, MechanicGst, MechanicMobileNo, MechanicShopName, Country, State, City;
    private String[] countryList = {"India", "India"};
    private String[] stateList = {"SELECT STATE", "Tamil Nadu", "Karnataka", "Telungana"};
    private String[] cityList = {"Chennai", "Bangalore", "Hyderabad"};
    private String[] tamilNaduCity = {"Chennai", "Madurai"};
    private String[] karnatakaCity = {"Bangalore"};
    private String[] telunganaCity = {"Hyderabad"};
    ArrayAdapter<String> cityAdapter;

    private FloatingActionButton back, home;

    private Alertbox alertbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mechanic);
        mechanicName = findViewById(R.id.name_add_mechanic);
        mechanicEmail = findViewById(R.id.email_add_mechanic);
        mechanicGst = findViewById(R.id.gst_no_add_mechanic);
        mechanicMobileNo = findViewById(R.id.mobileno_add_mechanic);
        mechanicShopName = findViewById(R.id.shop_name_add_mechanic);
        country = findViewById(R.id.country_add_mechanic);
        city = findViewById(R.id.city_add_mechanic);
        state = findViewById(R.id.state_add_mechanic);
        addMechanic = findViewById(R.id.add_mechanic);
        back = findViewById(R.id.back);
        home = findViewById(R.id.home);
        country.setEnabled(false);
        alertbox = new Alertbox(AddMechanicActivity.this);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent home = new Intent(AddMechanicActivity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });


        ArrayAdapter<String> countryAdapter = new ArrayAdapter<>(AddMechanicActivity.this, android.R.layout.simple_list_item_1, countryList);
        country.setAdapter(countryAdapter);

        ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(AddMechanicActivity.this, android.R.layout.simple_list_item_1, stateList);
        state.setAdapter(stateAdapter);


        state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String item = adapterView.getItemAtPosition(i).toString();
                if (item.equals("Tamil Nadu")) {
                    cityAdapter = new ArrayAdapter<>(AddMechanicActivity.this, android.R.layout.simple_list_item_1, tamilNaduCity);
                } else if (item.equals("Karnataka")) {
                    cityAdapter = new ArrayAdapter
                            <>(AddMechanicActivity.this, android.R.layout.simple_list_item_1, karnatakaCity);
                } else if (item.equals("Telungana")) {
                    cityAdapter = new ArrayAdapter<>(AddMechanicActivity.this, android.R.layout.simple_list_item_1, telunganaCity);
                }

                city.setAdapter(cityAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        addMechanic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    MechanicName = mechanicName.getText().toString();
                    MechanicEmail = mechanicEmail.getText().toString();
                    MechanicMobileNo = mechanicMobileNo.getText().toString();
                    MechanicGst = mechanicGst.getText().toString();
                    MechanicShopName = mechanicShopName.getText().toString();
                    Country = country.getSelectedItem().toString();
                    State = state.getSelectedItem().toString();
//                    City = city.getSelectedItem().toString();


                    if (MechanicName.equals("")) {
                        mechanicName.setError("Enter the Retailer Name");
                    } else if (MechanicEmail.equals("")) {
                        mechanicEmail.setError("Enter the Retailer Email");
                    } else if (MechanicMobileNo.equals("")) {
                        mechanicMobileNo.setError("Enter the Mobile Number");
                    } else if (MechanicGst.equals("")) {
                        mechanicGst.setError("Enter the Shop GST Number");
                    } else if (MechanicShopName.equals("")) {
                        mechanicShopName.setError("Enter the Retailer Shop Name");
                    } else if (State.equals("")) {
                        Toast.makeText(AddMechanicActivity.this, "Select State Name", Toast.LENGTH_LONG).show();
                    } else if (city.getSelectedItem().toString().equals("")
                            || city.getSelectedItem().toString() == null) {
                        Toast.makeText(AddMechanicActivity.this, "Select City Name", Toast.LENGTH_SHORT).show();
                    } else {
                        addmechanic();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void addmechanic() {
        try {
            final ProgressDialog loading = ProgressDialog.show(AddMechanicActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

            APIService service = RetrofitClient.getApiService();

            Call<AddMechanic> call = service.gatesAddMechanic(
                    MechanicName,
                    MechanicMobileNo,
                    MechanicEmail,
                    MechanicShopName,
                    MechanicGst,
                    Country,
                    State,
                    City
            );

            call.enqueue(new Callback<AddMechanic>() {
                @Override
                public void onResponse(Call<AddMechanic> call, Response<AddMechanic> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        alertbox.showAlertWithBack("Successfully Created");
                    } else if (response.body().getResult().equals("Already Exist")) {
                        alertbox.showAlertWithBack("User already added");
                    } else {
                        alertbox.showAlertWithBack("Please try again");
                    }
                }

                @Override
                public void onFailure(Call<AddMechanic> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showAlertWithBack("Please try again");
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
            alertbox.showAlertWithBack("Please try again");
        }
    }
}