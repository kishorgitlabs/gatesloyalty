package com.brainmagic.gatescatalog.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.brainmagic.gatescatalog.activities.loyalty.DashBoardGatesActivity;
import com.brainmagic.gatescatalog.activities.loyalty.DashboardActivity;
import com.brainmagic.gatescatalog.activities.loyalty.LoginActivity;
import com.brainmagic.gatescatalog.activities.loyalty.ViewProfileActivity;
import com.brainmagic.gatescatalog.api.models.changepassword.ChangePassword;
import com.brainmagic.gatescatalog.api.models.loyaltyapprove.LoyaltyApprove;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.checkregister.CheckRegister;
import com.brainmagic.gatescatalog.api.models.scanverify.gatesemployeeapproval.Approval;
import com.brainmagic.gatescatalog.api.models.scanverify.olduserverify.OldUserVerify;
import com.brainmagic.gatescatalog.api.models.scanverify.verifieddata.VerifiedData;
import com.brainmagic.gatescatalog.api.models.sendotp.SendOTP;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.facebook.appevents.AppEventsLogger;
import com.goodiebag.pinview.Pinview;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeScreenActivity extends Activity {
    private RelativeLayout productSearchLayout, scanLayout, whatsNewLayout, notificationLayout, loyaltyLayout;
    private TextView scanText, updatedPassword;
    private Button changePassBtn;
    private ImageView menu, close;
    AlertDialog alertDialog;
    private EditText newPassword;
    private Menu menus;
    private static final String TAG = "HomeScreenActivity";
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private boolean permissionGranted = false, otpgenerated = false;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private SettingsClient mSettingsClient;
    private Boolean mRequestingLocationUpdates;
    private FusedLocationProviderClient mFusedLocationClient;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 50000;
    private static final int REQUEST_CHECK_SETTINGS = 100;
    private static final int REQUEST_CHECK_SETTINGS_GPS = 100;
    private String locationaddress;
    String Email, Mobno, Usertype, Name, Bussinessname, longitude, latitude;
    String businessname, usertype, address, email, name, usertypereg, enteredotp;
    private List<String> otp = new ArrayList<>();

    private Alertbox alertbox;
    boolean gatesLogin, otherLogin;
    private String UserName;
    private long Id;
    private ImageView scanIcon;

    private Network_Connection_Activity networkConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TO show alert only once if the app update is available when the app is opened
        setContentView(R.layout.activity_home_screen);


        productSearchLayout = findViewById(R.id.product_search_relativelayout);
        whatsNewLayout = findViewById(R.id.whats_new_relativelayout);
        scanLayout = findViewById(R.id.scan_layout);
        loyaltyLayout = findViewById(R.id.loyalty_icon);
        scanText = findViewById(R.id.videos_textView);
        scanIcon = findViewById(R.id.videos_imageView);
        notificationLayout = findViewById(R.id.notification_relativelayout);
        menu = findViewById(R.id.menu);
        alertbox = new Alertbox(HomeScreenActivity.this);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        myshare.getString("StreetName", "");
        myshare.getString("City", "");
        myshare.getString("State", "");
        myshare.getString("Country", "");
        myshare.getString("ZipCode", "");
        myshare.getString("Usertype", "");
        editor = myshare.edit();
        int logout = R.id.gates_employee_logout;
        networkConnection = new Network_Connection_Activity(HomeScreenActivity.this);

        gatesLogin = myshare.getBoolean("gatesLogin", false);
        otherLogin = myshare.getBoolean("otherLogin", false);
//        UserName = myshare.getString("userNameGates", "");
//        Id = myshare.getLong("id", 0);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.brainmagic.gatescatalog.gates",
                    PackageManager.GET_SIGNATURES);
            Log.d("created", "afterpackage");
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.wtf("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("KeyHash", e.getMessage());

        } catch (NoSuchAlgorithmException e) {
            Log.d(TAG, "KeyHash: ", e);
        }

        productSearchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeScreenActivity.this, Product_Activity.class));
            }
        });

        if (myshare.getString("Usertype", "").equals("Mechanic")) {
            scanIcon.setImageResource(R.drawable.mechanic_icon);
            scanText.setText("Scan Coupon Code");
        } else if (myshare.getString("Usertype", "").equals("Dealer / Retailer")) {
            scanText.setText("Scan MRP Label ");
//            scanIcon.setImageResource(R.drawable.mrp_label_scan);
            scanIcon.setImageResource(R.drawable.play_button);
        } else if (myshare.getString("Usertype", "").equals("Gates Employee")) {
//            scanText.setText("Loyalty Managemnet");
            scanText.setText("Login");
//            if (gatesLogin){
//                scanText.setText("Loyalty Managemnet");
//            }else {
//                scanText.setText("Login");
//            }
        } else {
            scanText.setText("Scan");
        }

        loyaltyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (myshare.getString("Usertype","").equals("Mechanic")){
//                    alertbox.showAlert("You are not Authorized");
//                }else {
                verifiedloyalty();
//                }
            }
        });

        initPermission();

        scanLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                init();
//                startLocationUpdate();
//                if (myshare.getString("Usertype", "").equals("Mechanic")) {
//                if (myshare.getString("Usertype", "").equals("Gates Employee")) {
                /*if (myshare.getString("Usertype", "").equals("Dealer / Retailer") ||
                myshare.getString("Usertype","").equals("Gates Employee")) {
                    approval();
                } else {
//                    UserVerified();
                    alertbox.showAlert("You are not Authorized");
                }*/
                /*if (myshare.getString("Usertype", "").equals("Mechanic")) {
//                    alertbox.showAlert("You are not Authorized");
                    verifiedloyalty();
                } else {
                    verifiedloyalty();
                }*/
                verifiedloyalty();
            }
        });


        whatsNewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeScreenActivity.this, WhatsNew_ApplicationActivity.class));
            }
        });

        notificationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeScreenActivity.this, Notification_Activity.class));
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(HomeScreenActivity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub
                        switch (item.getItemId()) {
                            case R.id.homepop:
//                                Intent home = new Intent(HomeScreenActivity.this, HomeScreenActivity.class);
//                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(HomeScreenActivity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(HomeScreenActivity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(HomeScreenActivity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(HomeScreenActivity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.scan:
                                startActivity(new Intent(HomeScreenActivity.this, ScanActivity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(HomeScreenActivity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(HomeScreenActivity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(HomeScreenActivity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(HomeScreenActivity.this, Distributor_Search_Activity.class));
                                return true;
                            case R.id.viewprofile:
                                startActivity(new Intent(HomeScreenActivity.this, ViewProfileActivity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(HomeScreenActivity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(HomeScreenActivity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;
                            case R.id.gates_employee_logout:
                                /*MenuItem menuItem = menus.findItem(R.id.gates_employee_logout);
                                menuItem.setVisible(true);*/
                                editor.clear();
                                editor.commit();
                                startActivity(new Intent(HomeScreenActivity.this, RegisterActivity.class));
                                finish();
//                                alertbox.showAlert("Successful Logout");
                                return true;

                            case R.id.change_password:
                                alertDialog = new AlertDialog.Builder(
                                        HomeScreenActivity.this).create();

                                LayoutInflater inflater = ((Activity) HomeScreenActivity.this).getLayoutInflater();
                                View dialogView = inflater.inflate(R.layout.change_password_layout, null);
                                alertDialog.setView(dialogView);
                                close = dialogView.findViewById(R.id.close);
                                newPassword = dialogView.findViewById(R.id.new_password);
                                updatedPassword = dialogView.findViewById(R.id.updated_password);

                                changePassBtn = dialogView.findViewById(R.id.change_password_btn);
                                close.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        alertDialog.dismiss();
                                    }
                                });

                                changePassBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        String NewPassword = newPassword.getText().toString();

                                        if (NewPassword.equals("")) {
                                            newPassword.setError("Enter new Password");
                                        } else {
                                            newPassChange(NewPassword);
                                        }
                                    }
                                });

                                Window window = alertDialog.getWindow();
                                window.setLayout(600, 600);
                                alertDialog.show();
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.pop_menu_home);
                popupMenu.show();
            }
        });

        initPermission();

    }

    private void checkInternet() {
    }

    private void newPassChange(String NewPass) {
        try {

            final ProgressDialog loading = ProgressDialog.show(HomeScreenActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

            APIService service = RetrofitClient.getApiService();

            Call<ChangePassword> call = service.changePassword(
                    myshare.getString("id", ""),
                    myshare.getString("MobNo", ""),
                    NewPass
            );

            call.enqueue(new Callback<ChangePassword>() {
                @Override
                public void onResponse(Call<ChangePassword> call, Response<ChangePassword> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        /*sharedEditor.putString("UserName", UserName);
                        sharedEditor.putString("Password", Password);
                        sharedEditor.putBoolean("gatesLogin", true);
                        sharedEditor.commit();
                        if (UserType.equalsIgnoreCase("Dealer / Retailer")) {
                            Intent dash = new Intent(getApplicationContext(), ScanActivity.class);
                            startActivity(dash);
                            finish();
                        } else if (UserType.equalsIgnoreCase("Mechanic")) {
                            Intent dash = new Intent(getApplicationContext(), DashboardActivity.class);
                            startActivity(dash);
                            finish();

                        } else {
                            alertbox.showAlertWithBack("Please try again");
                        }*/
//                        alertbox.showAlert("New Password Updated");
//                        alertDialog.dismiss();
                        newPassword.setVisibility(View.GONE);
                        updatedPassword.setVisibility(View.VISIBLE);
                        changePassBtn.setVisibility(View.GONE);
                        updatedPassword.setText("Password Updated");
//                        username.getText().clear();
//                        password.getText().clear();
                    } else if (response.body().getResult().equals("Not Success")) {
                        alertbox.showAlert("Please try again later");
                    } else {
                        alertbox.showAlert("Please try again");
                    }
                }

                @Override
                public void onFailure(Call<ChangePassword> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showAlert(getString(R.string.server_error));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            alertbox.showAlert(getString(R.string.internet_error_msg));
        }
    }

    private void verifiedloyalty() {
        try {

            final ProgressDialog loading = ProgressDialog.show(HomeScreenActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

            APIService service = RetrofitClient.getApiService();

            Call<LoyaltyApprove> call = service.loyaltyApprove(myshare.getString("MobNo", ""), myshare.getString("Usertype", ""));

            call.enqueue(new Callback<LoyaltyApprove>() {
                @Override
                public void onResponse(Call<LoyaltyApprove> call, Response<LoyaltyApprove> response) {
                    loading.dismiss();
                    if (response.body().getResult().equals("Verified")) {

                        if (response.body().getData().getApproveStatus().equals("Approved")) {
                            if (gatesLogin) {
//                                scanText.setText("Loyalty Management");
                                if (response.body().getData().getUsertype().equalsIgnoreCase("Dealer / Retailer")) {
                                    Intent dash = new Intent(getApplicationContext(), ScanActivity.class);
                                    dash.putExtra("location", locationaddress);
                                    dash.putExtra("latitude", mCurrentLocation.getLatitude());
                                    dash.putExtra("longitude", mCurrentLocation.getLongitude());
                                    startActivity(dash);
//                                    finish();
                                } else if (response.body().getData().getUsertype().equalsIgnoreCase("Mechanic")) {
                                    Intent dash = new Intent(getApplicationContext(), DashboardActivity.class);
                                    dash.putExtra("location", locationaddress);
                                    dash.putExtra("latitude", mCurrentLocation.getLatitude());
                                    dash.putExtra("longitude", mCurrentLocation.getLongitude());
                                    startActivity(dash);
//                                    finish();
                                } else if (response.body().getData().getUsertype().equalsIgnoreCase("Gates Employee")) {
                                    Intent dash = new Intent(getApplicationContext(), DashBoardGatesActivity.class);
                                    dash.putExtra("location", locationaddress);
                                    dash.putExtra("latitude", mCurrentLocation.getLatitude());
                                    dash.putExtra("longitude", mCurrentLocation.getLongitude());
                                    startActivity(dash);
//                                    finish();
                                } /*else if (response.body().getData().getUsertype().equalsIgnoreCase("Others")) {
                                    alertbox.showAlert("You are not Authorized");
                                }*/ else {
                                    alertbox.showAlert("Please try again later");
                                }
                            } else {
//                                Intent dash = new Intent(getApplicationContext(), DashboardActivity.class);
//                                dash.putExtra("location", locationaddress);
//                                dash.putExtra("latitude", mCurrentLocation.getLatitude());
//                                dash.putExtra("longitude", mCurrentLocation.getLongitude());
//                                startActivity(dash);

                                /*Intent dash = new Intent(getApplicationContext(), LoginActivity.class);
                                dash.putExtra("location", locationaddress);
                                dash.putExtra("latitude", mCurrentLocation.getLatitude());
                                dash.putExtra("longitude", mCurrentLocation.getLongitude());
                                startActivity(dash);*/

//                                scanText.setText("Login");

                                if (response.body().getData().getUsertype().equalsIgnoreCase("Others")) {
                                    alertbox.showAlert("You are not Authorized");
                                } else {
                                    Intent dash = new Intent(getApplicationContext(), LoginActivity.class);
                                    dash.putExtra("location", locationaddress);
                                    dash.putExtra("latitude", mCurrentLocation.getLatitude());
                                    dash.putExtra("longitude", mCurrentLocation.getLongitude());
                                    startActivity(dash);
//                                    finish();
                                }
                            }
                            /*if (response.body().getData().getUsertype().equals("Dealer / Retailer")) {
                                Intent dash = new Intent(getApplicationContext(), ScanActivity.class);
                                dash.putExtra("location", locationaddress);
                                dash.putExtra("latitude", mCurrentLocation.getLatitude());
                                dash.putExtra("longitude", mCurrentLocation.getLongitude());
                                startActivity(dash);

                            } else if (response.body().getData().getUsertype().equals("Mechanic")) {

                                Intent dash = new Intent(getApplicationContext(), DashboardActivity.class);
                                dash.putExtra("location", locationaddress);
                                dash.putExtra("latitude", mCurrentLocation.getLatitude());
                                dash.putExtra("longitude", mCurrentLocation.getLongitude());
                                startActivity(dash);

                            } else if (response.body().getData().getUsertype().equals("Gates Employee")) {
                                if (gatesLogin) {
//                                    scanText.setText("Loyalty Management");
                                    Intent dash = new Intent(getApplicationContext(), DashBoardGatesActivity.class);
                                    dash.putExtra("location", locationaddress);
                                    dash.putExtra("latitude", mCurrentLocation.getLatitude());
                                    dash.putExtra("longitude", mCurrentLocation.getLongitude());
                                    startActivity(dash);
//                                    finish();
                                } else {
                                    Intent dash = new Intent(getApplicationContext(), DashboardActivity.class);
                                    dash.putExtra("location", locationaddress);
                                    dash.putExtra("latitude", mCurrentLocation.getLatitude());
                                    dash.putExtra("longitude", mCurrentLocation.getLongitude());
                                    startActivity(dash);
                                    finish();
                                }
//                        } else if (response.body().getData().getUsertype().equals("Gates Employee")) {
//                            if (gatesLogin) {
//                                Intent dash = new Intent(getApplicationContext(), DashBoardGatesActivity.class);
//                                dash.putExtra("location", locationaddress);
//                                dash.putExtra("latitude", mCurrentLocation.getLatitude());
//                                dash.putExtra("longitude", mCurrentLocation.getLongitude());
//                                startActivity(dash);
////                                    finish();
                            } else if (response.body().getData().getUsertype().equals("Others")) {
                                alertbox.showAlert("You are not Authorized");
                            }*/
//                            else {
//                                alertbox.showAlert("Please try again later");
//                            }
//                        } else if (response.body().getData().getUsertype().equals("Others")) {
//                            alertbox.showAlert("You are not Authorized");
//                        }
                        } else if (response.body().getData().getApproveStatus().equals("Pending")) {
                            alertbox.showAlert("Please Wait For Admin Approval");
                        } else {
                            alertbox.showAlert("Please try again later");
                        }
                    }
                }

                @Override
                public void onFailure(Call<LoyaltyApprove> call, Throwable t) {
                    loading.dismiss();
                    alertbox.showAlert(getString(R.string.server_error));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            alertbox.showAlert(getString(R.string.internet_error_msg));
        }
    }

    public void logScanEvent(String scanCode, double valToSum) {
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        Bundle params = new Bundle();
        params.putString("ScanCode", scanCode);
        logger.logEvent("Scan", valToSum, params);
    }


    private void approval() {
        try {
            final ProgressDialog loading = ProgressDialog.show(HomeScreenActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<Approval> call = service.approval(myshare.getString("MobNo", ""));
            call.enqueue(new Callback<Approval>() {
                @Override
                public void onResponse(Call<Approval> call, Response<Approval> response) {
                    try {
                        if (response.body().getResult().equals("Success")) {
                            loading.dismiss();
                            UserVerified();
                        } else if (response.body().getResult().equals("NotSuccess")) {
                            loading.dismiss();
                            Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                            alertbox.showAlert("Please Wait For Admin Approval");
                        } else {
                            loading.dismiss();
                            Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                            alertbox.showAlert("Failed to fetch data from the Server. Please try again later");
                        }

                    } catch (Exception e) {
                        loading.dismiss();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Approval> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                    alertbox.showAlert("Server is not Responding. Please contact Admin");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void UserVerified() {
        try {
            final ProgressDialog loading = ProgressDialog.show(HomeScreenActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();

            Call<VerifiedData> call = service.verified(myshare.getString("MobNo", ""), myshare.getString("Usertype", ""));
            call.enqueue(new Callback<VerifiedData>() {
                @Override
                public void onResponse(Call<VerifiedData> call, Response<VerifiedData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Verified")) {
                            if (myshare.getString("Usertype", "").equals("Mechanic") || myshare.getString("Usertype", "").equals("Dealer / Retailer")
                                    || myshare.getString("Usertype", "").equals("Gates Employee")) {
                                Intent scan = new Intent(getApplicationContext(), ScanActivity.class);
                                scan.putExtra("location", locationaddress);
                                scan.putExtra("latitude", mCurrentLocation.getLatitude());
                                scan.putExtra("longitude", mCurrentLocation.getLongitude());
                                startActivity(scan);
                            } else if (!myshare.getString("Usertype", "").equals("Mechanic") || myshare.getString("Usertype", "").equals("Dealer / Retailer")) {
                                Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                                alertbox.showAlert("You are not Authorized");

                            }
                        } else if (response.body().getResult().equals("NotVerified")) {
                            loading.dismiss();
                            checkuserexists();

                        } else {
                            loading.dismiss();
                            Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                            alertbox.showAlert("Please Try Again Later");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<VerifiedData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();


                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void checkuserexists() {
        try {
            final ProgressDialog loading = ProgressDialog.show(HomeScreenActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<CheckRegister> call = service.checkreg(myshare.getString("MobNo", ""));
            call.enqueue(new Callback<CheckRegister>() {
                @Override
                public void onResponse(Call<CheckRegister> call, Response<CheckRegister> response) {
                    try {
                        if (response.body().getResult().equals("Success")) {
                            loading.dismiss();
                            final AlertDialog alertDialogotpverify = new AlertDialog.Builder(HomeScreenActivity.this).create();
                            final View olduserverify = LayoutInflater.from(HomeScreenActivity.this).inflate(R.layout.otpalertoldnumberverify, null);
                            final EditText nameold = olduserverify.findViewById(R.id.nameold);
                            final EditText emailold = olduserverify.findViewById(R.id.emailold);
                            final EditText mobilenoold = olduserverify.findViewById(R.id.mobilenoold);
                            final TextView otpmsg1old = olduserverify.findViewById(R.id.otpmsg1old);
                            final TextView otpmsg2old = olduserverify.findViewById(R.id.otpmsg2old);
                            final EditText businessnameold = olduserverify.findViewById(R.id.businessnameold);
                            final Spinner usertypeold = olduserverify.findViewById(R.id.usertypeold);
                            final EditText addressssold = olduserverify.findViewById(R.id.addressssold);
                            final Button generateotpforolduser = olduserverify.findViewById(R.id.generateotpforolduser);
                            Button verifyolduser = olduserverify.findViewById(R.id.verifyolduser);
                            final Pinview pinviewolduserverify = olduserverify.findViewById(R.id.pinviewolduserverify);
                            mobilenoold.setText(response.body().getData().getMobileno());
                            businessnameold.setText(response.body().getData().getBusinessName());
                            nameold.setText(response.body().getData().getName());
                            addressssold.setText(locationaddress);
                            ArrayList usertypes = new ArrayList();
                            usertypes.add("Distributors");
                            usertypes.add("Dealer / Retailer");
                            usertypes.add("Mechanic");
                            usertypes.add("Gates Employee");
                            usertypes.add("Others");
                            if (usertypes.contains(response.body().getData().getUsertype())) {
                                usertypes.remove(response.body().getData().getUsertype());
                            }
                            usertypes.add(0, response.body().getData().getUsertype());

                            ArrayAdapter<String> xxxl = new ArrayAdapter<String>(HomeScreenActivity.this, android.R.layout.simple_list_item_1, usertypes);
                            usertypeold.setAdapter(xxxl);
                            emailold.setText(response.body().getData().getEmail());

                            generateotpforolduser.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    otpmsg1old.setVisibility(View.VISIBLE);
                                    otpmsg2old.setVisibility(View.VISIBLE);
                                    generateotpforolduser.setText("Resend OTP");
                                    Name = nameold.getText().toString();
                                    Mobno = mobilenoold.getText().toString();
                                    email = emailold.getText().toString();
                                    businessname = businessnameold.getText().toString();
                                    usertype = usertypeold.getSelectedItem().toString();
                                    address = addressssold.getText().toString();
                                    if (Mobno.length() != 10) {
                                        Toast.makeText(HomeScreenActivity.this, "Mobile Number Must be 10 digits", Toast.LENGTH_SHORT).show();
                                    } else if (businessname.equals("")) {
                                        Toast toast = Toast.makeText(HomeScreenActivity.this, "Please Enter Business Name", Toast.LENGTH_LONG);
                                        toast.show();
                                    } else if (Usertype.equals("Type Of User")) {
                                        Toast.makeText(HomeScreenActivity.this, "Select User Type", Toast.LENGTH_LONG).show();
                                    } else {
                                        sendOtp();
                                        pinviewolduserverify.setVisibility(View.VISIBLE);
                                        otpgenerated = true;
                                    }
                                }
                            });
                            verifyolduser.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (otpgenerated) {
                                        name = nameold.getText().toString();
                                        email = emailold.getText().toString();
                                        businessname = businessnameold.getText().toString();
                                        usertype = usertypeold.getSelectedItem().toString();
                                        address = addressssold.getText().toString();
                                        enteredotp = pinviewolduserverify.getValue();
                                        if (businessname.equals("")) {
                                            Toast toast = Toast.makeText(HomeScreenActivity.this, "Please Enter Business Name", Toast.LENGTH_LONG);
                                            toast.show();
                                        } else if (Usertype.equals("Type Of User")) {
                                            Toast.makeText(HomeScreenActivity.this, "Select User Type", Toast.LENGTH_LONG).show();
                                        } else {

                                            alertDialogotpverify.dismiss();
                                            verifieduser(usertype, businessname);
                                        }
                                    } else {
                                        Toast.makeText(HomeScreenActivity.this, "Please Generate OTP then verify", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            });
                            alertDialogotpverify.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations;
                            alertDialogotpverify.setView(olduserverify);
                            alertDialogotpverify.setCanceledOnTouchOutside(false);
                            alertDialogotpverify.show();
                            ;

                        } else if (response.body().getResult().equals("Notsuccess")) {
                            loading.dismiss();
                            Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                            alertbox.showAlert("Please Try Again Later");

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                        alertbox.showAlert("Please Try Again Later");
                    }
                }

                @Override
                public void onFailure(Call<CheckRegister> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                    alertbox.showAlert("Please Try Again Later");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void sendOtp() {
        try {
            final ProgressDialog loading = ProgressDialog.show(HomeScreenActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<SendOTP> call = service.sendtop(Mobno, Name, usertype, email, businessname, address);
            call.enqueue(new Callback<SendOTP>() {
                @Override
                public void onResponse(Call<SendOTP> call, Response<SendOTP> response) {
                    try {
                        if (response.body().getResult().equals("Success")) {
                            loading.dismiss();
                            otp.add(response.body().getData());
                        } else if (response.body().getResult().equals("Notsuccess")) {
                            loading.dismiss();
                            Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                            alertbox.showAlert("Please Try Again Later");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                        alertbox.showAlert("Please Try Again Later");
                    }
                }

                @Override
                public void onFailure(Call<SendOTP> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                    alertbox.showAlert("Please Try Again Later");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
            alertbox.showAlert("Please Try Again Later");
        }
    }

    private void verifieduser(final String usertype, final String bname) {
        try {
            final ProgressDialog loading = ProgressDialog.show(HomeScreenActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();

            Call<OldUserVerify> call = service.newverify(name, Mobno, email, usertype, "Verified", enteredotp, businessname, address);
            call.enqueue(new Callback<OldUserVerify>() {
                @Override
                public void onResponse(Call<OldUserVerify> call, Response<OldUserVerify> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Verified")) {
                            editor.putString("Bname", businessname);
                            editor.putString("MobNo", Mobno);
                            editor.putString("Name", name);
                            editor.putString("Usertype", usertype);
                            editor.commit();
                            recreate();
                            if (usertype.equals("Gates Employee")) {
                                Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                                alertbox.showAlert("Please Wait For Admin Approval");
                            } else if (usertype.equals("Mechanic")) {
                                Intent scan = new Intent(getApplicationContext(), ScanActivity.class);
                                scan.putExtra("location", locationaddress);
                                scan.putExtra("latitude", mCurrentLocation.getLatitude());
                                scan.putExtra("longitude", mCurrentLocation.getLongitude());
                                startActivity(scan);
                            } else {
                                Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                                alertbox.showAlert("You are not Authorized");
                            }
                        } else if (response.body().getResult().equals("OTPInvalid")) {
                            loading.dismiss();
                            Toast.makeText(getApplicationContext(), "Invalid OTP", Toast.LENGTH_SHORT).show();
                        } else if (response.body().getResult().equals("NotVerified")) {
                            loading.dismiss();
                            Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                            alertbox.showAlert("Usertype Mismatch Please Select Registered User Type");
                        } else {
                            loading.dismiss();
                            Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                            alertbox.showAlert("Please Try Again Later");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                        alertbox.showAlert("Please Try Again Later");
                    }
                }

                private void requestpremission() {
                    int MY_PERMISSIONS_REQUEST_CAMERA = 0;
                    if (ContextCompat.checkSelfPermission(HomeScreenActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    } else {
                        ActivityCompat.requestPermissions(HomeScreenActivity.this, new String[]{Manifest.permission.CAMERA}, 9);
                    }
                }

                @Override
                public void onFailure(Call<OldUserVerify> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
                    alertbox.showAlert("Please Try Again Later");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Alertbox alertbox = new Alertbox(HomeScreenActivity.this);
            alertbox.showAlert("This Part Number is Not Eligible For Reward Points");
        }


    }

    private void initPermission() {
        if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(HomeScreenActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(HomeScreenActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 100);
//            init();
        } else {
            permissionGranted = true;
            init();
            startLocationUpdate();
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, final String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            int len = permissions.length;
            for (int i = 0; i < len; i++) {
                if (ActivityCompat.checkSelfPermission(HomeScreenActivity.this, permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = false;
                }
            }

            if (permissionGranted) {
//                permissionGranted = true;
                init();
//            if(isLocationSuccess)

                startLocationUpdate();

            } else {

                AlertDialog.Builder dialog = new AlertDialog.Builder(HomeScreenActivity.this);
                dialog.setTitle("Location");
                dialog.setMessage("You did not give permission to access your Location. Do want to exit");
                dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestManualPermission(permissions, requestCode);
                        permissionGranted = true;
                    }
                });
                dialog.show();
            }
        }
    }

    private void requestManualPermission(String[] permissions, int requestCode) {
        ActivityCompat.requestPermissions(this, permissions, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
//                        startLocationUpdate();
                        if (permissionGranted) {
                            startLocationUpdate();
                        } else {
                            initPermission();
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeScreenActivity.this);
                        alertDialog.setMessage("If you don't enable GPS, Registration Not To be Done Please Enable GPS");
                        alertDialog.setTitle("Gates Catalog");

                        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        });

                        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                initPermission();
                            }
                        });
                        alertDialog.setCancelable(false);
                        alertDialog.show();
                        break;
                    default:
                        finish();
                        break;
                }
                break;
        }
    }

    private void init() {

        if (permissionGranted) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mSettingsClient = LocationServices.getSettingsClient(this);

            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    // location is received
                    mCurrentLocation = locationResult.getLastLocation();
//                    mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                    updateLocationUI();

                }
            };

            mRequestingLocationUpdates = false;
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setFastestInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(mLocationRequest);
            mLocationSettingsRequest = builder.build();
//        startLocationButtonClick();
        }
    }

    private void startLocationUpdate() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

//                        Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());


//                        LocationManager manager= (LocationManager) getSystemService(LOCATION_SERVICE);


//                        updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(HomeScreenActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(HomeScreenActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                                break;
                            case LocationSettingsStatusCodes.SUCCESS:
                                permissionGranted = true;
                                init();
                        }
//                        updateLocationUI();
                    }
                });
    }

    private void updateLocationUI() {
        if (mCurrentLocation != null) {
//            addressss.setText(
//                    "Lat: " + mCurrentLocation.getLatitude() + ", " +
//                            "Lng: " + mCurrentLocation.getLongitude()
//            );
            new GeocodeAsyncTask().execute(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        }
    }

    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {

        String errorMessage = "";

        @SuppressLint("LongLogTag")
        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(HomeScreenActivity.this, Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
                    Log.d(TAG, "doInBackground: ************");
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }
                if (addresses != null && addresses.size() > 0)
                    return addresses.get(0);
            }
//            else {
//                new GetGeoCodeAPIAsynchTask().execute(latlang[0], latlang[1]);
//            }

            return null;
        }

        @SuppressLint("LongLogTag")
        protected void onPostExecute(Address addresss) {

            if (addresss == null) {
//                new GetGeoCodeAPIAsynchTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
                Log.d(TAG, "onPostExecute: *****");
            } else {
//                progressBar.setVisibility(View.GONE);
                String address = addresss.getAddressLine(0);
                editor.putString("ToAddress", address);
                editor.commit();
                editor.apply();
                String City = addresss.getLocality();
                Log.d(TAG, "onPostExecute: **************************" + City);
                String city = addresss.getLocality();
                String state = addresss.getAdminArea();
                //create your custom title
                String title = city + "-" + state;
                locationaddress = address +
                        "\n"
                        + title;
                editor.putString("FromAddress", address);
                editor.commit();
//                addressss.setText(address +
//                        "\n"
//                        + title);
//                locationaddress=addressss.getText().toString();
                editor.putString("FromAddress", address);
                Geocoder geocoder = new Geocoder(HomeScreenActivity.this);
                try {
                    ArrayList<Address> addresses = (ArrayList<Address>) geocoder.getFromLocationName("karur", 50);
                    for (Address address3 : addresses) {
                        double lat = address3.getLatitude();
                        double lon = address3.getLongitude();
//                        address2.setText(lat +
//                                "\n"
//                                + lon);

                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }
                //create your custom title
//                String title = city + "-" + state;
//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertboxwithback("Your Current location is "+city);


            }
        }
    }
}