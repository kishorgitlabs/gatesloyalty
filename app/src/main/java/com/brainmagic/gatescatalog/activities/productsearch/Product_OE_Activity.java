package com.brainmagic.gatescatalog.activities.productsearch;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainmagic.gatescatalog.activities.loyalty.ViewProfileActivity;
import com.brainmagic.gatescatalog.adapter.OEPartSearchAdapter;
import com.brainmagic.gatescatalog.adapter.OeSearchAdapter;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.Sample;
import com.brainmagic.gatescatalog.api.models.oesearch.OESearch;
import com.brainmagic.gatescatalog.api.models.oesearchpartno.OESearchPartNo;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.bumptech.glide.Glide;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.adapter.OE_Search_Adapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Product_OE_Activity extends Activity {

    private ListView listView;
    private SQLiteDatabase db;
    private Cursor c;
    //    private ArrayList<String> Vechicle_name_List;
    private ArrayList<String> supplierList;
    private ArrayList<String> descList, Part_Number_List, ImageName_List;
    private AutoCompleteTextView searchbox;
    private String id, segment_id, vehiclelist;
    private ImageView menu;
    private ImageView searchbtn, info_Search;
    private HorizontalScrollView horizlist;
    private ProgressDialog progressDialog;
    private Alertbox box = new Alertbox(Product_OE_Activity.this);

    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String Email, Mobno, Usertype, Country;
    private OE_Search_Adapter adapter;

    private TextView ProfileName;
    //    private ConstraintLayout signout_relativelayout;
    private LinearLayout signout_relativelayout, oeSearchPartNoLayout, oeSearchEmptyLayout;
    private FloatingActionButton back, home;
    private ViewGroup includeviewLayout;
    private ImageView profilePicture;
    private String word;
    private OEPartSearchAdapter searchAdapter;
    private RelativeLayout relativeLayoutOE;

    private TextView searchResult;
    private List<String> searchList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_oe);
        listView = (ListView) findViewById(R.id.product_oe_search_list);
        searchbox = findViewById(R.id.text_search_box);
        searchbtn = (ImageView) findViewById(R.id.search);
        info_Search = (ImageView) findViewById(R.id.info_search);
        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        menu = (ImageView) findViewById(R.id.menu);
        oeSearchPartNoLayout = findViewById(R.id.gates_oe_search_part_no);
        oeSearchEmptyLayout = findViewById(R.id.list_view_empty_oe_search);
        searchResult = findViewById(R.id.search_result);
        Part_Number_List = new ArrayList<>();
        searchList = new ArrayList<>();
        searchResult.setVisibility(View.GONE);
        relativeLayoutOE = findViewById(R.id.relative_layout_oe);

        includeviewLayout = findViewById(R.id.profile_logo);
        ProfileName = (TextView) includeviewLayout.findViewById(R.id.profilename);
        profilePicture = (ImageView) includeviewLayout.findViewById(R.id.profilepicture);
        signout_relativelayout = findViewById(R.id.signout);
        signout_relativelayout.setVisibility(View.GONE);

       /* horizlist = (HorizontalScrollView) findViewById(R.id.horilist);
        horizlist.setVisibility(View.GONE);*/


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();

        Email = myshare.getString("email", "");
        Mobno = myshare.getString("MobNo", "");
        Usertype = myshare.getString("Usertype", "");
        Country = myshare.getString("Country", "");
        ProfileName.setText(myshare.getString("name", "") + " , " + Country);
        final ImageView downArrow = includeviewLayout.findViewById(R.id.down_arrow);
        final RelativeLayout userData = includeviewLayout.findViewById(R.id.user_data);
        if (!myshare.getString("profile_path", "").equals(""))
            Glide.with(Product_OE_Activity.this)
                    .load(new File(myshare.getString("profile_path", "")))
                    .into(profilePicture);

        if (myshare.getBoolean("islogin", false))
            signout_relativelayout.setVisibility(View.VISIBLE);
        else
            signout_relativelayout.setVisibility(View.GONE);

        signout_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("islogin", false).commit();
                final Snackbar mySnackbar = Snackbar.make(findViewById(R.id.oe_grid_activity), getString(R.string.channel_network_sign_out), Snackbar.LENGTH_INDEFINITE);
                mySnackbar.setActionTextColor(getResources().getColor(R.color.red));
                mySnackbar.setAction(getString(R.string.okay_button), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mySnackbar.dismiss();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);

                    }
                });
                mySnackbar.show();


            }
        });

//        downArrow.setOnClickListener(new View.OnClickListener() {
//            boolean visible;
//            @Override
//            public void onClick(View v) {
//                ChangeBounds changeBounds=new ChangeBounds();
//                changeBounds.setDuration(600L);
//                TransitionManager.beginDelayedTransition(includeviewLayout,changeBounds);
//                visible = !visible;
//                if(visible)
//                    downArrow.animate().rotation(180).setInterpolator(new LinearInterpolator()).setDuration(500);
//                else
//                    downArrow.animate().rotation(0).setInterpolator(new LinearInterpolator()).setDuration(500);
//                userData.setVisibility(visible? View.VISIBLE: View.GONE);
//            }
//        });


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(Product_OE_Activity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Product_OE_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(Product_OE_Activity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;
                            case R.id.viewprofile:
                                startActivity(new Intent(Product_OE_Activity.this, ViewProfileActivity.class));
                                return true;
                            case R.id.aboutpop:
                                startActivity(new Intent(Product_OE_Activity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(Product_OE_Activity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Product_OE_Activity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Product_OE_Activity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Product_OE_Activity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(Product_OE_Activity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Product_OE_Activity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Product_OE_Activity.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Product_OE_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(Product_OE_Activity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Model_List_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });

        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();

                startActivity(new Intent(Product_OE_Activity.this, MoreDetailsActivity.class)
                                .putExtra("searchType","OeSearch")
//                        .putExtra("navtxt", "OE >" + Part_Number_List.get(position))
                                .putExtra("navtxt","OE >" + item)
                                .putExtra("partNo",item)
                );
            }
        });*/


//        setAutoCompleteAdapter();


        searchbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                word = searchbox.getText().toString().toUpperCase();
//                String OENumber = "981702";
//                String crossPlatform = "10PK1045";
//                String gatesNumber = "10PK1070";
//                if (word.equals(OENumber) || word.equals(crossPlatform))
//                {
//                    horizlist.setVisibility(View.VISIBLE);
//                    setListView();
//                }
//                else {
//                if(!TextUtils.isEmpty(word)){
//                    getOESearchPartNo(word);
//                    getOESearchPartNoNew(word);
//                }
                if (word.equals("")){
                    relativeLayoutOE.setVisibility(View.GONE);
                    oeSearchEmptyLayout.setVisibility(View.GONE);
                    Alertbox alertbox = new Alertbox(Product_OE_Activity.this);
                    alertbox.showAlert("Please Enter the Number");
                }else if (word.length() <= 2) {
                    relativeLayoutOE.setVisibility(View.GONE);
                    oeSearchEmptyLayout.setVisibility(View.GONE);
                    searchbox.setError("Enter the Minimum Three Words");
                } else if (!TextUtils.isEmpty(word)) {
//                    getOESearchPartNoNew(word);
                } /*else {
                    relativeLayoutOE.setVisibility(View.GONE);
                    oeSearchEmptyLayout.setVisibility(View.GONE);
                    Alertbox alertbox = new Alertbox(Product_OE_Activity.this);
                    alertbox.showAlert("Please Enter the Number");
                }*/
//                 if(!word.isEmpty()) {
//                     String Query = " Select distinct * from oemparts where OENumber = '" + word + "'  order by OENumber asc";
//                     catalog(Query);

//                 }
//                 else
//                 {
//                     horizlist.setVisibility(View.GONE);
//                 }
            }
        });
        // String Query = " Select distinct * from oemparts";
        //catalog(Query);

        info_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                box.showAlert("⚠️ \n For general reference only. Check catalog for correct application. \n" +
//                        "Any use of this interchange to determine application is done at the installers risk.\n");
                box.showAlert(getString(R.string.oe_info));
            }
        });


    }

    private void getOESearchPartNoNew(String word) {
        try {

            final CustomProgressDialog progressDialog = new CustomProgressDialog(Product_OE_Activity.this);
            progressDialog.setCancelable(false);
            progressDialog.show();


            APIService service = RetrofitClient.getApiService();

            Call<OESearch> call = service.oeSearchNew(word);

            call.enqueue(new Callback<OESearch>() {
                @Override
                public void onResponse(Call<OESearch> call, Response<OESearch> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        OeSearchAdapter oeSearchAdapter = new OeSearchAdapter(Product_OE_Activity.this, response.body().getData());
                        listView.setAdapter(oeSearchAdapter);
                        relativeLayoutOE.setVisibility(View.VISIBLE);
                        oeSearchEmptyLayout.setVisibility(View.GONE);
                    } else {
                        relativeLayoutOE.setVisibility(View.GONE);
                        oeSearchEmptyLayout.setVisibility(View.VISIBLE);

                    }
                }

                @Override
                public void onFailure(Call<OESearch> call, Throwable t) {
                    progressDialog.dismiss();
                    box.showAlert(getString(R.string.server_error));
                    relativeLayoutOE.setVisibility(View.GONE);
                    oeSearchEmptyLayout.setVisibility(View.VISIBLE);
                }
            });

        }
        catch (Exception e) {
            e.printStackTrace();
            box.showAlertWithBack(getString(R.string.internet_error_msg));
        }
    }

//    private void getOESearchPartNo(String word) {
//        try{
//        final CustomProgressDialog progressDialog =new CustomProgressDialog(Product_OE_Activity.this);
//        progressDialog.setCancelable(false);
//        progressDialog.show();
//
//
//            APIService service = RetrofitClient.getApiService();
//
//            Call<OESearchPartNo> call = service.oeSearchPartNo(word);
//
//            call.enqueue(new Callback<OESearchPartNo>() {
//                @Override
//                public void onResponse(Call<OESearchPartNo> call, Response<OESearchPartNo> response) {
//                    progressDialog.dismiss();
//                    if (response.body().getResult().equals("Success")){
//                        searchList.clear();
//                        searchResult.setVisibility(View.VISIBLE);
//                        searchResult.setText("Search Result for - "+word);
//                        listView.setVisibility(View.VISIBLE);
//                        oeSearchPartNoLayout.setVisibility(View.VISIBLE);
//                        oeSearchEmptyLayout.setVisibility(View.GONE);
//                        searchList = response.body().getData();
//                        searchAdapter = new OEPartSearchAdapter(Product_OE_Activity.this, searchList);
//                        listView.setAdapter(searchAdapter);
//
//                    }else {
////                        box.showAlert("Not found");
//                        searchResult.setVisibility(View.VISIBLE);
//                        searchResult.setText("Search Result for - "+word);
//                        searchList.clear();
//                        listView.setVisibility(View.GONE);
//                        oeSearchEmptyLayout.setVisibility(View.VISIBLE);
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<OESearchPartNo> call, Throwable t) {
//                    progressDialog.dismiss();
//                    searchResult.setText("Search Result for - "+word);
//                   box.showAlertWithBack("Failed to Reach the Server. Please try again Later");
//                }
//            });
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//
////
////        startActivity(new Intent(Product_OE_Activity.this, MoreDetailsActivity.class)
////                .putExtra("searchType","OeSearch")
////                .putExtra("navtxt", "Search > " + word)
////                .putExtra("partNo",word)
////        );
//    }

    private void setAutoCompleteAdapter() {
        List<String> partNumbers = new ArrayList<>();
        partNumbers.add("981702");
        partNumbers.add("10PK1045");
        partNumbers.add("10PK1070");
        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(Product_OE_Activity.this, R.layout.support_simple_spinner_dropdown_item, partNumbers);
        searchbox.setAdapter(arrayAdapter);
        searchbox.setThreshold(1);
    }


    @Override
    protected void onResume() {
        super.onResume();

//        if (myshare.getBoolean("islogin", false))
//            signout_relativelayout.setVisibility(View.VISIBLE);
//        else
//            signout_relativelayout.setVisibility(View.GONE);

    }

    private void setListView() {
//        Vechicle_name_List = new ArrayList<>();
        supplierList = new ArrayList<>();
        descList = new ArrayList<>();
        Part_Number_List = new ArrayList<>();
        ImageName_List = new ArrayList<>();

//        Vechicle_name_List.add("Tata");
        supplierList.add("VOLVO");
        descList.add("Micro-V® Belt");
        Part_Number_List.add("10PK1045");
        ImageName_List.add("asdf");
        adapter = new OE_Search_Adapter(Product_OE_Activity.this, supplierList, descList, Part_Number_List);
        listView.setAdapter(adapter);

    }


}