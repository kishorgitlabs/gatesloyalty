package com.brainmagic.gatescatalog.activities.loyalty;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.brainmagic.gatescatalog.api.models.gates.approvedlist.Datum;
import com.brainmagic.gatescatalog.gatesloyalty.R;

import java.util.List;

public class ApprovedAdapter extends BaseAdapter {

    private Context mContext;
    private List<Datum> list;

    public ApprovedAdapter(Context mContext, List<Datum> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.approved_view_layout, viewGroup, false);
        TextView approvedName = view.findViewById(R.id.approved_name);
        TextView approvedMobileNo = view.findViewById(R.id.approved_mobile_no);
        TextView approvedEmail = view.findViewById(R.id.approved_email);
        TextView approvedUserType = view.findViewById(R.id.approved_user_type);
        TextView approvedShopName = view.findViewById(R.id.approved_shop_name);
        TextView approvedStatus = view.findViewById(R.id.approved_status);
        TextView approvedRegistrationType = view.findViewById(R.id.registration_type);

        String ApprovedName = list.get(i).getName();
        String ApprovedMobileNo = list.get(i).getMobileno();
        String ApprovedEmail = list.get(i).getEmail();
        String ApprovedUserType = list.get(i).getUsertype();
        String ApprovedShopName = String.valueOf(list.get(i).getShopName());
        String ApprovedStatus = list.get(i).getStatus();
        String ApprovedRegistrationType = list.get(i).getRegistrationType();


        approvedName.setText(ApprovedName);
        approvedMobileNo.setText(ApprovedMobileNo);
        approvedEmail.setText(ApprovedEmail);
        approvedUserType.setText(ApprovedUserType);
        approvedShopName.setText(ApprovedShopName);
        approvedStatus.setText(ApprovedStatus);
        approvedRegistrationType.setText(ApprovedRegistrationType);

        return view;
    }
}
