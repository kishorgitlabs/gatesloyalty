package com.brainmagic.gatescatalog.activities.loyalty;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.redeenpoints.RedeemPoints;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RewardPointsHistory extends AppCompatActivity {

    private FloatingActionButton back, home;
    private Alertbox alertbox;
    SharedPreferences myShare;
    SharedPreferences.Editor sharedEditor;
    private TextView tottalpoints, claimedpoints, blancedpoints;

    private int blanPoint = 0, total = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward_points_history);

        back = findViewById(R.id.back);
        home = findViewById(R.id.home);
        blancedpoints = findViewById(R.id.blancedpoints);
        claimedpoints = findViewById(R.id.claimedpoints);
        tottalpoints = findViewById(R.id.tottalpoints);
        alertbox = new Alertbox(RewardPointsHistory.this);
        myShare = getSharedPreferences("Registration", MODE_PRIVATE);
        sharedEditor = myShare.edit();


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent home = new Intent(RewardPointsHistory.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });

        Network_Connection_Activity connection_activity = new Network_Connection_Activity(RewardPointsHistory.this);
        if (connection_activity.CheckInternet()) {

            rewardpoints();
        } else {
            alertbox.showAlert("Please Check Your Internet Connection");
        }

        tottalpoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RewardPointsHistory.this, ScanHistoryLoyaltyActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void rewardpoints() {

        final ProgressDialog loading = ProgressDialog.show(RewardPointsHistory.this, getResources().getString(R.string.app_name), "Loading...", false, false);

        APIService service = RetrofitClient.getApiService();

        Call<RedeemPoints> call = service.redeemPoints(myShare.getString("MobNo", ""));
        call.enqueue(new Callback<RedeemPoints>() {
            @Override
            public void onResponse(Call<RedeemPoints> call, Response<RedeemPoints> response) {
                loading.dismiss();
                if (response.body().getResult().equals("Success")) {
                    String b = response.body().getData();

                    SpannableString content = new SpannableString(response.body().getData());
                    content.setSpan(new UnderlineSpan(), 0, content.length(),0);


                    blancedpoints.setText(b);
                    tottalpoints.setText(content);
                }else if (response.body().getResult().equals("No Record")){
                    alertbox.showAlertWithBack("No Record Found !");
                }else {
                    alertbox.showAlert("Please try again Later.");
                }
            }

            @Override
            public void onFailure(Call<RedeemPoints> call, Throwable t) {
                loading.dismiss();
                alertbox.showAlert("Server Connection is low.Please try again Later.");

            }
        });
    }
}