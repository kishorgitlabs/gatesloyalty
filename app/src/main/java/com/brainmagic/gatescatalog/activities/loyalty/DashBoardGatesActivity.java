package com.brainmagic.gatescatalog.activities.loyalty;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.gatesloyalty.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class DashBoardGatesActivity extends AppCompatActivity {

    private RelativeLayout addRetailer, addMechanic, approvalPending, approved;

    private FloatingActionButton home, back;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board_gates);
        addRetailer = findViewById(R.id.add_retailer_layout);
        addMechanic = findViewById(R.id.add_mechanic_layout);
        approvalPending = findViewById(R.id.approval_pending_layout);
        approved = findViewById(R.id.view_approved_list_layout);
        home = findViewById(R.id.home);
        back = findViewById(R.id.back);
//        back.setVisibility(View.GONE);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent back = new Intent(DashBoardGatesActivity.this, HomeScreenActivity.class);
                back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(back);
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent home = new Intent(DashBoardGatesActivity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });

        addMechanic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardGatesActivity.this, AddMechanicActivity.class);
                startActivity(intent);
            }
        });

        addRetailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardGatesActivity.this, AddRetailerActivity.class);
                startActivity(intent);
            }
        });

        approvalPending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardGatesActivity.this, ApprovalPendingActivity.class);
                startActivity(intent);
            }
        });

        approved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardGatesActivity.this, ApprovedListActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent back = new Intent(DashBoardGatesActivity.this, HomeScreenActivity.class);
        back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(back);
    }
}