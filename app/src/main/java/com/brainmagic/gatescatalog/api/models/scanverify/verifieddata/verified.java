package com.brainmagic.gatescatalog.api.models.scanverify.verifieddata;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 * */
public class verified {
  @SerializedName("Status")
  @Expose
  private String Status;
  @SerializedName("BusinessName")
  @Expose
  private Object BusinessName;
  @SerializedName("usertype")
  @Expose
  private String usertype;
  @SerializedName("mobileno")
  @Expose
  private Long mobileno;
  @SerializedName("Date")
  @Expose
  private String Date;
  @SerializedName("appid")
  @Expose
  private String appid;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("RegistrationType")
  @Expose
  private String RegistrationType;
  @SerializedName("Country")
  @Expose
  private Object Country;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("OTPStatus")
  @Expose
  private String OTPStatus;
  @SerializedName("updatetime")
  @Expose
  private Object updatetime;
  @SerializedName("email")
  @Expose
  private String email;
  @SerializedName("Password")
  @Expose
  private Object Password;
  public void setStatus(String Status){
   this.Status=Status;
  }
  public String getStatus(){
   return Status;
  }
  public void setBusinessName(Object BusinessName){
   this.BusinessName=BusinessName;
  }
  public Object getBusinessName(){
   return BusinessName;
  }
  public void setUsertype(String usertype){
   this.usertype=usertype;
  }
  public String getUsertype(){
   return usertype;
  }
  public void setMobileno(Long mobileno){
   this.mobileno=mobileno;
  }
  public Long getMobileno(){
   return mobileno;
  }
  public void setDate(String Date){
   this.Date=Date;
  }
  public String getDate(){
   return Date;
  }
  public void setAppid(String appid){
   this.appid=appid;
  }
  public String getAppid(){
   return appid;
  }
  public void setName(String name){
   this.name=name;
  }
  public String getName(){
   return name;
  }
  public void setRegistrationType(String RegistrationType){
   this.RegistrationType=RegistrationType;
  }
  public String getRegistrationType(){
   return RegistrationType;
  }
  public void setCountry(Object Country){
   this.Country=Country;
  }
  public Object getCountry(){
   return Country;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setOTPStatus(String OTPStatus){
   this.OTPStatus=OTPStatus;
  }
  public String getOTPStatus(){
   return OTPStatus;
  }
  public void setUpdatetime(Object updatetime){
   this.updatetime=updatetime;
  }
  public Object getUpdatetime(){
   return updatetime;
  }
  public void setEmail(String email){
   this.email=email;
  }
  public String getEmail(){
   return email;
  }
  public void setPassword(Object Password){
   this.Password=Password;
  }
  public Object getPassword(){
   return Password;
  }
}