package com.brainmagic.gatescatalog.api.models.moredetails;

import com.google.gson.annotations.SerializedName;

public class PDFModel {

    @SerializedName("ProductthumbURLpdf")
    String pdfLink;
    @SerializedName("pdfname")
    String pdfName;

    public String getPdfName() {
        return pdfName;
    }

    public void setPdfName(String pdfName) {
        this.pdfName = pdfName;
    }

    public String getPdfLink() {
        return pdfLink;
    }

    public void setPdfLink(String pdfLink) {
        this.pdfLink = pdfLink;
    }
}
