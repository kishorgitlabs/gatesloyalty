
package com.brainmagic.gatescatalog.api.models.gateslogin;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Data {

    @SerializedName("City")
    private String mCity;
    @SerializedName("ConfirmPassword")
    private String mConfirmPassword;
    @SerializedName("Country")
    private String mCountry;
    @SerializedName("Department")
    private String mDepartment;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("Gates_Emp_No")
    private String mGatesEmpNo;
    @SerializedName("id")
    private Long mId;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("State")
    private String mState;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("UpdateDate")
    private Object mUpdateDate;
    @SerializedName("UserType")
    private String mUserType;

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getConfirmPassword() {
        return mConfirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        mConfirmPassword = confirmPassword;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getDepartment() {
        return mDepartment;
    }

    public void setDepartment(String department) {
        mDepartment = department;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getGatesEmpNo() {
        return mGatesEmpNo;
    }

    public void setGatesEmpNo(String gatesEmpNo) {
        mGatesEmpNo = gatesEmpNo;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String insertDate) {
        mInsertDate = insertDate;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Object getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(Object updateDate) {
        mUpdateDate = updateDate;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

}
