
package com.brainmagic.gatescatalog.api.models.gates.searchapproved;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SearchApproved {

    @SerializedName("data")
    private List<Datum> mData;
    @SerializedName("result")
    private String mResult;

    public List<Datum> getData() {
        return mData;
    }

    public void setData(List<Datum> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
