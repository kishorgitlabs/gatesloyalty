
package com.brainmagic.gatescatalog.api.models.gates.approvedlist;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Datum {

    @SerializedName("Address")
    private Object mAddress;
    @SerializedName("appid")
    private String mAppid;
    @SerializedName("ApproveStatus")
    private String mApproveStatus;
    @SerializedName("BusinessName")
    private String mBusinessName;
    @SerializedName("City")
    private Object mCity;
    @SerializedName("Country")
    private Object mCountry;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("Gstin")
    private Object mGstin;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Insertdate")
    private Object mInsertdate;
    @SerializedName("Latitude")
    private Object mLatitude;
    @SerializedName("Location")
    private Object mLocation;
    @SerializedName("Longitude")
    private Object mLongitude;
    @SerializedName("mobileno")
    private String mMobileno;
    @SerializedName("name")
    private String mName;
    @SerializedName("OTPStatus")
    private String mOTPStatus;
    @SerializedName("Password")
    private Object mPassword;
    @SerializedName("RegistrationType")
    private String mRegistrationType;
    @SerializedName("ShopName")
    private Object mShopName;
    @SerializedName("State")
    private Object mState;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("updatetime")
    private Object mUpdatetime;
    @SerializedName("usertype")
    private String mUsertype;

    public Object getAddress() {
        return mAddress;
    }

    public void setAddress(Object address) {
        mAddress = address;
    }

    public String getAppid() {
        return mAppid;
    }

    public void setAppid(String appid) {
        mAppid = appid;
    }

    public String getApproveStatus() {
        return mApproveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        mApproveStatus = approveStatus;
    }

    public String getBusinessName() {
        return mBusinessName;
    }

    public void setBusinessName(String businessName) {
        mBusinessName = businessName;
    }

    public Object getCity() {
        return mCity;
    }

    public void setCity(Object city) {
        mCity = city;
    }

    public Object getCountry() {
        return mCountry;
    }

    public void setCountry(Object country) {
        mCountry = country;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Object getGstin() {
        return mGstin;
    }

    public void setGstin(Object gstin) {
        mGstin = gstin;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Object getInsertdate() {
        return mInsertdate;
    }

    public void setInsertdate(Object insertdate) {
        mInsertdate = insertdate;
    }

    public Object getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Object latitude) {
        mLatitude = latitude;
    }

    public Object getLocation() {
        return mLocation;
    }

    public void setLocation(Object location) {
        mLocation = location;
    }

    public Object getLongitude() {
        return mLongitude;
    }

    public void setLongitude(Object longitude) {
        mLongitude = longitude;
    }

    public String getMobileno() {
        return mMobileno;
    }

    public void setMobileno(String mobileno) {
        mMobileno = mobileno;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getOTPStatus() {
        return mOTPStatus;
    }

    public void setOTPStatus(String oTPStatus) {
        mOTPStatus = oTPStatus;
    }

    public Object getPassword() {
        return mPassword;
    }

    public void setPassword(Object password) {
        mPassword = password;
    }

    public String getRegistrationType() {
        return mRegistrationType;
    }

    public void setRegistrationType(String registrationType) {
        mRegistrationType = registrationType;
    }

    public Object getShopName() {
        return mShopName;
    }

    public void setShopName(Object shopName) {
        mShopName = shopName;
    }

    public Object getState() {
        return mState;
    }

    public void setState(Object state) {
        mState = state;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Object getUpdatetime() {
        return mUpdatetime;
    }

    public void setUpdatetime(Object updatetime) {
        mUpdatetime = updatetime;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

}
