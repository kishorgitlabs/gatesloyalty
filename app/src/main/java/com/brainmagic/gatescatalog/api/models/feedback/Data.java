package com.brainmagic.gatescatalog.api.models.feedback;

import java.io.Serializable;

public class Data implements Serializable {
	private int Id;
	private String Name;
	private String Email;
	private String MobileNo;
	private String Message;
	private String Inserdate;

	public void setId(int id){
		this.Id = id;
	}

	public int getId(){
		return Id;
	}

	public void setName(String name){
		this.Name = name;
	}

	public String getName(){
		return Name;
	}

	public void setEmail(String email){
		this.Email = email;
	}

	public String getEmail(){
		return Email;
	}

	public void setMobileNo(String mobileNo){
		this.MobileNo = mobileNo;
	}

	public String getMobileNo(){
		return MobileNo;
	}

	public void setMessage(String message){
		this.Message = message;
	}

	public String getMessage(){
		return Message;
	}

	public void setInserdate(String inserdate){
		this.Inserdate = inserdate;
	}

	public String getInserdate(){
		return Inserdate;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"id = '" + Id + '\'' +
			",name = '" + Name + '\'' +
			",email = '" + Email + '\'' +
			",mobileNo = '" + MobileNo + '\'' +
			",message = '" + Message + '\'' +
			",inserdate = '" + Inserdate + '\'' +
			"}";
		}
}