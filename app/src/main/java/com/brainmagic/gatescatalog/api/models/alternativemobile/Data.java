
package com.brainmagic.gatescatalog.api.models.alternativemobile;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Data {

    @SerializedName("Alter_Mblno")
    private String mAlterMblno;
    @SerializedName("email")
    private Object mEmail;
    @SerializedName("mobileno")
    private String mMobileno;
    @SerializedName("name")
    private String mName;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("Username")
    private String mUsername;

    public String getAlterMblno() {
        return mAlterMblno;
    }

    public void setAlterMblno(String alterMblno) {
        mAlterMblno = alterMblno;
    }

    public Object getEmail() {
        return mEmail;
    }

    public void setEmail(Object email) {
        mEmail = email;
    }

    public String getMobileno() {
        return mMobileno;
    }

    public void setMobileno(String mobileno) {
        mMobileno = mobileno;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

}
