
package com.brainmagic.gatescatalog.api.models.moredetails;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class EngineCodeModel implements Serializable {

    @SerializedName("data")
    private List<EngineCodeResult> mData;
    @SerializedName("result")
    private String mResult;
    @SerializedName("data1")
    private YearList mData1;

    public List<EngineCodeResult> getData() {
        return mData;
    }

    public void setData(List<EngineCodeResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

    public YearList getmData1() {
        return mData1;
    }

    public void setmData1(YearList mData1) {
        this.mData1 = mData1;
    }
}
