package com.brainmagic.gatescatalog.api.models.scanhistoryloyalty;

import java.io.Serializable;
import java.util.List;

public class ScanHistoryLoyalty implements Serializable {
	private String result;
	private List<DataItem> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<DataItem> data){
		this.data = data;
	}

	public List<DataItem> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"ScanHistoryLoyalty{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}