package com.brainmagic.gatescatalog.api.models.scanloyalty;

import java.io.Serializable;

public class Data implements Serializable {

	private int Id;
	private String Mobileno;
	private String Name;
	private String Serialno;
	private String Couponcode;
	private String Address;
	private String Latitude;
	private String Longitude;
	private String InsertDate;
	private String InsertDate1;
	private String Usertype;
	private String Validity;
	private String RewardPoint;
	private String Scanstatus;
	private String ScanCount;

	public void setId(int id){
		this.Id = id;
	}

	public int getId(){
		return Id;
	}

	public void setMobileno(String mobileno){
		this.Mobileno = mobileno;
	}

	public String getMobileno(){
		return Mobileno;
	}

	public void setName(String name){
		this.Name = name;
	}

	public String getName(){
		return Name;
	}

	public void setSerialno(String serialno){
		this.Serialno = serialno;
	}

	public String getSerialno(){
		return Serialno;
	}

	public void setCouponcode(String couponcode){
		this.Couponcode = couponcode;
	}

	public String getCouponcode(){
		return Couponcode;
	}

	public void setAddress(String address){
		this.Address = address;
	}

	public String getAddress(){
		return Address;
	}

	public void setLatitude(String latitude){
		this.Latitude = latitude;
	}

	public String getLatitude(){
		return Latitude;
	}

	public void setLongitude(String longitude){
		this.Longitude = longitude;
	}

	public String getLongitude(){
		return Longitude;
	}

	public void setInsertDate(String insertDate){
		this.InsertDate = insertDate;
	}

	public String getInsertDate(){
		return InsertDate;
	}

	public void setInsertDate1(String insertDate1){
		this.InsertDate1 = insertDate1;
	}

	public String getInsertDate1(){
		return InsertDate1;
	}

	public void setUsertype(String usertype){
		this.Usertype = usertype;
	}

	public String getUsertype(){
		return Usertype;
	}

	public void setValidity(String validity){
		this.Validity = validity;
	}

	public String getValidity(){
		return Validity;
	}

	public void setRewardPoint(String rewardPoint){
		this.RewardPoint = rewardPoint;
	}

	public String getRewardPoint(){
		return RewardPoint;
	}

	public void setScanstatus(String scanstatus){
		this.Scanstatus = scanstatus;
	}

	public String getScanstatus(){
		return Scanstatus;
	}

	public void setScanCount(String scanCount){
		this.ScanCount = scanCount;
	}

	public String getScanCount(){
		return ScanCount;
	}

	@Override
 	public String toString(){
		return
			"Data{" +
			"id = '" + Id + '\'' +
			",mobileno = '" + Mobileno + '\'' +
			",name = '" + Name + '\'' +
			",serialno = '" + Serialno + '\'' +
			",couponcode = '" + Couponcode + '\'' +
			",address = '" + Address + '\'' +
			",latitude = '" + Latitude + '\'' +
			",longitude = '" + Longitude + '\'' +
			",insertDate = '" + InsertDate + '\'' +
			",insertDate1 = '" + InsertDate1 + '\'' +
			",usertype = '" + Usertype + '\'' +
			",validity = '" + Validity + '\'' +
			",rewardPoint = '" + RewardPoint + '\'' +
			",scanstatus = '" + Scanstatus + '\'' +
			",scanCount = '" + ScanCount + '\'' +
			"}";
		}
}