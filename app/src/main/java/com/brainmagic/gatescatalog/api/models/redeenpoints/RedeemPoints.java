package com.brainmagic.gatescatalog.api.models.redeenpoints;

import java.io.Serializable;

public class RedeemPoints implements Serializable {
	private String result;
	private String data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(String data){
		this.data = data;
	}

	public String getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"RedeemPoints{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}