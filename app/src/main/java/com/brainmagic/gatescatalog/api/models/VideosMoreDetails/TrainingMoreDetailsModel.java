
package com.brainmagic.gatescatalog.api.models.VideosMoreDetails;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class TrainingMoreDetailsModel {

    @SerializedName("data")
    private List<TrainingMoreDetailResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<TrainingMoreDetailResult> getData() {
        return mData;
    }

    public void setData(List<TrainingMoreDetailResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
