
package com.brainmagic.gatescatalog.api.models.oesearch;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Datum {

    @SerializedName("ARG")
    private String mARG;
    @SerializedName("ApplicationAttributes")
    private String mApplicationAttributes;
    @SerializedName("Article")
    private String mArticle;
    @SerializedName("Articlegroup")
    private String mArticlegroup;
    @SerializedName("CC")
    private String mCC;
    @SerializedName("CarID")
    private String mCarID;
    @SerializedName("DateFrom")
    private String mDateFrom;
    @SerializedName("DateTo")
    private String mDateTo;
    @SerializedName("Enginecode")
    private String mEnginecode;
    @SerializedName("Fuel")
    private String mFuel;
    @SerializedName("HMDnr")
    private String mHMDnr;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("KW")
    private String mKW;
    @SerializedName("Level1")
    private String mLevel1;
    @SerializedName("MRP")
    private Double mMRP;
    @SerializedName("Make")
    private String mMake;
    @SerializedName("Model")
    private String mModel;
    @SerializedName("ModelSort")
    private String mModelSort;
    @SerializedName("Modelcode")
    private String mModelcode;
    @SerializedName("Modelrange")
    private String mModelrange;
    @SerializedName("MonthFrom")
    private String mMonthFrom;
    @SerializedName("MonthTill")
    private String mMonthTill;
    @SerializedName("Position")
    private String mPosition;
    @SerializedName("Segment")
    private String mSegment;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("StatusDescription")
    private String mStatusDescription;
    @SerializedName("Stroke")
    private Double mStroke;
    @SerializedName("TecDocStatus")
    private String mTecDocStatus;
    @SerializedName("Type")
    private String mType;
    @SerializedName("VehicleInfo")
    private String mVehicleInfo;
    @SerializedName("YearFrom")
    private String mYearFrom;
    @SerializedName("YearTill")
    private String mYearTill;

    public String getARG() {
        return mARG;
    }

    public void setARG(String aRG) {
        mARG = aRG;
    }

    public String getApplicationAttributes() {
        return mApplicationAttributes;
    }

    public void setApplicationAttributes(String applicationAttributes) {
        mApplicationAttributes = applicationAttributes;
    }

    public String getArticle() {
        return mArticle;
    }

    public void setArticle(String article) {
        mArticle = article;
    }

    public String getArticlegroup() {
        return mArticlegroup;
    }

    public void setArticlegroup(String articlegroup) {
        mArticlegroup = articlegroup;
    }

    public String getCC() {
        return mCC;
    }

    public void setCC(String cC) {
        mCC = cC;
    }

    public String getCarID() {
        return mCarID;
    }

    public void setCarID(String carID) {
        mCarID = carID;
    }

    public String getDateFrom() {
        return mDateFrom;
    }

    public void setDateFrom(String dateFrom) {
        mDateFrom = dateFrom;
    }

    public String getDateTo() {
        return mDateTo;
    }

    public void setDateTo(String dateTo) {
        mDateTo = dateTo;
    }

    public String getEnginecode() {
        return mEnginecode;
    }

    public void setEnginecode(String enginecode) {
        mEnginecode = enginecode;
    }

    public String getFuel() {
        return mFuel;
    }

    public void setFuel(String fuel) {
        mFuel = fuel;
    }

    public String getHMDnr() {
        return mHMDnr;
    }

    public void setHMDnr(String hMDnr) {
        mHMDnr = hMDnr;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String insertDate) {
        mInsertDate = insertDate;
    }

    public String getKW() {
        return mKW;
    }

    public void setKW(String kW) {
        mKW = kW;
    }

    public String getLevel1() {
        return mLevel1;
    }

    public void setLevel1(String level1) {
        mLevel1 = level1;
    }

    public Double getMRP() {
        return mMRP;
    }

    public void setMRP(Double mRP) {
        mMRP = mRP;
    }

    public String getMake() {
        return mMake;
    }

    public void setMake(String make) {
        mMake = make;
    }

    public String getModel() {
        return mModel;
    }

    public void setModel(String model) {
        mModel = model;
    }

    public String getModelSort() {
        return mModelSort;
    }

    public void setModelSort(String modelSort) {
        mModelSort = modelSort;
    }

    public String getModelcode() {
        return mModelcode;
    }

    public void setModelcode(String modelcode) {
        mModelcode = modelcode;
    }

    public String getModelrange() {
        return mModelrange;
    }

    public void setModelrange(String modelrange) {
        mModelrange = modelrange;
    }

    public String getMonthFrom() {
        return mMonthFrom;
    }

    public void setMonthFrom(String monthFrom) {
        mMonthFrom = monthFrom;
    }

    public String getMonthTill() {
        return mMonthTill;
    }

    public void setMonthTill(String monthTill) {
        mMonthTill = monthTill;
    }

    public String getPosition() {
        return mPosition;
    }

    public void setPosition(String position) {
        mPosition = position;
    }

    public String getSegment() {
        return mSegment;
    }

    public void setSegment(String segment) {
        mSegment = segment;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getStatusDescription() {
        return mStatusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        mStatusDescription = statusDescription;
    }

    public Double getStroke() {
        return mStroke;
    }

    public void setStroke(Double stroke) {
        mStroke = stroke;
    }

    public String getTecDocStatus() {
        return mTecDocStatus;
    }

    public void setTecDocStatus(String tecDocStatus) {
        mTecDocStatus = tecDocStatus;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getVehicleInfo() {
        return mVehicleInfo;
    }

    public void setVehicleInfo(String vehicleInfo) {
        mVehicleInfo = vehicleInfo;
    }

    public String getYearFrom() {
        return mYearFrom;
    }

    public void setYearFrom(String yearFrom) {
        mYearFrom = yearFrom;
    }

    public String getYearTill() {
        return mYearTill;
    }

    public void setYearTill(String yearTill) {
        mYearTill = yearTill;
    }

}
