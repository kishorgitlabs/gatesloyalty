package com.brainmagic.gatescatalog.api.models.scanhistoryloyalty;

import java.io.Serializable;

public class DataItem implements Serializable {
	private int Id;
	private String Mobileno;
	private String Name;
	private String Serialno;
	private String Couponcode;
	private Object Address;
	private Object Latitude;
	private Object Longitude;
	private String InsertDate;
	private String InsertDate1;
	private String Usertype;
	private Object Validity;
	private String RewardPoint;
	private String Scanstatus;
	private String ScanCount;

	public void setId(int id){
		this.Id = id;
	}

	public int getId(){
		return Id;
	}

	public void setMobileno(String mobileno){
		this.Mobileno = mobileno;
	}

	public String getMobileno(){
		return Mobileno;
	}

	public void setName(String name){
		this.Name = name;
	}

	public String getName(){
		return Name;
	}

	public void setSerialno(String serialno){
		this.Serialno = serialno;
	}

	public String getSerialno(){
		return Serialno;
	}

	public void setCouponcode(String couponcode){
		this.Couponcode = couponcode;
	}

	public String getCouponcode(){
		return Couponcode;
	}

	public void setAddress(Object address){
		this.Address = address;
	}

	public Object getAddress(){
		return Address;
	}

	public void setLatitude(Object latitude){
		this.Latitude = latitude;
	}

	public Object getLatitude(){
		return Latitude;
	}

	public void setLongitude(Object longitude){
		this.Longitude = longitude;
	}

	public Object getLongitude(){
		return Longitude;
	}

	public void setInsertDate(String insertDate){
		this.InsertDate = insertDate;
	}

	public String getInsertDate(){
		return InsertDate;
	}

	public void setInsertDate1(String insertDate1){
		this.InsertDate1 = insertDate1;
	}

	public String getInsertDate1(){
		return InsertDate1;
	}

	public void setUsertype(String usertype){
		this.Usertype = usertype;
	}

	public String getUsertype(){
		return Usertype;
	}

	public void setValidity(Object validity){
		this.Validity = validity;
	}

	public Object getValidity(){
		return Validity;
	}

	public void setRewardPoint(String rewardPoint){
		this.RewardPoint = rewardPoint;
	}

	public String getRewardPoint(){
		return RewardPoint;
	}

	public void setScanstatus(String scanstatus){
		this.Scanstatus = scanstatus;
	}

	public String getScanstatus(){
		return Scanstatus;
	}

	public void setScanCount(String scanCount){
		this.ScanCount = scanCount;
	}

	public String getScanCount(){
		return ScanCount;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"id = '" + Id + '\'' +
			",mobileno = '" + Mobileno + '\'' +
			",name = '" + Name + '\'' +
			",serialno = '" + Serialno + '\'' +
			",couponcode = '" + Couponcode + '\'' +
			",address = '" + Address + '\'' +
			",latitude = '" + Latitude + '\'' +
			",longitude = '" + Longitude + '\'' +
			",insertDate = '" + InsertDate + '\'' +
			",insertDate1 = '" + InsertDate1 + '\'' +
			",usertype = '" + Usertype + '\'' +
			",validity = '" + Validity + '\'' +
			",rewardPoint = '" + RewardPoint + '\'' +
			",scanstatus = '" + Scanstatus + '\'' +
			",scanCount = '" +ScanCount + '\'' +
			"}";
		}
}