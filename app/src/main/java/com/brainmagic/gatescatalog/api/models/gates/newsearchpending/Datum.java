
package com.brainmagic.gatescatalog.api.models.gates.newsearchpending;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Datum {

    @SerializedName("Address")
    private Object mAddress;
    @SerializedName("appid")
    private Object mAppid;
    @SerializedName("ApproveStatus")
    private String mApproveStatus;
    @SerializedName("BusinessName")
    private String mBusinessName;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Country")
    private String mCountry;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("Gstin")
    private String mGstin;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Insertdate")
    private String mInsertdate;
    @SerializedName("Latitude")
    private Object mLatitude;
    @SerializedName("Location")
    private Object mLocation;
    @SerializedName("Longitude")
    private Object mLongitude;
    @SerializedName("mobileno")
    private String mMobileno;
    @SerializedName("name")
    private String mName;
    @SerializedName("OTPStatus")
    private String mOTPStatus;
    @SerializedName("Password")
    private Object mPassword;
    @SerializedName("RegistrationType")
    private String mRegistrationType;
    @SerializedName("ShopName")
    private String mShopName;
    @SerializedName("State")
    private String mState;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("updatetime")
    private Object mUpdatetime;
    @SerializedName("usertype")
    private String mUsertype;

    public Object getAddress() {
        return mAddress;
    }

    public void setAddress(Object address) {
        mAddress = address;
    }

    public Object getAppid() {
        return mAppid;
    }

    public void setAppid(Object appid) {
        mAppid = appid;
    }

    public String getApproveStatus() {
        return mApproveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        mApproveStatus = approveStatus;
    }

    public String getBusinessName() {
        return mBusinessName;
    }

    public void setBusinessName(String businessName) {
        mBusinessName = businessName;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getGstin() {
        return mGstin;
    }

    public void setGstin(String gstin) {
        mGstin = gstin;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertdate() {
        return mInsertdate;
    }

    public void setInsertdate(String insertdate) {
        mInsertdate = insertdate;
    }

    public Object getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Object latitude) {
        mLatitude = latitude;
    }

    public Object getLocation() {
        return mLocation;
    }

    public void setLocation(Object location) {
        mLocation = location;
    }

    public Object getLongitude() {
        return mLongitude;
    }

    public void setLongitude(Object longitude) {
        mLongitude = longitude;
    }

    public String getMobileno() {
        return mMobileno;
    }

    public void setMobileno(String mobileno) {
        mMobileno = mobileno;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getOTPStatus() {
        return mOTPStatus;
    }

    public void setOTPStatus(String oTPStatus) {
        mOTPStatus = oTPStatus;
    }

    public Object getPassword() {
        return mPassword;
    }

    public void setPassword(Object password) {
        mPassword = password;
    }

    public String getRegistrationType() {
        return mRegistrationType;
    }

    public void setRegistrationType(String registrationType) {
        mRegistrationType = registrationType;
    }

    public String getShopName() {
        return mShopName;
    }

    public void setShopName(String shopName) {
        mShopName = shopName;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Object getUpdatetime() {
        return mUpdatetime;
    }

    public void setUpdatetime(Object updatetime) {
        mUpdatetime = updatetime;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

}
