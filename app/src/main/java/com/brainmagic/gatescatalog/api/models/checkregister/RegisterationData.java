package com.brainmagic.gatescatalog.api.models.checkregister;

import com.google.gson.annotations.SerializedName;

public class RegisterationData {

	@SerializedName("Status")
	private String status;

	@SerializedName("ApproveStatus")
	private String approveStatus;

	@SerializedName("ShopName")
	private Object shopName;

	@SerializedName("BusinessName")
	private String businessName;

	@SerializedName("Address")
	private String address;

	@SerializedName("Insertdate")
	private String insertdate;

	@SerializedName("usertype")
	private String usertype;

	@SerializedName("Latitude")
	private String latitude;

	@SerializedName("mobileno")
	private String mobileno;

	@SerializedName("City")
	private Object city;

	@SerializedName("Longitude")
	private String longitude;

	@SerializedName("Date")
	private String date;

	@SerializedName("Gstin")
	private Object gstin;

	@SerializedName("State")
	private Object state;

	@SerializedName("appid")
	private String appid;

	@SerializedName("name")
	private String name;

	@SerializedName("RegistrationType")
	private String registrationType;

	@SerializedName("Country")
	private String country;

	@SerializedName("id")
	private int id;

	@SerializedName("OTPStatus")
	private String oTPStatus;

	@SerializedName("updatetime")
	private Object updatetime;

	@SerializedName("email")
	private String email;

	@SerializedName("Password")
	private String password;

	@SerializedName("Location")
	private Object location;

	public String getStatus(){
		return status;
	}

	public String getApproveStatus(){
		return approveStatus;
	}

	public Object getShopName(){
		return shopName;
	}

	public String getBusinessName(){
		return businessName;
	}

	public String getAddress(){
		return address;
	}

	public String getInsertdate(){
		return insertdate;
	}

	public String getUsertype(){
		return usertype;
	}

	public String getLatitude(){
		return latitude;
	}

	public String getMobileno(){
		return mobileno;
	}

	public Object getCity(){
		return city;
	}

	public String getLongitude(){
		return longitude;
	}

	public String getDate(){
		return date;
	}

	public Object getGstin(){
		return gstin;
	}

	public Object getState(){
		return state;
	}

	public String getAppid(){
		return appid;
	}

	public String getName(){
		return name;
	}

	public String getRegistrationType(){
		return registrationType;
	}

	public String getCountry(){
		return country;
	}

	public int getId(){
		return id;
	}

	public String getOTPStatus(){
		return oTPStatus;
	}

	public Object getUpdatetime(){
		return updatetime;
	}

	public String getEmail(){
		return email;
	}

	public String getPassword(){
		return password;
	}

	public Object getLocation(){
		return location;
	}
}