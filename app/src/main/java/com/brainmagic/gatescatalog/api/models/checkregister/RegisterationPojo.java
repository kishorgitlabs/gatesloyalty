package com.brainmagic.gatescatalog.api.models.checkregister;

import com.google.gson.annotations.SerializedName;

public class RegisterationPojo{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private RegisterationData data;

	public String getResult(){
		return result;
	}

	public RegisterationData getData(){
		return data;
	}
}