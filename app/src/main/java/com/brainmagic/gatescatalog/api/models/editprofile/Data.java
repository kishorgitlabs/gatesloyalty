
package com.brainmagic.gatescatalog.api.models.editprofile;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Data {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("Alter_Mblno")
    private String mAlterMblno;
    @SerializedName("appid")
    private String mAppid;
    @SerializedName("ApproveStatus")
    private String mApproveStatus;
    @SerializedName("Approvedby")
    private Object mApprovedby;
    @SerializedName("BusinessName")
    private String mBusinessName;
    @SerializedName("City")
    private String mCity;
    @SerializedName("Country")
    private String mCountry;
    @SerializedName("Createdby")
    private Object mCreatedby;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("Gstin")
    private Object mGstin;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Insertdate")
    private String mInsertdate;
    @SerializedName("Latitude")
    private String mLatitude;
    @SerializedName("Location")
    private Object mLocation;
    @SerializedName("Longitude")
    private String mLongitude;
    @SerializedName("mobileno")
    private String mMobileno;
    @SerializedName("name")
    private String mName;
    @SerializedName("OTPStatus")
    private String mOTPStatus;
    @SerializedName("Password")
    private Object mPassword;
    @SerializedName("RegistrationType")
    private String mRegistrationType;
    @SerializedName("Rejectedby")
    private Object mRejectedby;
    @SerializedName("ShopName")
    private Object mShopName;
    @SerializedName("State")
    private String mState;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("updatetime")
    private Object mUpdatetime;
    @SerializedName("Username")
    private Object mUsername;
    @SerializedName("usertype")
    private String mUsertype;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAlterMblno() {
        return mAlterMblno;
    }

    public void setAlterMblno(String alterMblno) {
        mAlterMblno = alterMblno;
    }

    public String getAppid() {
        return mAppid;
    }

    public void setAppid(String appid) {
        mAppid = appid;
    }

    public String getApproveStatus() {
        return mApproveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        mApproveStatus = approveStatus;
    }

    public Object getApprovedby() {
        return mApprovedby;
    }

    public void setApprovedby(Object approvedby) {
        mApprovedby = approvedby;
    }

    public String getBusinessName() {
        return mBusinessName;
    }

    public void setBusinessName(String businessName) {
        mBusinessName = businessName;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public Object getCreatedby() {
        return mCreatedby;
    }

    public void setCreatedby(Object createdby) {
        mCreatedby = createdby;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Object getGstin() {
        return mGstin;
    }

    public void setGstin(Object gstin) {
        mGstin = gstin;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertdate() {
        return mInsertdate;
    }

    public void setInsertdate(String insertdate) {
        mInsertdate = insertdate;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public Object getLocation() {
        return mLocation;
    }

    public void setLocation(Object location) {
        mLocation = location;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getMobileno() {
        return mMobileno;
    }

    public void setMobileno(String mobileno) {
        mMobileno = mobileno;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getOTPStatus() {
        return mOTPStatus;
    }

    public void setOTPStatus(String oTPStatus) {
        mOTPStatus = oTPStatus;
    }

    public Object getPassword() {
        return mPassword;
    }

    public void setPassword(Object password) {
        mPassword = password;
    }

    public String getRegistrationType() {
        return mRegistrationType;
    }

    public void setRegistrationType(String registrationType) {
        mRegistrationType = registrationType;
    }

    public Object getRejectedby() {
        return mRejectedby;
    }

    public void setRejectedby(Object rejectedby) {
        mRejectedby = rejectedby;
    }

    public Object getShopName() {
        return mShopName;
    }

    public void setShopName(Object shopName) {
        mShopName = shopName;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Object getUpdatetime() {
        return mUpdatetime;
    }

    public void setUpdatetime(Object updatetime) {
        mUpdatetime = updatetime;
    }

    public Object getUsername() {
        return mUsername;
    }

    public void setUsername(Object username) {
        mUsername = username;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

}
