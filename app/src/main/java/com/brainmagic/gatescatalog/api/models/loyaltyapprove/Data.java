package com.brainmagic.gatescatalog.api.models.loyaltyapprove;

public class Data{
	private String status;
	private String ApproveStatus;
	private Object shopName;
	private String businessName;
	private String address;
	private String insertdate;
	private String usertype;
	private String latitude;
	private String mobileno;
	private Object city;
	private String longitude;
	private String date;
	private Object gstin;
	private Object state;
	private String appid;
	private String name;
	private String registrationType;
	private String country;
	private int id;
	private String oTPStatus;
	private Object updatetime;
	private Object email;
	private Object password;
	private Object location;

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setApproveStatus(String approveStatus){
		this.ApproveStatus = approveStatus;
	}

	public String getApproveStatus(){
		return ApproveStatus;
	}

	public void setShopName(Object shopName){
		this.shopName = shopName;
	}

	public Object getShopName(){
		return shopName;
	}

	public void setBusinessName(String businessName){
		this.businessName = businessName;
	}

	public String getBusinessName(){
		return businessName;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setInsertdate(String insertdate){
		this.insertdate = insertdate;
	}

	public String getInsertdate(){
		return insertdate;
	}

	public void setUsertype(String usertype){
		this.usertype = usertype;
	}

	public String getUsertype(){
		return usertype;
	}

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setMobileno(String mobileno){
		this.mobileno = mobileno;
	}

	public String getMobileno(){
		return mobileno;
	}

	public void setCity(Object city){
		this.city = city;
	}

	public Object getCity(){
		return city;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setGstin(Object gstin){
		this.gstin = gstin;
	}

	public Object getGstin(){
		return gstin;
	}

	public void setState(Object state){
		this.state = state;
	}

	public Object getState(){
		return state;
	}

	public void setAppid(String appid){
		this.appid = appid;
	}

	public String getAppid(){
		return appid;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setRegistrationType(String registrationType){
		this.registrationType = registrationType;
	}

	public String getRegistrationType(){
		return registrationType;
	}

	public void setCountry(String country){
		this.country = country;
	}

	public String getCountry(){
		return country;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setOTPStatus(String oTPStatus){
		this.oTPStatus = oTPStatus;
	}

	public String getOTPStatus(){
		return oTPStatus;
	}

	public void setUpdatetime(Object updatetime){
		this.updatetime = updatetime;
	}

	public Object getUpdatetime(){
		return updatetime;
	}

	public void setEmail(Object email){
		this.email = email;
	}

	public Object getEmail(){
		return email;
	}

	public void setPassword(Object password){
		this.password = password;
	}

	public Object getPassword(){
		return password;
	}

	public void setLocation(Object location){
		this.location = location;
	}

	public Object getLocation(){
		return location;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"status = '" + status + '\'' + 
			",approveStatus = '" + ApproveStatus + '\'' +
			",shopName = '" + shopName + '\'' + 
			",businessName = '" + businessName + '\'' + 
			",address = '" + address + '\'' + 
			",insertdate = '" + insertdate + '\'' + 
			",usertype = '" + usertype + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",mobileno = '" + mobileno + '\'' + 
			",city = '" + city + '\'' + 
			",longitude = '" + longitude + '\'' + 
			",date = '" + date + '\'' + 
			",gstin = '" + gstin + '\'' + 
			",state = '" + state + '\'' + 
			",appid = '" + appid + '\'' + 
			",name = '" + name + '\'' + 
			",registrationType = '" + registrationType + '\'' + 
			",country = '" + country + '\'' + 
			",id = '" + id + '\'' + 
			",oTPStatus = '" + oTPStatus + '\'' + 
			",updatetime = '" + updatetime + '\'' + 
			",email = '" + email + '\'' + 
			",password = '" + password + '\'' + 
			",location = '" + location + '\'' + 
			"}";
		}
}
